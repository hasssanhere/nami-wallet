import React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
const Favourites = () => {
  const images = [
    {
      image:
        "https://images.pexels.com/photos/7249183/pexels-photo-7249183.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
      text: "First",
    },
    {
      image:
        "https://images.pexels.com/photos/36704/pexels-photo.jpg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
      text: "second",
    },
    {
      image:
        "https://images.pexels.com/photos/7261977/pexels-photo-7261977.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
      text: "Third",
    },
    {
      image:
        "https://images.pexels.com/photos/2427797/pexels-photo-2427797.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
      text: "Fourth",
    },
    {
      image:
        "https://images.pexels.com/photos/3163677/pexels-photo-3163677.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
      text: "Fifth",
    },
  ];
  return (
    <section className="relative">
      {/* Section background (needs .relative class on parent and next sibling elements) */}

      <div className="relative max-w-10xl mx-auto  sm:px-6">
        {/* Items */}

        <div className="max-w-sm mx-auto grid gap-8 md:grid-cols-2 lg:grid-cols-3 md:max-w-2xl lg:max-w-none">
          {images.map((image) => {
            return (
              <div
                style={{
                  position: "relative",
                  // marginRight: "1rem",
                  // marginLeft: "1rem",
                  margin: "0rem 1rem",
                }}
              >
                <Card
                  // sx={{
                  //   maxWidth: 350,
                  //   minHeight: 450,
                  //   minWidth: 350,
                  // }}
                  style={{
                    backgroundColor: "rgba(196, 196, 196, 0.6)",
                    borderRadius: "10px",
                    boxShadow: "none",
                  }}
                >
                  <CardMedia
                    component="img"
                    height="194"
                    image={image.image}
                    alt="green iguana"
                    sx={{ minHeight: 380 }}
                  />
                  <CardContent>
                    <Typography
                      className="font-bold"
                      gutterBottom
                      variant="p"
                      component="div"
                    >
                      {image.text}
                    </Typography>
                    <Typography color="black">
                      current bid: <p className="float-right">6.5 ADA</p>
                    </Typography>
                  </CardContent>
                  {/* <CardActions></CardActions> */}
                </Card>
              </div>
            );
          })}
        </div>
      </div>
    </section>
  );
};

export default Favourites;
