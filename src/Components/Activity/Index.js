import React, { useState } from "react";
// import Header from "../partials/Header";
// import Footer from "../partials/Footer";
// import DummyNotificationData from "../Data/Notifications/DummyData";
import AppsIcon from "@mui/icons-material/Apps";

import { styled } from "@mui/material/styles";
import Paper from "@mui/material/Paper";
import {
  MDBTabs,
  MDBTabsItem,
  MDBTabsLink,
  MDBTabsContent,
  MDBTabsPane,
  MDBRow,
  MDBCol,
} from "mdb-react-ui-kit";
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
} from "@material-ui/core";
import MainActivity from "../Activity/Tab1/MainActivity";
import Activebids from "./Tab2/Activebids";
import Notifications from "./Tab3/Notifications";
import Favourites from "./Tab4/Favourites";

const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));
function Dashboard() {
  const [verticalActive, setVerticalActive] = useState("tab1");

  const handleVerticalClick = (value) => {
    if (value === verticalActive) {
      return;
    }

    setVerticalActive(value);
  };
  const images = [
    {
      image:
        "https://images.pexels.com/photos/7249183/pexels-photo-7249183.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
      text: "First",
    },
    {
      image:
        "https://images.pexels.com/photos/36704/pexels-photo.jpg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
      text: "second",
    },
    {
      image:
        "https://images.pexels.com/photos/7261977/pexels-photo-7261977.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
      text: "Third",
    },
    {
      image:
        "https://images.pexels.com/photos/2427797/pexels-photo-2427797.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
      text: "Fourth",
    },
    {
      image:
        "https://images.pexels.com/photos/3163677/pexels-photo-3163677.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
      text: "Fifth",
    },
  ];
  return (
    <>
      <div>
        <section class="py-20 2xl:py-40  overflow-hidden">
          <div
            style={{
              margin: "5rem 3rem",
              // marginLeft: "2rem",
            }}
          >
            {" "}
            <div>
              <MDBRow>
                <MDBCol size="2">
                  <MDBTabs className="flex-column text-center">
                    <MDBTabsItem>
                      <MDBTabsLink
                        onClick={() => handleVerticalClick("tab1")}
                        active={verticalActive === "tab1"}
                        style={{
                          color: "black",
                          border: "1px solid black",
                          // width: "10rem",
                          textAlign: "left",
                        }}
                      >
                        <AppsIcon className="ml-2" /> ACTIVE SESSIONS
                      </MDBTabsLink>
                    </MDBTabsItem>
                    <MDBTabsItem className="mt-3">
                      <MDBTabsLink
                        onClick={() => handleVerticalClick("tab2")}
                        active={verticalActive === "tab2"}
                        style={{
                          color: "black",
                          // width: "10rem",
                          border: "1px solid black",
                          textAlign: "left",
                        }}
                      >
                        <AppsIcon className="ml-2" /> ACTIVE BIDS
                      </MDBTabsLink>
                    </MDBTabsItem>
                    <MDBTabsItem className="mt-3">
                      <MDBTabsLink
                        onClick={() => handleVerticalClick("tab3")}
                        active={verticalActive === "tab3"}
                        style={{
                          color: "black",
                          border: "1px solid black",
                          // width: "10rem",
                          textAlign: "left",
                        }}
                      >
                        <AppsIcon className="ml-2" /> NOTIFICATIONS
                      </MDBTabsLink>
                    </MDBTabsItem>
                    <MDBTabsItem className="mt-3">
                      <MDBTabsLink
                        onClick={() => handleVerticalClick("tab4")}
                        active={verticalActive === "tab4"}
                        style={{
                          color: "black",
                          border: "1px solid black",
                          // width: "10rem",
                          textAlign: "left",
                        }}
                      >
                        <AppsIcon className="ml-2" /> FAVOURITES
                      </MDBTabsLink>
                    </MDBTabsItem>
                  </MDBTabs>
                </MDBCol>

                <MDBCol size="10">
                  <MDBTabsContent>
                    <MDBTabsPane show={verticalActive === "tab1"}>
                      <MainActivity />
                    </MDBTabsPane>
                    <MDBTabsPane show={verticalActive === "tab2"}>
                      <Activebids images={images} />
                    </MDBTabsPane>
                    <MDBTabsPane show={verticalActive === "tab3"}>
                      <Notifications />
                    </MDBTabsPane>
                    <MDBTabsPane show={verticalActive === "tab4"}>
                      <Favourites images={images} />
                    </MDBTabsPane>
                  </MDBTabsContent>
                </MDBCol>
              </MDBRow>
            </div>
          </div>
        </section>
      </div>
    </>
  );
}

export default Dashboard;
