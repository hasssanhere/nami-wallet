import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Typography,
} from "@mui/material";
import React from "react";
import DummyNotificationData from "../../../App/Assets/Data/Notifications/DummyData";

const Notifications = () => {
  return (
    <>
      {" "}
      {DummyNotificationData?.map((item) => {
        return (
          <Accordion className="mt-1">
            <AccordionSummary
              className="hover:text-white rounded"
              aria-controls="panel1a-content "
              id="panel1a-header"
              style={{
                fontFamily: "Raleway",
                backgroundColor: "rgba(196, 196, 196, 0.6)",
              }}
            >
              <div class="container">
                <div class="row">
                  <div class="col-sm-2">{item.time}</div>
                  <div class="col-sm-10">{item.title}</div>
                </div>
              </div>
            </AccordionSummary>
            <AccordionDetails
              style={{
                fontFamily: "Raleway",
                backgroundColor: "#f5f5f5",
                color: "#000000",
              }}
            >
              <Typography>{item.description}</Typography>
            </AccordionDetails>
          </Accordion>
        );
      })}
    </>
  );
};

export default Notifications;
