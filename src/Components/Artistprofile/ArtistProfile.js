import React, { useEffect, useState } from "react";
import "../../App/Assets/css/css/profile.css";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import { CircularProgress } from "@mui/material";

import {
  getArtistDetailbyId,
  getArtistNftById,
  getFollowUnfollowStatus,
  unFollowArtist,
} from "../../Services/artistSevices";
import { followArtist } from "../../Services/artistSevices";
import Swal from "sweetalert2";
import bannerimg from "../../App/Assets/images/profile-banner.png";
import { useLocation, useParams } from "react-router-dom";
import { Card, CardContent, CardMedia, Skeleton } from "@mui/material";
import { Link } from "react-router-dom";

function ArtistProfile(props) {
  const location = useLocation();
  const id = location?.state?.userId;
  const [artistData, setArtistData] = useState({});
  const [buttonState, setButtonState] = useState();
  const [btnLoading, setBtnLoading] = useState(false);

  const [NftData, setNftData] = useState([]);

  const [Nftmsg, setNftMsg] = useState("");
  const [count, setCount] = useState(1);
  const [loading, setLoading] = useState(true);

  const [buttonText, setButtonText] = useState("");

  useEffect(() => {
    getArtistDetailbyId(id, setArtistData);
    getArtistNftById({ id, setNftData, setNftMsg, setLoading, count });
    getFollowUnfollowStatus(id, setButtonState);
  }, [count]);

  useEffect(() => window.scrollTo(0, 0), []);

  const followButtonClick = (id) => {
    if (sessionStorage.getItem("id")) {
      if (buttonText === "UNFOLLOW") {
        setButtonText("FOLLOW");
        unFollowArtist(id);
      } else {
        setButtonText("UNFOLLOW");
        followArtist(id);
      }
    } else {
      Swal.fire({
        icon: "error",
        text: "Login To Follow this user",
      });
    }
  };
  const fetchData = () => {
    setBtnLoading(true);
    //Faking API call here
    setTimeout(() => {
      setBtnLoading(false);
    }, 2000);
  };
  useEffect(() => {
    if (buttonState === true) {
      setButtonText("UNFOLLOW");
    } else {
      setButtonText("FOLLOW ");
    }
  }, [buttonState]);

  return (
    <div className=" wmt-67">
      <div className="max-w-max ">
        <>
          <img class="card-img-top" src={bannerimg} alt="Card image cap" />
          <div class="card-body little-profile text-center ">
            <div class="pro-img relative">
              <img
                className="pimage"
                src={artistData?.image}
                onError={(e) => {
                  /**
                   * Any code. For instance, changing the `src` prop with a fallback url.
                   * In our code, I've added `e.target.className = fallback_className` for instance.
                   */
                  e.target.src =
                    "https://media.istockphoto.com/vectors/male-profile-flat-blue-simple-icon-with-long-shadow-vector-id522855255?k=20&m=522855255&s=612x612&w=0&h=fLLvwEbgOmSzk1_jQ0MgDATEVcVOh_kqEe0rqi7aM5A=";
                }}
                alt="user"
              />
            </div>
            <h3 class="mb-2 f-36 w-username">{artistData?.flname} </h3>
            <p className="f-20 text-black">
              @{artistData?.username}{" "}
              {artistData?.reqStatus ? (
                <CheckCircleIcon style={{ fontSize: "25", color: "#367BF5" }} />
              ) : null}
            </p>{" "}
            <div class="d-flex justify-content-center px-50 mt-4">
              <div class="d-flex align-items-center w-folow">
                <p>{artistData?.followers?.length}</p> <span>Followers</span>
              </div>
              <div class="d-flex align-items-center w-folow">
                <p>{artistData?.following?.length}</p> <span>Following</span>
              </div>
              {artistData?._id == sessionStorage.getItem("id") ? null : (
                <div class="d-flex align-items-center w-folow">
                  <button
                    // data-aos="zoom-y-out"
                    className=" btn btn-btn-big text-center "
                    style={{ width: "99px", height: "37px" }}
                    onClick={() => followButtonClick(artistData?._id)}
                  >
                    <strong style={{ fontSize: "12px" }}>{buttonText}</strong>
                  </button>
                </div>
              )}
            </div>
          </div>
        </>
        <div className=" gridsm:grid-cols-12 gap-8 py-8 md:py-8 ">
          {NftData?.length !== 0 && !loading ? (
            <section className="relative">
              {/* Section background (needs .relative class on parent and next sibling elements) */}

              <div className="relative max-w-5xl mx-auto   sm:px-6">
                {/* Items */}
                <div className="max-w-sm mx-auto grid gap-8 md:grid-cols-3 lg:grid-cols-3 md:max-w-2xl lg:max-w-none">
                  {/*Loop Start*/}
                  {/* 1st item */}
                  {NftData?.map((item, key) => {
                    return (
                      <>
                        <Link
                          style={{
                            textDecoration: "none",
                            color: "inherit",
                          }}
                          to={{
                            pathname: `/artworkdetailed/${item._id}?nftId=${item._id}&userId=${item.artistId._id}`,
                            state: {
                              nftId: item._id,
                              userId: item.artistId._id,
                            },
                          }}
                        >
                          <Card
                            className="shadow-md"
                            style={{
                              backgroundColor: "#FfFfFf",
                              borderRadius: "10px",
                              // boxShadow: "none",
                              // padding: "10px 15px",
                            }}
                          >
                            <CardContent style={{ padding: "15px" }}>
                              <CardMedia
                                component="img"
                                // className="cardmediastyling"
                                height="194"
                                image={item.gatewayLink}
                                onError={(e) => {
                                  /**
                                   * Any code. For instance, changing the `src` prop with a fallback url.
                                   * In our code, I've added `e.target.className = fallback_className` for instance.
                                   */
                                  e.target.src =
                                    "https://mpama.com/wp-content/uploads/2017/04/default-image-620x600.jpg";
                                }}
                                style={{
                                  height: 280,
                                  objectFit: "cover",
                                  width: "100%",
                                  borderRadius: "10px",
                                }}
                              />
                            </CardContent>
                            <CardContent>
                              <div className=" font-Raleway grid grid-cols-2 text-left">
                                <div className="sm:col-span-2 md:col-span-2 lg:col-span-2 text-left my-2">
                                  <p
                                    className="font-bold text-base	text-black-100 text-break	"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title={item?.title}
                                  >
                                    {item?.title?.length > 15 ? (
                                      <>
                                        {`${item?.title?.substring(
                                          0,
                                          15
                                        )}...` || " "}
                                      </>
                                    ) : (
                                      <>{`${item?.title}` || " "}</>
                                    )}
                                  </p>
                                  <p
                                    className=" text-base	text-black-100 text-break	"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title={item?.collectionId?.collectionName}
                                  >
                                    {item?.collectionId?.collectionName
                                      ?.length > 20 ? (
                                      <>
                                        {`${item?.collectionId?.collectionName?.substring(
                                          0,
                                          20
                                        )}...` || " "}
                                      </>
                                    ) : (
                                      <>
                                        {`${item?.collectionId?.collectionName}` ||
                                          " "}
                                      </>
                                    )}
                                  </p>
                                </div>
                              </div>
                              <div className=" font-Raleway grid grid-cols-1 text-left">
                                <div className="sm:col-span-1 md:col-span-1 lg:col-span-1 text-left">
                                  <p
                                    className="font-bold text-base	text-black-100 text-break font-roboto	"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title={item?.price}
                                  >
                                    ADA {item?.price}
                                  </p>
                                </div>
                              </div>
                            </CardContent>
                          </Card>
                        </Link>
                      </>
                    );
                  })}
                </div>
                {NftData?.length > 11 && NftData?.length % 12 === 0 ? (
                  <div
                    className="col-md-12 d-flex mt-5"
                    style={{ justifyContent: "space-evenly" }}
                  >
                    <p
                      className="btn-sm fontFamily ml-6 mr-6  text-white text-lg rounded-md cursor-pointer"
                      style={{ fontWeight: "400", fontSize: "16px" }}
                      onClick={() => {
                        setCount(count + 1);
                        fetchData();
                      }}
                    >
                      {btnLoading && (
                        <CircularProgress size={20} className="mr-3" />
                      )}
                      {btnLoading && <span>Loading </span>}
                      {!btnLoading && <span>LoadMore</span>}{" "}
                    </p>
                  </div>
                ) : null}
              </div>
            </section>
          ) : (
            <>
              {loading ? (
                <div className="relative max-w-5xl mx-auto sm:px-6">
                  <div
                    className="max-w-sm mx-auto grid  md:grid-cols-3 lg:grid-cols-3 md:max-w-2xl lg:max-w-none"
                    style={{
                      display: "flex",
                      width: "100%",
                      flexDirection: "row",
                    }}
                  >
                    {[1, 2, 3].map(() => (
                      <div style={{ width: "33%", margin: "1rem" }}>
                        <Skeleton
                          animation="wave"
                          variant="rectangular"
                          width="100%"
                          height={300}
                        />
                        <Skeleton width="100%" />
                        <Skeleton width="100%" />
                        <Skeleton width="100%" />
                        <Skeleton width="100%" />
                      </div>
                    ))}
                  </div>
                </div>
              ) : (
                <>
                  <div className=" gridsm:grid-cols-12 gap-8 py-8 md:py-8  ">
                    <>
                      <section className="relative">
                        {/* Section background (needs .relative class on parent and next sibling elements) */}

                        <div className="relative max-w-5xl mx-auto  sm:px-6 text-center">
                          <div className="">
                            {/* Items */}

                            <p>
                              Looks like there's nothing in your listing yet!
                            </p>
                            <p>
                              See what's being created and collected in the{" "}
                              <a href="/market">activity feed</a>
                            </p>
                          </div>
                        </div>
                      </section>
                    </>
                  </div>
                </>
              )}
            </>
          )}
        </div>
      </div>
    </div>
  );
}

export default ArtistProfile;
