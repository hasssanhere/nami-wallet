import React, { useEffect, useState } from "react";

// import Header from "../partials/Header";
import HeroHome from "../Home/Content/HeroHome";
// import Footer from "../partials/Footer";
import HomeCarousel from "../Home/Content/HomeCarousel";
import HomeCollectArt from "../Home/Content/HomeCollectArt";
// import HomeRecentActivity from "../partials/Home/HomeRecentActivity";
import HomeHowitWorks from "../Home/Content/HomeHowitWorks";
// import HomeMarketMove from "../partials/Home/HomeMarketMove";

import "../../App/Assets/css/css/Home.css";
import "react-slideshow-image/dist/styles.css";
import TopArtist from "./Content/TopArtist";
import { getfeaturedNft } from "../../Services/HomeServices";

function Home() {
  const [data, setData] = useState([]);

  useEffect(() => {
    getfeaturedNft(setData);
  }, []);
  return (
    <>
      <HeroHome data={data} />
      <HomeCollectArt />

      <HomeCarousel data={data} />

      {/* <HomeHowitWorks /> */}
      <TopArtist />
    </>
  );
}

export default Home;
