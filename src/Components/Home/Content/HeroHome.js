import React, { useState } from "react";
import { Link } from "react-router-dom";

import { Fade } from "react-slideshow-image";
// import image1 from "./../../images/image1.jpg";
import image1 from "../../../App/Assets/images/image1.jpg";

import { Skeleton } from "@mui/material";

function HeroHome({ data }) {
  const [index, setIndex] = useState(0);

  //Slider Things Slider
  const [previousIndex, setPreviousIndex] = useState(null);
  const [nextIndex, setNextIndex] = useState(null);

  const style = {
    width: "580px",
    height: "580px",
    borderRadius: "25px 25px",
  };

  const properties =
    data?.filter((item) => item.mainBannerPrice)?.length > 1
      ? {
          autoplay: true,
          indicators: true,
          arrows: false,
          onChange: (previous, next) => {
            setPreviousIndex(previous);
            setNextIndex(next);
          },
        }
      : {
          autoplay: false,
          indicators: false,
          arrows: false,
          // onChange: (previous, next) => {
          //   setPreviousIndex(previous);
          //   setNextIndex(next);
          // },
        };

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };
  return (
    <section class="mt-20 ">
      <div class="container px-4 mx-auto">
        <div
          class="lg:flex lg:flex-wrap lg:m-24 lg:items-center  lg:-ml-5 "
          style={{
            marginTop: "5rem",
          }}
        >
          <div class="w-full lg:w-1/2 px-5">
            <div class="max-w-md m-auto">
              <h2 className=" fontFamily  text-2xl lg:text-4xl   mb-4 font-bold font-heading text-center">
                Invest in Spiritual Digital Art{" "}
              </h2>
              <p className="fontFamily text-center ">
                Buy and Sell NFT's from the World's top artist
              </p>
              <h2 className=" fontFamily text-4xl font-bold font-heading text-center">
                <Link
                  // className=" fontFamily text-4xl  font-bold font-heading text-center"
                  to="/market"
                >
                  <button
                    // data-aos="zoom-y-out"
                    style={{ width: "220px", height: "45px" }}
                    className="mt-8 mb-8 btn btn-big text-center "
                  >
                    <strong style={{ fontSize: "15px" }}>
                      Start Exploring NFT's
                    </strong>
                  </button>
                </Link>
              </h2>
              <Link to="/market">
                <p className="fontFamily text-center text-black hover:font-bold mb-8">
                  Get Featured on HomePage
                </p>
              </Link>
            </div>
          </div>
          <div class=" lg:w-1/2  sm:w-full ">
            {data?.length !== 0 ? (
              <Fade {...properties}>
                {data
                  ?.filter((item) => item.mainBannerPrice)
                  ?.map((item) => {
                    return (
                      <Link
                        style={{
                          textDecoration: "none",
                          color: "inherit",
                        }}
                        to={{
                          pathname: `/artworkdetailed/${item._id}?nftId=${item._id}&userId=${item.artistId._id}`,
                          state: {
                            nftId: item._id,
                            userId: item.artistId._id,
                          },
                        }}
                      >
                        <div
                          className="bg-contain  bg-no-repeat bg-center n-mainImage"
                          style={{
                            ...style,
                            // width: "100%",
                            backgroundImage: ` url(${item?.gatewayLink})`,
                          }}
                        ></div>
                      </Link>
                    );
                  })}
              </Fade>
            ) : (
              <Skeleton
                animation="wave"
                variant="rectangular"
                width="100%"
                height={580}
                className=" rounded-3xl"
              />
            )}
          </div>
        </div>
      </div>
    </section>
  );
}

export default HeroHome;
