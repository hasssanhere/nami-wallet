import React from "react";
import { Link } from "react-router-dom";
function HomeCollectArt() {
  return (
    <section class="mt-2 ">
      <div class="container px-1 ">
        <div class="lg:grid lg:grid-cols-3 md:grid md:grid-cols-3 sm:grid sm:grid-flow-row sm:auto-rows-max gap-10   text-left">
          <div className="sm:col-span-3 md:col-span-1 lg:col-span-1 p-6 border my-3 my-lg-0 rounded-md">
            <h4 className="text-gray-900 text-4xl	fontFamily font-semibold	mb-3">
              The future of art collection
            </h4>
            <p className="fontFamily">
              Browse and build your collection of the world's most cutting-edge
              digital art
            </p>
          </div>

          <div className="sm:col-span-3 md:col-span-1 lg:col-span-1 p-6 border my-3 my-lg-0 rounded-md">
            <h4 className="text-gray-900 text-4xl	 fontFamily font-semibold  mb-3">
              Pioneering art market royalties
            </h4>
            <p className="fontFamily">
              Artists receive continuous royalties for all secondary sales on
              their artworks forever
            </p>
          </div>

          <div className="sm:col-span-3 md:col-span-1 lg:col-span-1 p-6 border my-3 my-lg-0 rounded-md">
            <h4 className="text-gray-900 text-4xl	fontFamily font-semibold mb-3">
              Built for longevity
            </h4>
            <p className="fontFamily">
              All transactions happen on-chain, creating a tamper-proof record
              of each artwork's
            </p>
          </div>
        </div>
      </div>
    </section>
  );
}
export default HomeCollectArt;
