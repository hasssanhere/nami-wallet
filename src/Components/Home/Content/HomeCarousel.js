import React from "react";
// import Carousel from "react-multi-carousel";
import styled from "styled-components";

import Carousel from "react-grid-carousel";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";

import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
// import { refresh } from "aos";
import { Link } from "react-router-dom";
import { Skeleton } from "@mui/material";

function HomeCarousel({ data }) {
  const responsive = {
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3,
      paritialVisibilityGutter: 60,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 1,
      paritialVisibilityGutter: 50,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
      paritialVisibilityGutter: 30,
    },
  };

  const ArrowBtn = styled.span`
    display: inline-block;
    z-index: 10;
    position: absolute;
    top: 40%;
    right: ${({ type }) => (type === "right" ? "-35px" : "unset")};
    left: ${({ type }) => (type === "left" ? "-35px" : "unset")};
    width: 72px;
    height: 72px;
    background: #fff;

    border-radius: 50%;
    box-shadow: 0 3px 10px rgba(0, 0, 0, 0.15);
    cursor: pointer;
    &::after {
      content: "";
      display: inline-block;
      position: absolute;
      top: 50%;
      left: 50%;
      transform: ${({ type }) =>
        type === "right"
          ? "translate(-75%, -50%) rotate(45deg)"
          : "translate(-25%, -50%) rotate(-135deg)"};
      width: 12px;
      height: 12px;
      border-color: #333;
      border-top: 4px solid black;
      border-right: 4px solid black;
    }
    &:hover {
      border-color: #fff;
      background: #000;
    }
    &:hover::after {
      border-color: #fff;
    }
  `;
  return (
    <div class="mt-20 mb-5 w-nft-home" style={{ backgroundColor: "#f5f5f5" }}>
      <div class="container px-4 py-4 m-auto">
        <div className=" font-Raleway grid grid-flow-row auto-rows-max gap-5 text-left">
          <div className="sm:col-span-3 md:col-span-3 lg:col-span-3 ">
            <h1 className=" pt-16 font-Raleway font-bold text-black text-4xl text-break">
              Featured Artwork
            </h1>
          </div>
          <div className="sm:col-span-3 md:col-span-3 lg:col-span-2 mb-12">
            <p className="font-Raleway text-xl	text-gray-600">
              Presearch has an innovative go-to-market strategy to target the
              most frequent searchers - web workers - and gain early adoption,
              on our way to releasing future versions of the open source
              platform
            </p>
          </div>
        </div>
        {/* {console} */}
        {data.length !== 0 ? (
          <>
            <Carousel
              cols={4}
              rows={1}
              gap={11}
              showDots
              loop
              arrowRight={data?.length >= 4 ? <ArrowBtn type="right" /> : <></>}
              arrowLeft={data?.length >= 4 ? <ArrowBtn type="left" /> : <></>}
              responsiveLayout={[
                {
                  breakpoint: 1200,
                  cols: 3,
                },
                {
                  breakpoint: 1050,
                  cols: 2,
                },

                {
                  breakpoint: 700,
                  cols: 1,
                },
              ]}
              mobileBreakpoint={670}
            >
              {data
                ?.filter((item) => item?.secondarySliderPrice)
                ?.map((items) => {
                  return (
                    <Carousel.Item>
                      <Link
                        style={{
                          textDecoration: "none",
                          color: "inherit",
                        }}
                        to={{
                          pathname: `/artworkdetailed/${items._id}?nftId=${items._id}&userId=${items.artistId._id}`,
                          state: {
                            nftId: items._id,
                            userId: items.artistId._id,
                          },
                        }}
                      >
                        <Card
                          style={{
                            backgroundColor: "#FfFfFf",
                            borderRadius: "10px",
                          }}
                        >
                          <CardContent style={{ padding: "20px 15px" }}>
                            <CardMedia
                              component="img"
                              image={items?.gatewayLink}
                              onError={(e) => {
                                /**
                                 * Any code. For instance, changing the `src` prop with a fallback url.
                                 * In our code, I've added `e.target.className = fallback_className` for instance.
                                 */
                                e.target.src =
                                  "https://mpama.com/wp-content/uploads/2017/04/default-image-620x600.jpg";
                              }}
                              sx={{ minHeight: 280, borderRadius: "10px" }}
                              style={{
                                height: 280,
                                objectFit: "contain",
                                width: "100%",
                                borderRadius: "10px",
                              }}
                            />
                          </CardContent>
                          <CardContent>
                            <div className=" font-Raleway grid grid-cols-2 text-left">
                              <div className=" font-Raleway grid grid-cols-1 text-left">
                                <div className="sm:col-span-1 md:col-span-1 lg:col-span-1 text-left">
                                  <p
                                    className="font-bold text-base	text-black-100 text-break	"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title={items?.title}
                                  >
                                    {items?.title?.length > 13 ? (
                                      <>
                                        {`${items?.title?.substring(
                                          0,
                                          13
                                        )}...` || " "}
                                      </>
                                    ) : (
                                      <>{`${items?.title}` || " "}</>
                                    )}
                                  </p>
                                </div>
                              </div>
                              <div className="sm:col-span-1 md:col-span-1 lg:col-span-1 text-right">
                                <Link
                                  to={{
                                    pathname: `/artist-detail/${items?.artistId?.username}`,
                                    state: {
                                      userId: items.artistId._id,
                                    },
                                  }}
                                  className="font-normal	text-sm text-blue-700 text-break"
                                  data-toggle="tooltip"
                                  data-placement="top"
                                  title={items?.artistId?.username}
                                >
                                  @
                                  {items?.artistId?.username?.length > 10 ? (
                                    <>
                                      {`${items?.artistId?.username?.substring(
                                        0,
                                        10
                                      )}...` || " "}
                                    </>
                                  ) : (
                                    <>{`${items?.artistId?.username}` || " "}</>
                                  )}
                                  <CheckCircleIcon
                                    style={{ fontSize: "20", color: "#367BF5" }}
                                  />
                                </Link>
                              </div>
                            </div>
                            <div className="sm:col-span-2 md:col-span-2 lg:col-span-2 text-left">
                              <Link
                                // to={`/artist-detail/${nft.artistId}`}
                                className="font-normal	text-sm text-black-100 text-break"
                                data-toggle="tooltip"
                                data-placement="top"
                                title={items?.collectionId?.collectionName}
                              >
                                {items?.collectionId?.collectionName?.length >
                                15 ? (
                                  <>
                                    {`${items?.collectionId?.collectionName?.substring(
                                      0,
                                      15
                                    )}...` || " "}
                                  </>
                                ) : (
                                  <>
                                    {`${items?.collectionId?.collectionName}` ||
                                      " "}
                                  </>
                                )}
                              </Link>
                            </div>
                            <div className=" font-Raleway grid grid-cols-2 text-left">
                              <div className="sm:col-span-1 md:col-span-1 lg:col-span-1 text-left">
                                <p
                                  className="font-bold text-base	text-black-100 text-break font-roboto	"
                                  data-toggle="tooltip"
                                  data-placement="top"
                                  title={items?.price}
                                >
                                  ADA {items?.price}
                                </p>
                              </div>
                            </div>
                          </CardContent>
                        </Card>
                      </Link>
                    </Carousel.Item>
                  );
                })}
            </Carousel>
          </>
        ) : (
          <div>
            <div
              style={{ display: "flex", width: "100%", flexDirection: "row" }}
            >
              {[1, 2, 3, 4].map(() => (
                <div style={{ width: "33%", margin: "1rem" }}>
                  <Skeleton
                    animation="wave"
                    variant="rectangular"
                    width="100%"
                    height={300}
                  />
                  <Skeleton width="100%" />
                  <Skeleton width="100%" />
                  <Skeleton width="100%" />
                  <Skeleton width="100%" />
                </div>
              ))}
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

export default HomeCarousel;
