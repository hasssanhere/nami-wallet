import React, { useEffect, useState } from "react";
// import Carousel from "react-multi-carousel";
import styled from "styled-components";
import { Skeleton } from "@mui/material";
import { Box } from "@mui/system";

import Carousel from "react-grid-carousel";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import { getTopArtists } from "../../../Services/artistSevices";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import { Link } from "react-router-dom";

function HomeCarousel() {
  const [data, setData] = useState([]);

  const images = [
    {
      image:
        "https://images.pexels.com/photos/7249183/pexels-photo-7249183.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
      text: "Islamic Art 1",
    },
    {
      image:
        "https://images.pexels.com/photos/36704/pexels-photo.jpg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
      text: "Quran Aesthetic",
    },
    {
      image:
        "https://images.pexels.com/photos/7261977/pexels-photo-7261977.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
      text: "Islamic Art HD",
    },
    {
      image:
        "https://images.pexels.com/photos/2427797/pexels-photo-2427797.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
      text: "Mosque",
    },
    {
      image:
        "https://images.pexels.com/photos/3163677/pexels-photo-3163677.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
      text: "Wazir Khan Mosque",
    },
  ];
  useEffect(() => {
    getTopArtists(setData);
  }, []);
  const ArrowBtn = styled.span`
    display: inline-block;
    z-index: 10;
    position: absolute;
    top: 40%;
    right: ${({ type }) => (type === "right" ? "-35px" : "unset")};
    left: ${({ type }) => (type === "left" ? "-35px" : "unset")};
    width: 72px;
    height: 72px;
    background: #fff;

    border-radius: 50%;
    box-shadow: 0 3px 10px rgba(0, 0, 0, 0.15);
    cursor: pointer;
    &::after {
      content: "";
      display: inline-block;
      position: absolute;
      top: 50%;
      left: 50%;
      transform: ${({ type }) =>
        type === "right"
          ? "translate(-75%, -50%) rotate(45deg)"
          : "translate(-25%, -50%) rotate(-135deg)"};
      width: 12px;
      height: 12px;
      border-color: #333;
      border-top: 4px solid black;
      border-right: 4px solid black;
    }
    &:hover {
      border-color: #fff;
      background: #000;
    }
    &:hover::after {
      border-color: #fff;
    }
  `;
  return (
    <>
      <div class="container w-nft-home px-4 py-3 m-auto">
        <div className=" font-Raleway grid grid-flow-row auto-rows-max gap-5 text-left">
          <div className="sm:col-span-3 md:col-span-3 lg:col-span-3 ">
            <h1 className=" pt-16 font-Raleway font-bold text-black text-4xl text-break">
              Top Author
            </h1>
          </div>
          <div className="sm:col-span-3 md:col-span-3 lg:col-span-2 mb-12">
            <p className="font-Raleway text-xl	text-gray-600">
              Presearch has an innovative go-to-market strategy to target the
              most frequent searchers - web workers - and gain early adoption,
              on our way to releasing future versions of the open source
              platform
            </p>
          </div>
        </div>
        {data?.length !== 0 ? (
          <Carousel
            cols={4}
            rows={1}
            gap={11}
            showDots
            loop
            arrowRight={data?.length >= 4 ? <ArrowBtn type="right" /> : <></>}
            arrowLeft={data?.length >= 4 ? <ArrowBtn type="left" /> : <></>}
            responsiveLayout={[
              {
                breakpoint: 1200,
                cols: 3,
              },
              {
                breakpoint: 1050,
                cols: 2,
              },

              {
                breakpoint: 700,
                cols: 1,
              },
            ]}
            mobileBreakpoint={670}
          >
            {data?.map((image) => {
              return (
                <Carousel.Item>
                  <Link
                    to={{
                      pathname: `/artist-detail/${image?.username}`,
                      state: {
                        userId: image?._id,
                      },
                    }}
                    className="font-normal	text-sm text-black-100 text-break"
                  >
                    <Card
                      style={{
                        backgroundColor: "#f5f5f5",
                        borderRadius: "10px",
                      }}
                    >
                      <CardContent style={{ padding: "20px 15px" }}>
                        <CardMedia
                          component="img"
                          image={image.image}
                          onError={(e) => {
                            /**
                             * Any code. For instance, changing the `src` prop with a fallback url.
                             * In our code, I've added `e.target.className = fallback_className` for instance.
                             */
                            e.target.src =
                              "https://media.istockphoto.com/vectors/male-profile-flat-blue-simple-icon-with-long-shadow-vector-id522855255?k=20&m=522855255&s=612x612&w=0&h=fLLvwEbgOmSzk1_jQ0MgDATEVcVOh_kqEe0rqi7aM5A=";
                          }}
                          alt="green iguana"
                          sx={{ minHeight: 280, borderRadius: "10px" }}
                          style={{
                            height: 280,
                            objectFit: "cover",
                            width: "100%",
                            borderRadius: "10px",
                          }}
                        />
                      </CardContent>
                      <CardContent>
                        <div className="sm:col-span-1 md:col-span-1 lg:col-span-1 text-center">
                          <Link
                            to={{
                              pathname: `/artist-detail/${image?.username}`,
                              state: {
                                userId: image?._id,
                              },
                            }}
                            className="font-normal	text-sm text-blue-700 text-break"
                          >
                            @{image.username}{" "}
                            <CheckCircleIcon
                              style={{ fontSize: "20", color: "#367BF5" }}
                            />
                          </Link>
                        </div>
                      </CardContent>
                    </Card>
                  </Link>
                </Carousel.Item>
              );
            })}
          </Carousel>
        ) : (
          <div>
            <div
              style={{ display: "flex", width: "100%", flexDirection: "row" }}
            >
              {[1, 2, 3, 4].map(() => (
                <div style={{ width: "33%", margin: "1rem" }}>
                  <Skeleton
                    animation="wave"
                    variant="rectangular"
                    width="100%"
                    height={300}
                  />
                  <Skeleton width="100%" />
                  <Skeleton width="100%" />
                  <Skeleton width="100%" />
                  <Skeleton width="100%" />
                </div>
              ))}
            </div>
          </div>
        )}
      </div>
    </>
  );
}

export default HomeCarousel;
