import React, { useEffect, useState } from "react";
import TextField from "@mui/material/TextField";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
import { createTheme } from "@mui/material/styles";
import PersonIcon from "@mui/icons-material/Person";
import { Signup } from "../../../Services/artistSevices";
import "../../../App/Assets/css/css/SignupSignin.css";
import Swal from "sweetalert2";
import { useHistory } from "react-router";
import { CircularProgress, Switch } from "@mui/material";
import { Modal } from "react-bootstrap";
import EditIcon from "../../../App/Assets/images/SVGIcons/EditIcon";
import EmailIcon from "../../../App/Assets/images/SVGIcons/EmailIcon";
import LockIcon from "../../../App/Assets/images/SVGIcons/LockIcon";
import InputAdornment from "@mui/material/InputAdornment";

import ArtistLogin from "../SignIn/Index";

function CreateArtistProfile(props) {
  const { setModalSignIn, setModalSignUp } = props;
  const [btnLoading, setBtnLoading] = useState(false);

  const [signup, setSignUp] = useState({
    // fname: "",
    // lname: "",
    flname: "",
    email: "",
    password: "",
    // walletaddress: "",
    role: "artist",
    username: "",
  });

  const {
    // fname,
    flname,
    email,
    password,
    // walletaddress,
    role,
    username,
  } = signup;

  const theme = createTheme();

  const selectingHandler = (e) => {
    if (e.target.checked) {
      setSignUp({ ...signup, [e.target.name]: "collector" });
    } else {
      setSignUp({ ...signup, [e.target.name]: "artist" });
    }
  };
  const changeHandler = (e) => {
    setSignUp({ ...signup, [e.target.name]: e.target.value });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const formdata = {
      ...signup,
      // walletaddress: sessionStorage?.getItem("walletAddress"),
    };

    // if (sessionStorage.getItem("walletAddress")) {
    if (email == "" || password == "") {
      Swal.fire({
        icon: "error",
        text: "Kindly Fill all required Fields",
      });
    } else {
      Signup(formdata, setModalSignUp);
    }
    // } else {
    //   Swal.fire({
    //     toast: true,
    //     icon: "error",
    //     text: "Kindly Connect a wallet before signup ",
    //   });
    // }
    setBtnLoading(true);
    //Faking API call here
    setTimeout(() => {
      setBtnLoading(false);
    }, 3000);
  };

  ////--------------------------------------------------------------------------------

  const onSignupclick = () => {
    setModalSignUp(false);
    setModalSignIn(true);
    setSignUp({
      flname: "",
      email: "",
      password: "",
      username: "",
    });
  };
  const onHideFunction = () => {
    setModalSignUp(false);
    setSignUp({
      flname: "",
      email: "",
      password: "",
      username: "",
    });
  };
  const fetchData = () => {
    setBtnLoading(true);
    //Faking API call here
    setTimeout(() => {
      setBtnLoading(false);
    }, 3000);
  };
  const handleKeypress = (e) => {
    //it triggers by pressing the enter key
    if (e.keyCode === 13 || e.key == "Enter") {
      handleSubmit(e);
      fetchData();
    }
  };
  return (
    <div>
      <Modal
        {...props}
        onHide={() => onHideFunction()}
        size="md"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Body>
          <div className="w-nft-sign-form">
            <p className="modalTop pl-5"> Sign up to NoorNFT</p>
            <Grid item xs={12} className=" text-center">
              <Switch name="role" onChange={selectingHandler} />
            </Grid>
            <div item xs={12} className=" d-flex justify-content-center">
              <p className="mx-4 font-bold ">Artist</p>
              <p className="mx-4 font-bold ">Buyer</p>
            </div>
            <Grid item xs={12} className="pl-5 pr-5">
              <TextField
                required
                fullWidth
                id="flname"
                label="Enter your full name"
                name="flname"
                placeholder="Full Name"
                autoComplete="flname"
                value={flname}
                onChange={changeHandler}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <EditIcon />
                    </InputAdornment>
                  ),
                }}
                required
                className="inputStyling wmb-15 mt-5"
              />
            </Grid>
            <Grid item xs={12} className="pl-5 pr-5">
              <TextField
                required
                fullWidth
                id="username"
                label="Enter your user name"
                placeholder="Username"
                name="username"
                autoComplete="username"
                value={username}
                onChange={changeHandler}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <PersonIcon />
                    </InputAdornment>
                  ),
                }}
                required
                className="inputStyling wmb-15"
              />
            </Grid>
            <Grid item xs={12} className="pl-5 pr-5">
              <TextField
                required
                fullWidth
                id="email"
                label="Enter your Email"
                name="email"
                placeholder="Email Address"
                autoComplete="email"
                value={email}
                onChange={changeHandler}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <EmailIcon />
                    </InputAdornment>
                  ),
                }}
                // value={email}
                // onChange={changeHandler}
                required
                className="inputStyling wmb-15"
              />
            </Grid>
            <Grid item xs={12} className="pl-5 pr-5">
              <TextField
                className="ctmfontFamily"
                required
                fullWidth
                onKeyUp={(e) => handleKeypress(e)}
                name="password"
                placeholder="Password"
                label="Enter your Password"
                type="password"
                id="password"
                value={password}
                autoComplete="new-password"
                onChange={changeHandler}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <LockIcon />
                    </InputAdornment>
                  ),
                }}
                // value={password}
                autoComplete="new-password"
                // onChange={changeHandler}
                required
                className="inputStyling wmb-15"
              />
            </Grid>
            <Grid item xs={12} className="pl-5 pr-5">
              <button
                // className=" btn btn-default btn-block bg-gray-900 ml-3  hover:bg-gray-900 text-white font-bold py-2 px-4 rounded inline-flex items-center mt-3 text-justify"
                className=" mt-2 btn-block btn btn-primary w-100"
                style={{
                  backgroundColor: "black",
                  borderColor: "black",
                }}
                type="submit"
                // fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2, bg: "#000000" }}
                onClick={
                  (() => {
                    Swal.fire({
                      icon: "success",
                      text: "Please Wait for admin to approve",
                      showConfirmButton: false,

                      timer: 5000,
                      timerProgressBar: true,
                      didOpen: (toast) => {
                        toast.addEventListener("mouseenter", Swal.stopTimer);
                        toast.addEventListener("mouseleave", Swal.resumeTimer);
                      },
                    });
                  },
                  handleSubmit)
                }
              >
                {btnLoading && <CircularProgress size={20} className="mr-3" />}
                {btnLoading && <span>Loading </span>}
                {!btnLoading && <span> Sign Up</span>}
              </button>
            </Grid>
            <Grid container justifyContent="flex-start" className="mt-3 pr-5">
              <Grid item>
                <p className="footermodal pl-5">
                  Already have an account?{"  "}
                  <Link
                    onClick={onSignupclick}
                    className="footermodal text-black font-bold"
                    style={{ textDecoration: " none ", cursor: "pointer" }}
                  >
                    Log in
                  </Link>
                </p>
              </Grid>
            </Grid>
          </div>
        </Modal.Body>
      </Modal>
    </div>
  );
}

export default CreateArtistProfile;
