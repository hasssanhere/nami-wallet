import React, { useState } from "react";
import TextField from "@mui/material/TextField";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
// import Header from "../partials/Header";
// import { Signin } from "../Utils/artistSevices";
import InputAdornment from "@mui/material/InputAdornment";
import EmailIcon from "../../../App/Assets/images/SVGIcons/EmailIcon";
import LockIcon from "../../../App/Assets/images/SVGIcons/LockIcon";
import { Signin } from "../../../Services/artistSevices";
import Swal from "sweetalert2";
import { Modal } from "react-bootstrap";
import { CircularProgress } from "@mui/material";
function ArtistLogin(props) {
  // const [modalShow, setModalShow] = useState(false);
  const { setModalSignIn, setModalSignUp } = props;
  const [signin, setSignIn] = useState({
    email: "",
    password: "",
  });
  const [btnLoading, setBtnLoading] = useState(false);
  const { email, password } = signin;
  const changeHandler = (e) => {
    setSignIn({ ...signin, [e.target.name]: e.target.value });
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    if (email == "") {
      Swal.fire({
        toast: true,
        icon: "error",
        text: "Kindly Provide your Email address ",
        showConfirmButton: false,
      });
    } else if (password == "") {
      Swal.fire({
        toast: true,
        icon: "error",
        text: "Password Required",
        showConfirmButton: false,
      });
    } else {
      const formdata = {
        email,
        password,
      };
      Signin(formdata, setModalSignIn);
    }
  };

  const onLoginclick = async () => {
    setModalSignIn(false);
    setModalSignUp(true);
    setSignIn({
      email: "",
      password: "",
    });
    // if (sessionStorage.getItem("walletAddress")) {

    // } else {
    //   Swal.fire({
    //     toast: true,
    //     icon: "error",
    //     text: "Kindly Connect a wallet before signup ",
    //   });
    // }
  };

  const onHideFunction = () => {
    setModalSignIn(false);
    setSignIn({
      email: "",
      password: "",
    });
  };
  // if (setModalSignIn(false)) {
  //   setSignIn({ email: "", password: "" });
  // }
  const fetchData = () => {
    setBtnLoading(true);
    //Faking API call here
    setTimeout(() => {
      setBtnLoading(false);
    }, 3000);
  };
  const handleKeypress = (e) => {
    //it triggers by pressing the enter key
    if (e.keyCode === 13 || e.key == "Enter") {
      handleSubmit(e);
      fetchData();
    }
  };
  return (
    <div>
      {/* {props.modalShoww == true && ( */}
      <Modal
        {...props}
        onHide={() => onHideFunction()}
        // show={props.modalShoww}
        size="md"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Body>
          <div className="w-nft-sign-form">
            <p className="modalTop mb-4 pl-5"> Log in to NoorNFT</p>
            <Grid item xs={12} className="pl-5 pr-5">
              <TextField
                fullWidth
                id="email"
                name="email"
                placeholder="Email Address  "
                autoComplete="email"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <EmailIcon style={{ Width: "20.04px", Height: "20px" }} />
                    </InputAdornment>
                  ),
                }}
                value={email}
                onChange={changeHandler}
                required
                className="inputStyling wmb-15"
              />
            </Grid>
            <Grid item xs={12} className="pl-5 pr-5">
              <TextField
                required
                fullWidth
                name="password"
                label="Enter your Password"
                type="password"
                placeholder="Password"
                id="password"
                value={password}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <LockIcon style={{ Width: "20.04px", Height: "20px" }} />
                    </InputAdornment>
                  ),
                }}
                value={password}
                autoComplete="new-password"
                onChange={changeHandler}
                required
                onKeyUp={(e) => handleKeypress(e)}
                className="inputStyling wmb-15"
              />
            </Grid>
            <Grid item xs={12} className="pl-5 pr-5">
              <button
                // className=" btn btn-default btn-block bg-gray-900 ml-3  hover:bg-gray-900 text-white font-bold py-2 px-4 rounded inline-flex items-center mt-3 text-justify"
                className=" mt-2 btn-block btn btn-primary w-100"
                style={{
                  backgroundColor: "black",
                  borderColor: "black",
                }}
                type="submit"
                onClick={(e) => {
                  handleSubmit(e);
                  fetchData();
                }}
                variant="contained"
                sx={{ mt: 3, mb: 2, bg: "#000000" }}
                // onClick={handleSubmit}
              >
                {btnLoading && <CircularProgress size={20} className="mr-3" />}
                {btnLoading && <span>Loading </span>}
                {!btnLoading && <span> Log in</span>}{" "}
              </button>
            </Grid>
            <Grid
              container
              justifyContent="space-between"
              className="mt-3 pl-5 pr-5"
            >
              <FormControlLabel
                control={<Checkbox value="remember" color="primary" />}
                label="Remember me"
              />
              <p className="footermodal">
                New to NoorNFT?{"  "}
                <Link
                  onClick={onLoginclick}
                  className="footermodal text-black font-bold"
                  style={{ textDecoration: "none", cursor: "pointer" }}
                >
                  Sign Up
                </Link>
              </p>
            </Grid>
          </div>
        </Modal.Body>
      </Modal>
    </div>
  );
}
export default ArtistLogin;
