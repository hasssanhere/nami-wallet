import React from "react";
// import Footer from "../partials/Footer";
// import Header from "../partials/Header";
import Index from "./NFTGuide";

const NFTGuide = () => {
  return (
    <>
      <div className="max-w-6xl  text-center mt-10 mx-auto px-4 sm:px-6">
        {/* Top area: Blocks */}
        <div className=" gridsm:grid-cols-12 gap-6 py-20 md:py-12">
          <h1
            className="font-extrabold text-5xl text-black"
            data-aos="zoom-y-out"
          >
            NFT Guide
          </h1>
        </div>
      </div>
      {/* Start*/}
      <Index />
    </>
  );
};

export default NFTGuide;
