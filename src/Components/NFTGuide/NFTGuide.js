import React from "react";
import { Typography } from "@material-ui/core";
import { Box } from "@mui/system";

const Index = () => {
  return (
    <>
      <div className="container ">
        <Typography variant="h4" component="h1" className=" font-bold">
          Explaining NFTs
        </Typography>

        <div className="grid grid-cols-2 gap-4 items-center">
          <div className="col-span-1">
            <Typography variant="h5" component="h2" className=" font-semibold">
              Non-Fungible Token{" "}
            </Typography>
            <p className="text-justify">
              A non-fungible token most commonly known as NFT, is a unique
              digital certificate stored on a blockchain. A non-fungible token
              basically describes something that is unique and cannot be
              interchanged. This digital certificate is a record of ownership
              rights in an asset which in most cases is a digital asset but can
              be a combination of a digital and a physical asset known as a
              phygital asset. Art can be in the form of a phygital asset when
              the physical item (an original painting) is digitised into its NFT
              form. The digitised version can then exist in the virtual world
              and serve as a record of ownership for that item in the real
              world.{" "}
            </p>
          </div>

          <Box className="my-4 col-span-1 rounded">
            <img
              className=" object-cover rounded"
              src="https://thumbor.forbes.com/thumbor/fit-in/1200x0/filters%3Aformat%28jpg%29/https%3A%2F%2Fspecials-images.forbesimg.com%2Fimageserve%2F60bfce137d2a8d637eab4850%2F0x0.jpg"
            />
          </Box>
        </div>
      </div>

      <div className="container mx-auto mb-8">
        <div className="grid grid-cols-2 gap-4">
          <div className="col-span-2">
            <Typography
              variant="h5"
              component="h2"
              className="my-2 font-semibold"
            >
              Blockchain{" "}
            </Typography>
            <p className="text-justify">
              In order to understand NFTs it is important to understand
              blockchain. A blockchain is a distributed ledger of transactions
              that is not controlled by a central authority; therefore, it is
              known as a decentralised database. There are many different
              blockchain ledgers that exist today. Transactions added to a
              ledger cannot be modified or erased. In order to validate and
              record transactions a blockchain relies on a distributed
              peer-to-peer computer network. By their very nature blockchain
              ledgers are fully transparent and viewable to anyone.{" "}
            </p>
          </div>
        </div>
      </div>

      <div className="container mx-auto mb-8 ">
        <div className="grid grid-cols-2 gap-4 items-center">
          <Box className="my-4 col-span-1 rounded">
            <img
              className="w-full object-cover rounded"
              src="https://miro.medium.com/max/1400/1*B0Cp9L-10E0pcVc-q3uNXg.png"
            />
          </Box>
          <div className="col-span-1">
            <Typography
              variant="h5"
              component="h2"
              className="my-2 font-semibold"
            >
              Minting{" "}
            </Typography>
            <p className="text-justify">
              To create a record of the NFT on the blockchain the NFT needs to
              be minted. An NFT is minted using a smart contract which is a
              digital contract stored on the blockchain. This contract contains
              underlying information knowns as metadata which can include the
              name of the art work, the TokenID, which is the unique identifier
              for the NFT, an identifier of where the digital art associated
              with the NFT may be found, the blockchain address of the current
              owner and the blockchain address of each owner since the creation,
              or minting of the NFT. Other useful information can also be noted
              here.{" "}
            </p>
          </div>
        </div>
      </div>
      <div className="container mx-auto mb-8">
        <div className="grid grid-cols-2 gap-4">
          <div className="col-span-2">
            <Typography
              variant="h5"
              component="h2"
              className="my-4 font-semibold"
            >
              Digital Wallet{" "}
            </Typography>
            <p className="text-justify">
              In order to purchase an NFT, the buyer must have a digital wallet.
              A digital wallet is a unique alphanumeric string of characters
              that is associated with and represents the on-chain address for a
              digital wallet.
              <br /> NFTs rely on the real-world principle of the buying and
              selling of an item, the only difference being they are done as a
              blockchain transaction using cryptocurrencies as their currency,
              transferring the NFT from the seller to the buyer. The transaction
              is stored on the blockchain ledger along with all historical
              transactions relating to that NFT.
              <br /> The digital art associated with an NFT is normally not
              stored on a blockchain, this is what is known as off-chain. The
              information in the NFT includes a reference that points to where
              the file is stored off-chain. The digital art may be stored in a
              digital wallet, and in the case of the physical art the NFT can
              include information identifying who has physical ownership of
              this, which could be an individual or a gallery.{" "}
            </p>
          </div>
        </div>
      </div>

      <div className="max-w-16xl pb-5 mt-5  mx-5 md:mx-3  sm:px-6 mx-0 ">
        {/* Top area: Blocks */}
        <div className="grid sm:grid-cols-12 px-6 gap-8 py-8 md:py-12">
          {/* 1st block */}
          <div className="sm:col-span-12 lg:col-span-3">
            <div className="mb-2">
              <h1 className="text-4xl md:text-3xl my-2 font-extrabold  tracking-tighter mb-4">
                Why invest in NFTs?
              </h1>
            </div>
          </div>

          {/* 2nd block */}
          <div className="sm:col-span-6 md:col-span-3 lg:col-span-3">
            <h6 className="text-gray-800 text-xs font-extrabold mb-2">
              Authenticity
            </h6>
            <p className="text-sm">
              Once an NFT is minted on the blockchain it is a unique token that
              anyone can view and access to confirm the metadata, ownership
              history, TokenID, blockchain address and other underlying
              information. In addition, since transactions on a blockchain are
              publicly viewable, buyers can see the address from which the NFT
              was first minted
            </p>
          </div>

          {/* 3rd block */}
          <div className="sm:col-span-6 md:col-span-3 lg:col-span-3">
            <h6 className="text-gray-800 text-xs font-extrabold mb-2">
              Collectability
            </h6>
            <p className="text-sm">
              Purchase at the asking price or make an offer by placing a bid.
              Once you own a piece you can resell it in the secondary market to
              other collectors.
            </p>
          </div>

          {/* 4th block */}
          <div className="sm:col-span-6 md:col-span-3 lg:col-span-3">
            <h6 className="text-gray-800 text-xs font-extrabold mb-2">
              Rarity
            </h6>
            <p className="text-sm">
              Customize your profile to show off your art collection to patrons
              around the world. Display your works in a VR gallery, digital
              display, or anywhere else you like.
            </p>
          </div>
        </div>
      </div>
    </>
  );
};

export default Index;
