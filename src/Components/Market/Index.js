import React, { useState, useEffect } from "react";
// import Header from "../partials/Header";
// import Footer from "../partials/Footer";
import "react-dropdown/style.css";
import MainMarket from "../Market/Content/MainMarket";

function Market() {
  return (
    <>
      <div>
        <div className="max-w-6xl  text-center mt-32 mx-auto px-4 sm:px-6">
          {/* Top area: Blocks */}
          <div className=" gridsm:grid-cols-12 gap-8 py-18 md:py-10">
            <h1 className="font-extrabold text-5xl text-black">Marketplace</h1>
          </div>
          <MainMarket />
        </div>
      </div>
    </>
  );
}
export default Market;
