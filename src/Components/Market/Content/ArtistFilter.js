import React, { useEffect, useState } from "react";
import SearchIcon from "@mui/icons-material/Search";
import DoneIcon from "@mui/icons-material/Done";

import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import { Dropdown } from "react-bootstrap";
import {
  artisitSearch,
  multiSearchFilter,
} from "../../../Services/MarketServices";
import { flushSync } from "react-dom";

const ArtistFilter = ({
  allArtists,
  setAllArtists,
  showData,
  setShowdata,
  filterData,
  setFilterData,
  filterSearching,
}) => {
  const [value, setValue] = useState("");

  const { artistId } = filterData;
  const { showArtist } = showData;

  useEffect(() => {
    if (artistId == "") {
      filterSearching();
    }
  }, [artistId]);

  return (
    <>
      <Dropdown>
        <Dropdown.Toggle
          // className="capitalize w-full focus:bg-black-100 focus:border-transparent focus:shadow-none active:bg-black-100 active:shadow-none	active:focus:shadow-none	bg-white-200 text-black-100 hover:bg-black-100 hover:border-transparent relative"
          className={
            artistId
              ? "capitalize w-full bg-black-100 focus:bg-black-100 focus:border-transparent focus:shadow-none active:bg-black-100 active:shadow-none	active:focus:shadow-none hover:bg-black-100 hover:border-transparent"
              : "capitalize w-full focus:bg-black-100 focus:border-transparent focus:shadow-none active:bg-black-100 active:shadow-none	active:focus:shadow-none	bg-white-200 text-black-100 hover:bg-black-100 hover:border-transparent"
          }
          // id="dropdown-custom-components"
          // onClick={() =>
          //   setShowdata({
          //     showArtist: !showArtist,
          //     showCollection: false,
          //     showPrice: false,
          //     showsort: false,
          //   })
          // }
        >
          {/* {artistId ? (
            <div className="absolute p-2 bg-red-500 rounded-3xl top-0 right-0"></div>
          ) : (
            <></>
          )} */}
          Artist <KeyboardArrowDownIcon />
        </Dropdown.Toggle>
        {/* onClick={() => handleClick(event.eventRef, event.name)} */}
        <Dropdown.Menu renderOnMount={false} flip={false}>
          <div className="">
            <div className="relative">
              <input
                type="text"
                className="h-auto w-auto border-black border-2 rounded-2xl p-2 mx-2"
                placeholder="Search Artists..."
                autoFocus
                onChange={(e) => {
                  setValue(e.target.value);
                }}
                value={value}
              />
              <SearchIcon
                style={{
                  position: "absolute",
                  right: "20px",
                  top: "10px",
                }}
              />
            </div>

            {allArtists ? (
              <div className="container w-auto h-32 overflow-y-auto p-1 my-2 border-b-2	border-black pb-2">
                <li>
                  {allArtists
                    .filter(
                      (child) =>
                        !value ||
                        child?.flname?.toLowerCase().match(value.toLowerCase())
                    )
                    .map((items, index) => {
                      return (
                        <p
                          className="capitalize m-2 cursor-pointer hover:bg-gray-200"
                          eventKey={index + 1}
                          onClick={(e) => {
                            e.preventDefault();
                            setFilterData({
                              ...filterData,
                              artistId: items._id,
                            });
                          }}
                        >
                          <div className="flex">
                            <div className="w-5/6">{items.flname}</div>

                            {artistId == items._id ? <DoneIcon /> : null}
                          </div>
                        </p>
                      );
                    })}
                </li>
              </div>
            ) : (
              <div className="container w-auto h-32 overflow-y-auto p-1 my-2 border-b-2	border-black pb-2">
                <li>No Artist to display</li>
              </div>
            )}
            <div class="flex flex-row justify-center px-4 py-2">
              <button
                class="n-clearButton"
                onClick={async () => {
                  setFilterData({
                    ...filterData,
                    artistId: "",
                  });
                  setValue("");
                }}
              >
                Clear
              </button>
              <Dropdown.Item
                className="n-applyButton"
                onClick={() => {
                  filterSearching();
                  setShowdata({
                    ...showData,
                    showArtist: false,
                    showCollection: false,
                    showPrice: false,
                    showsort: false,
                  });
                }}
              >
                Apply
              </Dropdown.Item>
            </div>
          </div>
        </Dropdown.Menu>
      </Dropdown>
    </>
  );
};

export default ArtistFilter;
