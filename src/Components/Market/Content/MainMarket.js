import React, { useState, useEffect, useRef } from "react";
import SearchIcon from "@mui/icons-material/Search";
import "react-dropdown/style.css";
import "../../../App/Assets/css/css/check.css";

import Loader from "../../../App/Layout/Loader";
import { MDBCol, MDBInput } from "mdbreact";
// import { getAllNftsByState, getallprices } from "../../../Services/CreateNFT";
import { getAllArtists } from "../../../Services/artistSevices";
import {
  generalSearchFilter,
  getallCollections,
  getAllListedNFTs,
  multiSearchFilter,
} from "../../../Services/MarketServices";

import Content from "./Content";
import ArtistFilter from "./ArtistFilter";
import CollectionFilter from "./CollectionFilter";
import PriceFilter from "./PriceFilter";
import RecentlyListed from "./RecentlyListed";
import { CircularProgress, Skeleton } from "@mui/material";
import { useLocation } from "react-router-dom";

function MainMarket() {
  const location = useLocation();
  const [NFTData, setNFTData] = useState([]);
  const [allArtists, setAllArtists] = useState([]);

  const [allCollection, setAllCollection] = useState([]);
  const [btnLoading, setBtnLoading] = useState(false);

  const [showData, setShowdata] = useState({
    showArtist: false,
    showCollection: false,
    showPrice: false,
    showsort: false,
  });
  const [generalSearch, setGeneralSearch] = useState("");
  const [filterData, setFilterData] = useState({
    artistId: "",
    collectionId: "",
    minPrice: "",
    maxPrice: "",
    sort: "",
  });
  const { artistId, collectionId, minPrice, maxPrice, sort } = filterData;
  const [success, setSuccess] = useState(true);

  const [count, setCount] = useState({
    countMarket: 1,
    countGeneral: 0,
    countFilters: 0,
  });
  const [countButton, setCountButton] = useState({
    buttonMarket: false,
    buttonGeneral: false,
    buttonFilters: false,
  });

  const { buttonMarket, buttonGeneral, buttonFilters } = countButton;
  const [loading, setLoading] = useState(true);

  const { countMarket, countGeneral, countFilters } = count;

  // const countHnader = (props) => {
  //   setCount({ ...count, countGeneral: 1 });
  // };

  const generalSearchHandler = async () => {
    if (generalSearch) {
      setCount({
        ...count,
        countGeneral: 1,
        countMarket: 0,
        countFilters: 0,
      });
    } else {
      setCount({
        ...count,
        countGeneral: 0,
        countMarket: 1,
        countFilters: 0,
      });
    }
  };

  const filterSearching = () => {
    if (artistId || collectionId || maxPrice || sort) {
      setCount({
        ...count,
        countGeneral: 0,
        countMarket: 0,
        countFilters: 1,
      });

      multiSearchFilter({
        filterData,
        setFilterData,
        setNFTData,
        count,
        setCountButton,
      });
    } else {
      setCount({
        ...count,
        countGeneral: 0,
        countMarket: 1,
        countFilters: 0,
      });
    }
  };

  //-----------------------------------------------------------------------------
  useEffect(() => {
    getAllListedNFTs({ setNFTData, count, setCountButton, setLoading });
  }, []);

  useEffect(() => {
    if (countGeneral != 1 && countFilters != 1 && countMarket >= 1) {
      getAllListedNFTs({ setNFTData, count, setCountButton, setLoading });
    }
  }, [countMarket]);

  useEffect(() => {
    if (countMarket != 1 && countFilters != 1 && countGeneral >= 1) {
      generalSearchFilter({
        generalSearch,
        setNFTData,
        count,
        setCountButton,
      });
    }
  }, [countGeneral]);
  useEffect(() => {
    if (countMarket != 1 && countGeneral != 1 && countFilters >= 1) {
      multiSearchFilter({
        filterData,
        setNFTData,
        count,
        setCountButton,
        setSuccess,
      });
    }
  }, [countFilters]);

  useEffect(() => {
    getAllArtists(setAllArtists);
    getallCollections(setAllCollection);
  }, []);
  const fetchData = () => {
    setBtnLoading(true);
    //Faking API call here
    setTimeout(() => {
      setBtnLoading(false);
    }, 2000);
  };
  console.log(location);
  return (
    <>
      <div>
        <div className="max-w-2xl  text-center  mx-auto px-4 sm:px-6">
          {/* Top area: Blocks */}
          <div>
            <MDBCol md="12">
              <MDBInput
                hint="Search for Art, Collection or Artist..."
                id="Search"
                type="text"
                style={{
                  borderRadius: "40px",
                  backgroundColor: "#ecebef",
                  height: "70px",
                }}
                containerClass="active-pink active-pink-2 mt-0 mb-3 w-search-market"
                value={generalSearch}
                onChange={(e) => {
                  setGeneralSearch(e.target.value);
                }}
                onKeyUp={(event) => {
                  if (event.keyCode == 13) {
                    generalSearchHandler();
                  }
                }}
              >
                <SearchIcon
                  type="submit"
                  style={{
                    marginRight: "20px",
                    marginTop: "-52",
                    float: "right",
                    fontSize: "40px",
                    cursor: "pointer",
                  }}
                  onClick={() => {
                    generalSearchHandler();
                  }}
                />
              </MDBInput>
            </MDBCol>
          </div>
        </div>
        <div className="max-w-6xl   mx-auto px-4 sm:px-6">
          {/* Top area: Blocks */}
          <div className="px-6  py-4 md:py-6">
            {/* 1st block */}
            <div className="d-flex flex-column flex-lg-row justify-content-center">
              <div className="mx-2 my-1 my-lg-0">
                <ArtistFilter
                  allArtists={allArtists}
                  setAllArtists={setAllArtists}
                  showData={showData}
                  setShowdata={setShowdata}
                  filterData={filterData}
                  setFilterData={setFilterData}
                  filterSearching={filterSearching}
                  setNFTData={setNFTData}
                  count={count}
                  setCountButton={setCountButton}
                />
              </div>
              <div className="mx-2 my-1 my-lg-0">
                <CollectionFilter
                  allCollection={allCollection}
                  setAllCollection={setAllCollection}
                  showData={showData}
                  setShowdata={setShowdata}
                  filterData={filterData}
                  setFilterData={setFilterData}
                  filterSearching={filterSearching}
                />
              </div>
              <div className="mx-2 my-1 my-lg-0">
                <PriceFilter
                  allArtists={allArtists}
                  setAllArtists={setAllArtists}
                  showData={showData}
                  setShowdata={setShowdata}
                  filterData={filterData}
                  setFilterData={setFilterData}
                  filterSearching={filterSearching}
                />
              </div>
              <div className="mx-2 my-1 my-lg-0">
                <RecentlyListed
                  allArtists={allArtists}
                  setAllArtists={setAllArtists}
                  showData={showData}
                  setShowdata={setShowdata}
                  filterData={filterData}
                  setFilterData={setFilterData}
                  filterSearching={filterSearching}
                />
              </div>
            </div>
          </div>
        </div>
      </div>

      <div>
        <div className="max-w-6xl mt-5 mx-auto px-4 sm:px-6">
          {/* Top area: Blocks */}
          <div className=" gridsm:grid-cols-12 gap-8 py-4 md:py-6 border-t border-black">
            {NFTData?.length !== 0 ? (
              <>
                {/* {loading ? (
                  <div className="mt-5">
                    <Loader />
                  </div>
                ) : (
                  <> */}
                <Content NFTData={NFTData} />
                {NFTData?.length > 11 && NFTData?.length % 12 === 0 ? (
                  <p
                    className="btn-sm fontFamily ml-6 mr-6  text-white text-lg rounded-md cursor-pointer"
                    disabled={loading}
                    style={{ fontWeight: "400", fontSize: "16px" }}
                    onClick={() => {
                      if (buttonMarket) {
                        setCount({
                          ...count,
                          countMarket: countMarket + 1,
                          countGeneral: 0,
                        });
                        fetchData();
                        // marketHandler();
                      } else if (buttonGeneral) {
                        setCount({
                          ...count,
                          countGeneral: countGeneral + 1,
                          countMarket: 0,
                        });
                        fetchData();
                      } else if (buttonFilters) {
                        // setCount({
                        //   ...count,
                        //   countGeneral: countGeneral + 1,
                        // });
                      }
                    }}
                  >
                    {btnLoading && (
                      <CircularProgress size={20} className="mr-3" />
                    )}
                    {btnLoading && <span>Loading </span>}
                    {!btnLoading && <span>LoadMore</span>}{" "}
                  </p>
                ) : null}
              </>
            ) : (
              <>
                {!success && NFTData?.length === 0 ? (
                  <>
                    <section className="relative">
                      {/* Section background (needs .relative class on parent and next sibling elements) */}

                      <div className="relative max-w-10xl mx-auto  sm:px-6">
                        <div className="py-8 md:py-10">
                          {/* Items */}
                          <div className="max-w-sm mx-auto grid gap-8 md:grid-cols-1 lg:grid-cols-1 md:max-w-2xl lg:max-w-none">
                            <div className="">
                              {/* Items */}

                              <p>
                                Looks like there's nothing in this Market Place
                                yet!
                              </p>
                              <p>
                                See what's being created and collected in the{" "}
                                <a href="/home">Home</a>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                  </>
                ) : (
                  <>
                    {!loading ? (
                      <>
                        <div className=" gridsm:grid-cols-12 gap-8 py-8 md:py-8  ">
                          <>
                            <section className="relative">
                              {/* Section background (needs .relative class on parent and next sibling elements) */}

                              <div className="relative max-w-5xl mx-auto  sm:px-6 text-center">
                                <div className="">
                                  {/* Items */}

                                  <p>
                                    Looks like there's nothing in your owned
                                    yet!
                                  </p>
                                  <p>
                                    See what's being created and collected in
                                    the <a href="/market">activity feed</a>
                                  </p>
                                </div>
                              </div>
                            </section>
                          </>
                        </div>
                      </>
                    ) : (
                      <div className="relative max-w-5xl mx-auto sm:px-6">
                        <div
                          className="max-w-sm mx-auto grid gap-8 md:grid-cols-3 lg:grid-cols-3 md:max-w-2xl lg:max-w-none"
                          style={{
                            display: "flex",
                            width: "100%",
                            flexDirection: "row",
                          }}
                        >
                          {[1, 2, 3].map(() => (
                            <div style={{ width: "33%", margin: "1rem" }}>
                              <Skeleton
                                animation="wave"
                                variant="rectangular"
                                width="100%"
                                height={300}
                              />
                              <Skeleton width="100%" />
                              <Skeleton width="100%" />
                              <Skeleton width="100%" />
                            </div>
                          ))}
                        </div>
                      </div>
                    )}
                  </>
                )}
              </>
            )}
          </div>
        </div>
      </div>
    </>
  );
}
export default MainMarket;
