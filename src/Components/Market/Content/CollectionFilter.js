import React, { useState } from "react";
import SearchIcon from "@mui/icons-material/Search";
import DoneIcon from "@mui/icons-material/Done";

import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import { Dropdown } from "react-bootstrap";
import { artisitSearch } from "../../../Services/MarketServices";

const CollectionFilter = ({
  allCollection,
  setAllCollection,
  showData,
  setShowdata,
  filterData,
  setFilterData,
  filterSearching,
}) => {
  const [value, setValue] = useState("");

  const { collectionId } = filterData;

  const { showCollection } = showData;

  return (
    <>
      <Dropdown autoClose={false}>
        <Dropdown.Toggle
          className={
            collectionId
              ? "capitalize w-full bg-black-100 focus:bg-black-100 focus:border-transparent focus:shadow-none active:bg-black-100 active:shadow-none	active:focus:shadow-none hover:bg-black-100 hover:border-transparent"
              : "capitalize w-full focus:bg-black-100 focus:border-transparent focus:shadow-none active:bg-black-100 active:shadow-none	active:focus:shadow-none	bg-white-200 text-black-100 hover:bg-black-100 hover:border-transparent"
          } // onClick={() =>
          //   setShowdata({
          //     showArtist: false,
          //     showCollection: !showCollection,
          //     showPrice: false,
          //     showsort: false,
          //   })
          // }
        >
          Collection <KeyboardArrowDownIcon />
        </Dropdown.Toggle>
        {/* onClick={() => handleClick(event.eventRef, event.name)} */}
        <Dropdown.Menu renderOnMount={false} flip={false}>
          <div className="">
            <div className="relative">
              <input
                type="text"
                className="h-auto w-auto border-black border-2 rounded-2xl p-2 mx-2"
                placeholder="Search Collection..."
                autoFocus
                onChange={(e) => {
                  setValue(e.target.value);
                }}
                value={value}
              />
              <SearchIcon
                style={{
                  position: "absolute",
                  right: "20px",
                  top: "10px",
                }}
              />
            </div>

            {allCollection ? (
              <div className="container w-auto h-32 overflow-y-auto p-1 my-2 border-b-2	border-black pb-2">
                <li>
                  {allCollection
                    .filter(
                      (child) =>
                        !value ||
                        child?.collectionName
                          ?.toLowerCase()
                          .match(value.toLowerCase())
                    )
                    .map((items, index) => {
                      return (
                        <p
                          className="capitalize m-2 cursor-pointer hover:bg-gray-200"
                          eventKey={index + 1}
                          onClick={(e) => {
                            e.preventDefault();
                            setFilterData({
                              ...filterData,
                              collectionId: items._id,
                            });
                          }}
                        >
                          <div className="flex">
                            <div className="w-5/6">{items.collectionName}</div>

                            {collectionId == items._id ? <DoneIcon /> : null}
                          </div>
                        </p>
                      );
                    })}
                </li>
              </div>
            ) : (
              <div className="container w-auto h-32 overflow-y-auto p-1 my-2 border-b-2	border-black pb-2">
                <li>No collection to display</li>
              </div>
            )}
            <div class="flex flex-row justify-center px-4 py-2">
              <button
                class="n-clearButton"
                onClick={() => {
                  setValue("");
                  setFilterData({
                    ...filterData,
                    collectionId: "",
                  });
                  filterSearching();
                }}
              >
                Clear
              </button>
              <Dropdown.Item
                className="n-applyButton"
                onClick={() => {
                  filterSearching();
                  setShowdata({
                    ...showData,
                    showArtist: false,
                    showCollection: false,
                    showPrice: false,
                    showsort: false,
                  });
                }}
              >
                Apply
              </Dropdown.Item>
            </div>
          </div>
        </Dropdown.Menu>
      </Dropdown>
    </>
  );
};

export default CollectionFilter;
