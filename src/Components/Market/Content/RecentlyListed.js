import React, { useEffect, useState } from "react";
import SearchIcon from "@mui/icons-material/Search";
import DoneIcon from "@mui/icons-material/Done";

import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import { Dropdown } from "react-bootstrap";
import { artisitSearch } from "../../../Services/MarketServices";

const RecentlyListed = ({
  showData,
  setShowdata,
  filterData,
  setFilterData,
  filterSearching,
}) => {
  const { showsort } = showData;
  const { sort } = filterData;

  // useEffect(() => {
  //   console.log(filterData);
  // }, [sort]);

  return (
    <>
      <Dropdown>
        <Dropdown.Toggle
          className={
            sort
              ? "capitalize w-full bg-black-100 focus:bg-black-100 focus:border-transparent focus:shadow-none active:bg-black-100 active:shadow-none	active:focus:shadow-none hover:bg-black-100 hover:border-transparent"
              : "capitalize w-full focus:bg-black-100 focus:border-transparent focus:shadow-none active:bg-black-100 active:shadow-none	active:focus:shadow-none	bg-white-200 text-black-100 hover:bg-black-100 hover:border-transparent"
          } // onClick={() =>
          //   setShowdata({
          //     showArtist: false,
          //     showCollection: true,
          //     showPrice: false,
          //     showsort: !showsort,
          //   })
          // }
        >
          Recently Listed <KeyboardArrowDownIcon />
        </Dropdown.Toggle>
        {/* onClick={() => handleClick(event.eventRef, event.name)} */}
        <Dropdown.Menu renderOnMount={false} flip={false}>
          <li>
            <p
              className="capitalize m-2 cursor-pointer hover:bg-gray-200"
              onClick={() => setFilterData({ ...filterData, sort: "newest" })}
            >
              <div className="flex">
                <div className="w-5/6">Newest</div>
                {sort === "newest" ? (
                  <div className="w-auto">
                    <DoneIcon right />
                  </div>
                ) : (
                  <div className="w-auto invisible">
                    <DoneIcon right />
                  </div>
                )}
                {/* <DoneIcon /> */}
              </div>
            </p>
          </li>

          <p
            className="capitalize m-2 cursor-pointer hover:bg-gray-200"
            onClick={() => setFilterData({ ...filterData, sort: "oldest" })}
          >
            <div className="flex">
              <div className="w-5/6">Oldest</div>

              {sort === "oldest" ? (
                <div className="w-auto">
                  <DoneIcon right />
                </div>
              ) : (
                <div className="w-auto invisible">
                  <DoneIcon right />
                </div>
              )}
              {/* <DoneIcon /> */}
            </div>
          </p>
          <p
            className="capitalize m-2 cursor-pointer hover:bg-gray-200"
            onClick={() =>
              setFilterData({ ...filterData, sort: "highestprice" })
            }
          >
            <div className="flex">
              <p className="w-40">Price: High to Low</p>

              {sort === "highestprice" ? (
                <div className="w-auto">
                  <DoneIcon right />
                </div>
              ) : (
                <div className="w-auto invisible">
                  <DoneIcon right />
                </div>
              )}
            </div>
          </p>
          <p
            className="capitalize m-2 cursor-pointer hover:bg-gray-200"
            onClick={() =>
              setFilterData({ ...filterData, sort: "lowestprice" })
            }
          >
            <div className="flex w-full">
              <p className="w-40">Price: Low to High</p>
              {sort === "lowestprice" ? (
                <div className="w-auto">
                  <DoneIcon right />
                </div>
              ) : (
                <div className="w-auto invisible">
                  <DoneIcon right />
                </div>
              )}
            </div>
          </p>

          <div class="flex flex-row justify-center px-4 py-2 border-t-2	border-black">
            <button
              className="n-clearButton"
              onClick={() => {
                setFilterData({
                  ...filterData,
                  minPrice: "",
                  maxPrice: "",
                });
                filterSearching();
              }}
            >
              Clear
            </button>
            <Dropdown.Item
              className="n-applyButton "
              onClick={() => {
                filterSearching();
                setShowdata({
                  ...showData,
                  showArtist: false,
                  showCollection: false,
                  showPrice: false,
                  showsort: false,
                });
              }}
            >
              Apply
            </Dropdown.Item>
          </div>
        </Dropdown.Menu>
      </Dropdown>
    </>
  );
};

export default RecentlyListed;
