import React, { useState } from "react";
import SearchIcon from "@mui/icons-material/Search";
import DoneIcon from "@mui/icons-material/Done";

import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import { Dropdown } from "react-bootstrap";
import { artisitSearch } from "../../../Services/MarketServices";
import Swal from "sweetalert2";

const PriceFilter = ({
  showData,
  setShowdata,
  filterData,
  setFilterData,
  filterSearching,
}) => {
  const { showPrice } = showData;

  const { minPrice, maxPrice } = filterData;

  return (
    <>
      <Dropdown autoClose={false}>
        <Dropdown.Toggle
          className={
            maxPrice
              ? "capitalize w-full bg-black-100 focus:bg-black-100 focus:border-transparent focus:shadow-none active:bg-black-100 active:shadow-none	active:focus:shadow-none hover:bg-black-100 hover:border-transparent"
              : "capitalize w-full focus:bg-black-100 focus:border-transparent focus:shadow-none active:bg-black-100 active:shadow-none	active:focus:shadow-none	bg-white-200 text-black-100 hover:bg-black-100 hover:border-transparent"
          }
        >
          Price Range <KeyboardArrowDownIcon />
        </Dropdown.Toggle>
        {/* onClick={() => handleClick(event.eventRef, event.name)} */}
        <Dropdown.Menu renderOnMount={false} flip={false}>
          <div className="m-2 font-bold">Price range</div>
          <div className="flex border-b-2	border-black pb-2">
            <input
              type="number"
              className="h-auto w-40 border-black border-2 rounded-md p-2 gap-4 mx-2"
              placeholder="Min price"
              name="minPrice"
              autoFocus
              onChange={(e) => {
                setFilterData({
                  ...filterData,
                  [e.target.name]: e.target.value,
                });
              }}
              value={minPrice}
            />
            <input
              type="number"
              className="h-auto w-40 border-black border-2 rounded-md p-2 gap-4 mx-2"
              placeholder="Max price"
              name="maxPrice"
              onChange={(e) => {
                setFilterData({
                  ...filterData,
                  [e.target.name]: e.target.value,
                });
              }}
              value={maxPrice}
            />
          </div>
          <div
            class="flex flex-row justify-content-end 
         px-4 py-2 mt-2"
          >
            <button
              class="n-clearButton ml-3 "
              onClick={() => {
                setFilterData({
                  ...filterData,
                  minPrice: "",
                  maxPrice: "",
                });
                filterSearching();
              }}
            >
              Clear
            </button>
            <Dropdown.Item
              className="n-applyButton "
              style={{ width: "27%" }}
              onClick={() => {
                console.log(filterData);
                if ((+maxPrice && +minPrice) >= 0) {
                  if (minPrice == "") {
                    setFilterData({
                      ...filterData,
                      minPrice: "0",
                    });
                  }
                  if (+minPrice <= +maxPrice) {
                    filterSearching();
                    setShowdata({
                      ...showData,
                      showArtist: false,
                      showCollection: false,
                      showPrice: false,
                      showsort: false,
                    });
                    // Swal.fire({
                    //   icon: "success",
                    //   text: "ABC",
                    //   toast: true,
                    // });
                  } else {
                    Swal.fire({
                      icon: "error",
                      text: "Min-Price Should be less than Max-Price",
                      toast: true,
                    });
                  }
                  // debugger;
                } else {
                  Swal.fire({
                    icon: "error",
                    text: "Please enter a positive number",
                    toast: true,
                  });
                }
              }}
            >
              Apply
            </Dropdown.Item>
          </div>
        </Dropdown.Menu>
      </Dropdown>
    </>
  );
};

export default PriceFilter;
