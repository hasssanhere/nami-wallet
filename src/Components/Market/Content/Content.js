import React from "react";
import { Link } from "react-router-dom";
import { Card, CardContent, CardMedia } from "@material-ui/core";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";

const Content = ({ NFTData }) => {
  return (
    <>
      <section className="relative">
        <div className="relative max-w-10xl mx-auto  sm:px-6">
          <div className="py-8 md:py-10">
            <div className="max-w-sm mx-auto grid gap-8 md:grid-cols-2 lg:grid-cols-3 md:max-w-2xl lg:max-w-none">
              <>
                {NFTData?.map((nft) => {
                  return (
                    <div className="   relative flex flex-col bg-white ">
                      <Link
                        style={{
                          textDecoration: "none",
                          color: "inherit",
                        }}
                        to={{
                          pathname: `/artworkdetailed/${nft._id}?nftId=${nft._id}&userId=${nft.artistId._id}`,
                          state: {
                            nftId: nft._id,
                            userId: nft.artistId,
                          },
                        }}
                        // {...(sessionStorage.getItem("walletaddress")
                        //   ? { href: "/signupp" }
                        //   : {
                        //       onClick: () =>
                        //         Swal.fire({
                        //           icon: "error",
                        //           text: "Connect Your Wallet First",
                        //         }),
                        //     })}
                      >
                        {" "}
                        <Card
                          style={{
                            backgroundColor: "#FfFfFf",
                            borderRadius: "10px",
                          }}
                        >
                          <CardContent
                            style={{
                              padding: "20px 15px 0 15px",
                            }}
                          >
                            <CardMedia
                              component="img"
                              image={nft?.gatewayLink}
                              onError={(e) => {
                                /**
                                 * Any code. For instance, changing the `src` prop with a fallback url.
                                 * In our code, I've added `e.target.className = fallback_className` for instance.
                                 */
                                e.target.src =
                                  "https://mpama.com/wp-content/uploads/2017/04/default-image-620x600.jpg";
                              }}
                              sx={{
                                minHeight: 280,
                                borderRadius: "10px",
                              }}
                              style={{
                                height: 280,
                                objectFit: "contain",
                                width: "100%",
                                borderRadius: "10px",
                                // backgroundImage:
                                //   "url(" +
                                //   "https://images.pexels.com/photos/34153/pexels-photo.jpg?auto=compress&cs=tinysrgb&h=350" +
                                //   ")",
                              }}
                            />
                          </CardContent>
                          <CardContent>
                            <div className=" font-Raleway grid grid-cols-2 text-left">
                              <div className=" font-Raleway grid grid-cols-1 text-left">
                                <div className="sm:col-span-1 md:col-span-1 lg:col-span-1 text-left">
                                  <p
                                    className="font-bold text-base	text-black-100 text-break	"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title={nft?.title}
                                  >
                                    {nft?.title?.length > 15 ? (
                                      <>
                                        {`${nft?.title?.substring?.(
                                          0,
                                          14
                                        )}...` || " "}
                                      </>
                                    ) : (
                                      <>{`${nft?.title}` || " "}</>
                                    )}
                                  </p>
                                </div>
                              </div>
                              <div className="sm:col-span-1 md:col-span-1 lg:col-span-1  text-right">
                                <Link
                                  // to={`/artist-detail/${nft.artistId}`}
                                  to={{
                                    pathname: `/artist-detail/${nft.artistId.username}`,
                                    state: {
                                      userId: nft.artistId._id,
                                    },
                                  }}
                                  className="font-normal text-sm text-blue-700 text-break"
                                  data-toggle="tooltip"
                                  data-placement="top"
                                  title={nft?.artistId?.username}
                                >
                                  @
                                  {nft?.artistId?.username?.length > 10 ? (
                                    <>
                                      {`${nft?.artistId?.username?.substring(
                                        0,
                                        9
                                      )}...` || " "}
                                    </>
                                  ) : (
                                    <>{`${nft?.artistId?.username}` || " "}</>
                                  )}
                                  <CheckCircleIcon
                                    style={{
                                      fontSize: "20",
                                      color: "#367BF5",
                                    }}
                                  />
                                </Link>
                              </div>
                              <div class="flex flex-col">
                                <div className="sm:col-span-2 md:col-span-2 lg:col-span-2 text-left">
                                  <p
                                    // to={`/artist-detail/${nft.artistId}`}
                                    className="font-normal	text-sm text-black-100 text-break"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title={nft?.collectionId?.collectionName}
                                  >
                                    {/* {`${nft?.collectionName?.substring(
                                                  0,
                                                  15
                                                )}...` || ""} */}

                                    {/* {nft?.collectionId?.collectionName?.length >
                                    18 ? (
                                      <>
                                        {" "}
                                        {`${nft?.collectionId?.collectionName?.substring(
                                          0,
                                          18
                                        )}...` || ""}
                                      </>
                                    ) : ( */}
                                    <>{nft?.collectionId?.collectionName}</>
                                    {/* )} */}
                                  </p>
                                </div>
                                <div className=" font-Raleway grid grid-cols-1 text-left">
                                  <div className="sm:col-span-1 md:col-span-1 lg:col-span-1 text-left">
                                    <p
                                      className="font-bold text-base	text-black-100 text-break font-roboto	"
                                      data-toggle="tooltip"
                                      data-placement="top"
                                      title={nft?.price}
                                    >
                                      ADA {nft?.price}
                                    </p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </CardContent>
                        </Card>
                      </Link>
                    </div>
                  );
                })}
              </>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Content;
