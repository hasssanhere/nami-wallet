import React, { useState } from "react";
import { Button, Modal } from "react-bootstrap";
import "../../../App/Assets/css/css/SignupSignin.css";
import { Card } from "react-bootstrap";
import Grid from "@mui/material/Grid";

import Swal from "sweetalert2";
import { editPriceOfListedNfts } from "../../../Services/ProfileServices";

function UpdatePriceModal(props) {
  const {
    onHide,
    setdataa,
    refresh,
    royalty,
    setRefresh,
    oldPrice,
    item,
    title,
    image,

    // setRefresh,
  } = props;
  console.log(item);

  const [price, setPrice] = useState("");
  const [Calculation, setCalculation] = useState({
    Peroyality: 0.0,
    Servicefee: 0.0,
    TEarning: 0.0,
  });

  const { Peroyality, Servicefee, TEarning } = Calculation;

  const changeHandler = (e) => {
    if (e.target.value >= 0) {
      setPrice(e.target.value);
      setCalculation({
        Peroyality: (Number(e.target.value) * Number(royalty)) / 100,
        Servicefee: (Number(e.target.value) * 2.5) / 100,
        TEarning:
          Number(e.target.value) -
          (Number(e.target.value) * Number(royalty)) / 100 -
          (Number(e.target.value) * 2.5) / 100,
      });
    } else {
      Swal.fire({
        icon: "error",
        text: "Please enter a Positive Number",
        toast: true,
      });
    }
  };
  // var rounded = Math.round(newUSD * 10) / 10;
  return (
    <div>
      <Modal
        {...props}
        size="md"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        className="n-Modal-myowned"
      >
        <Modal.Body class="n-modalBody">
          <>
            <Grid container>
              <Grid item xs={12}>
                <Card className="n-card-Modal">
                  <Card.Body>
                    <div
                      className="w-sellModalTitle"
                      style={{ fontSize: "20px" }}
                    >
                      Update Price
                      <button
                        type="button"
                        className="close float-right"
                        onClick={onHide}
                      >
                        <svg
                          width="40"
                          height="40"
                          viewBox="0 0 40 40"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            fill-rule="evenodd"
                            clip-rule="evenodd"
                            d="M13.2929 13.2929C13.6834 12.9024 14.3166 12.9024 14.7071 13.2929L20 18.5858L25.2929 13.2929C25.6834 12.9024 26.3166 12.9024 26.7071 13.2929C27.0976 13.6834 27.0976 14.3166 26.7071 14.7071L21.4142 20L26.7071 25.2929C27.0976 25.6834 27.0976 26.3166 26.7071 26.7071C26.3166 27.0976 25.6834 27.0976 25.2929 26.7071L20 21.4142L14.7071 26.7071C14.3166 27.0976 13.6834 27.0976 13.2929 26.7071C12.9024 26.3166 12.9024 25.6834 13.2929 25.2929L18.5858 20L13.2929 14.7071C12.9024 14.3166 12.9024 13.6834 13.2929 13.2929Z"
                            fill="black"
                          />
                          <rect
                            x="1"
                            y="1"
                            width="38"
                            height="38"
                            rx="19"
                            stroke="#E5E8EB"
                            stroke-width="2"
                          />
                        </svg>
                      </button>
                    </div>
                    <div class="n-updateModalBody">
                      <div>
                        <span class="font-bold text-lg">Assets</span>
                      </div>
                      <div class="mt-4 flex flex-row items-center">
                        <Card.Img
                          src={image}
                          onError={(e) => {
                            e.target.src =
                              "https://mpama.com/wp-content/uploads/2017/04/default-image-620x600.jpg";
                          }}
                          style={{
                            width: "50px",
                            height: "50px",
                            borderRadius: "5px",
                          }}
                        />
                        <span
                          class="ml-4"
                          style={{ fontSize: "20px", fontWeight: "500" }}
                        >
                          {title}
                        </span>
                      </div>
                      <div class="d-flex n-sellDescription ">
                        <span class="font-bold text-lg">Price</span>
                        {/* <span className=" ml-2 font-bold ">{oldPrice} ADA</span> */}
                      </div>

                      <div className="n-modal-form">
                        <form className="w-nft-form">
                          <input
                            type="number"
                            required
                            className="inputStyling n-priceInput n-placeholder"
                            id="price"
                            label="Price"
                            placeholder="0.00"
                            name="price"
                            autoComplete="price"
                            value={price}
                            onChange={changeHandler}
                          />
                        </form>
                      </div>
                      <div class="n-sellDescription flex flex-row justify-between">
                        <span class="text-xl font-medium">
                          Royalty: ({Math.round(royalty * 10) / 10}%)
                        </span>
                        <span class="text-xl font-medium">
                          {Math.round(Peroyality * 10) / 10 || "0"} ADA
                        </span>
                      </div>
                      <div class="n-sellDescription flex flex-row justify-between">
                        <span class="text-xl font-medium">
                          Service Fee: (2.5%)
                        </span>
                        <span class="text-xl font-medium">
                          {Math.round(Servicefee * 10) / 10 || "0"} ADA
                        </span>
                      </div>
                      <div class="mt-2 text-sm text-slate-600">
                        A minimum transaction fee of 1 ADA <br /> is required by
                        Cardano network
                      </div>
                      <div class="n-sellDescription flex flex-row justify-between">
                        <span class="text-2xl font-medium">Your earning</span>
                        <span class="text-2xl font-medium">
                          {Math.round(TEarning * 10) / 10 || "0"} ADA
                        </span>
                      </div>
                      <button
                        className="btn-block btn btn-primary w-full mt-3"
                        style={{
                          backgroundColor: "black",
                          borderColor: "black",
                        }}
                        type="submit"
                        variant="contained"
                        sx={{ mt: 3, mb: 2, bg: "#000000" }}
                        onClick={async () => {
                          console.log(item);
                          if (price) {
                            const obj = {
                              nftid: item,
                              price: price,
                            };
                            editPriceOfListedNfts({
                              obj,
                              refresh,
                              setRefresh,
                            });
                            onHide();
                          } else {
                            Swal.fire({
                              toast: true,
                              icon: "error",
                              text: "Kindly Provide price",
                            });
                          }
                        }}
                      >
                        Update
                      </button>
                    </div>
                  </Card.Body>
                </Card>
              </Grid>
              <Grid item xs={12} className="ctmfontFamily "></Grid>
            </Grid>
          </>
        </Modal.Body>
      </Modal>
    </div>
  );
}

export default UpdatePriceModal;
