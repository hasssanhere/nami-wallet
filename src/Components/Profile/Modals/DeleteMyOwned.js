import React from "react";
import "../../../App/Assets/css/css/SignupSignin.css";
import { Modal } from "react-bootstrap";
import { deleteNFT } from "../../../Services/CreateNFT";
function DeleteMyOwned(props) {
  const { item, refresh, setRefresh, onHide } = props;

  const deletenft = (item) => {
    deleteNFT(item, refresh, setRefresh);
    onHide();
  };
  return (
    <div>
      {" "}
      <Modal
        {...props}
        size="sm"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Body>
          <>
            <p className="pt-10 text-center font-Raleway font-normal">
              Are you sure you want to delete this NFT?
            </p>
            <div class="grid grid-cols-2 gap-5 m-auto pb-2 text-center mx-5 my-4 ">
              <button
                // data-aos="zoom-y-out"
                className=" px-4  py-2 btn btn-sm text-center text-white-100 rounded-md  "
                onClick={() => deletenft(item)}
              >
                <strong>Yes</strong>
              </button>

              <button
                className="px-4 py-2 btn btn-sm-rev text-center text-black-100 hover:text-white-100 rounded-md  "
                onClick={onHide}
              >
                <strong>No</strong>
              </button>
            </div>
          </>
        </Modal.Body>
      </Modal>
    </div>
  );
}

export default DeleteMyOwned;
