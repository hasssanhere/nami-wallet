import React, { useEffect } from "react";
import Grid from "@mui/material/Grid";
import "../../../App/Assets/css/css/SignupSignin.css";
import { Modal } from "react-bootstrap";
import HighlightOffIcon from "@mui/icons-material/HighlightOff";
import { removeWallet } from "../../../Services/ProfileServices";
import { getUserDetailsbyID } from "../../../Services/artistSevices";

function DeleteModal(props) {
  const { onHide, refresh, setrefresh, walletaddress, setdata } = props;

  const disconnectWallet = () => {
    const body = {
      address: walletaddress,
    };
    removeWallet(body, refresh, setrefresh);

    onHide();
  };

  return (
    <div>
      <Modal
        {...props}
        size="md"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Body>
          <>
            <p className="pt-10 text-center font-Raleway font-normal">
              Are you sure you want to remove the wallet?
            </p>
            <div class="grid grid-cols-2 gap-5 m-auto pb-2 text-center mx-5 my-4 ">
              <button
                // data-aos="zoom-y-out"
                className=" px-4  py-2 btn btn-sm text-center text-white-100 rounded-md  "
                onClick={disconnectWallet}
              >
                <strong>Yes</strong>
              </button>

              <button
                className="px-4 py-2 btn btn-sm-rev text-center text-black-100 hover:text-white-100 rounded-md  "
                onClick={onHide}
              >
                <strong>No</strong>
              </button>
            </div>
          </>
        </Modal.Body>
      </Modal>
    </div>
  );
}

export default DeleteModal;
