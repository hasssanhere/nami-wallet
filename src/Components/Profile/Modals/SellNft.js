import React, { useState } from "react";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";
import "../../../App/Assets/css/css/SignupSignin.css";
import Swal from "sweetalert2";
import { Modal, Card } from "react-bootstrap";
import { listaNft } from "../../../Services/ProfileServices";
import ArrowRightAltIcon from "@mui/icons-material/ArrowRightAlt";
import { Link } from "react-router-dom";
import cardino from "../../../App/Assets/images/cardano.png";

function SellNft(props) {
  const {
    onHide,
    setdataa,
    refresh,
    setRefresh,
    id,
    title,
    image,
    description,
  } = props;

  const [price, setPrice] = useState("");
  const [isReadMore, setIsReadMore] = useState(true);
  const toggleReadMore = () => {
    setIsReadMore(!isReadMore);
  };

  const changeHandler = (e) => {
    if (e.target.value >= 0) {
      setPrice(e.target.value);
    } else {
      Swal.fire({
        icon: "error",
        text: "Please enter a Positive Number",
        toast: true,
      });
    }
  };
  return (
    <div>
      <Modal
        {...props}
        size="md"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        className="w-Modal-myowned"
      >
        <Modal.Body class="n-modalBody">
          <>
            <Grid container>
              <Grid item xs={12}>
                <Card className="W-card-Modal">
                  <div className="n-sellModalTitleNew">
                    {title}
                    <button
                      type="button"
                      className="close float-right"
                      onClick={onHide}
                    >
                      <svg
                        width="40"
                        height="40"
                        viewBox="0 0 40 40"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          fill-rule="evenodd"
                          clip-rule="evenodd"
                          d="M13.2929 13.2929C13.6834 12.9024 14.3166 12.9024 14.7071 13.2929L20 18.5858L25.2929 13.2929C25.6834 12.9024 26.3166 12.9024 26.7071 13.2929C27.0976 13.6834 27.0976 14.3166 26.7071 14.7071L21.4142 20L26.7071 25.2929C27.0976 25.6834 27.0976 26.3166 26.7071 26.7071C26.3166 27.0976 25.6834 27.0976 25.2929 26.7071L20 21.4142L14.7071 26.7071C14.3166 27.0976 13.6834 27.0976 13.2929 26.7071C12.9024 26.3166 12.9024 25.6834 13.2929 25.2929L18.5858 20L13.2929 14.7071C12.9024 14.3166 12.9024 13.6834 13.2929 13.2929Z"
                          fill="black"
                        />
                        <rect
                          x="1"
                          y="1"
                          width="38"
                          height="38"
                          rx="19"
                          stroke="#E5E8EB"
                          stroke-width="2"
                        />
                      </svg>
                    </button>
                  </div>
                  <Card.Img
                    variant="top"
                    src={image}
                    onError={(e) => {
                      e.target.src =
                        "https://mpama.com/wp-content/uploads/2017/04/default-image-620x600.jpg";
                    }}
                    style={{
                      objectFit: "contain",
                    }}
                  />
                  <Card.Body>
                    <div class="h-full flex flex-column justify-between">
                      <div>
                        <div className="n-sellModalTitle">
                          {title}
                          <button
                            type="button"
                            className="close float-right"
                            onClick={onHide}
                          >
                            <svg
                              width="40"
                              height="40"
                              viewBox="0 0 40 40"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <path
                                fill-rule="evenodd"
                                clip-rule="evenodd"
                                d="M13.2929 13.2929C13.6834 12.9024 14.3166 12.9024 14.7071 13.2929L20 18.5858L25.2929 13.2929C25.6834 12.9024 26.3166 12.9024 26.7071 13.2929C27.0976 13.6834 27.0976 14.3166 26.7071 14.7071L21.4142 20L26.7071 25.2929C27.0976 25.6834 27.0976 26.3166 26.7071 26.7071C26.3166 27.0976 25.6834 27.0976 25.2929 26.7071L20 21.4142L14.7071 26.7071C14.3166 27.0976 13.6834 27.0976 13.2929 26.7071C12.9024 26.3166 12.9024 25.6834 13.2929 25.2929L18.5858 20L13.2929 14.7071C12.9024 14.3166 12.9024 13.6834 13.2929 13.2929Z"
                                fill="black"
                              />
                              <rect
                                x="1"
                                y="1"
                                width="38"
                                height="38"
                                rx="19"
                                stroke="#E5E8EB"
                                stroke-width="2"
                              />
                            </svg>
                          </button>
                        </div>
                        <div class="n-sellDescription">
                          {isReadMore
                            ? description?.slice(0, 150)
                            : description}
                          {description?.length > 150 && (
                            <span
                              style={{ color: "blue", cursor: "pointer" }}
                              onClick={toggleReadMore}
                            >
                              {isReadMore ? "...read more" : " ...show less"}
                            </span>
                          )}
                        </div>
                      </div>

                      <div className="w-modal-form">
                        <form className="w-nft-form">
                          <div class="flex items-end mt-3">
                            <p
                              style={{
                                fontSize: "14px",
                              }}
                            >
                              Price in{" "}
                            </p>{" "}
                            <img
                              src={cardino}
                              style={{
                                height: "25px",
                              }}
                            />
                          </div>

                          <input
                            type="number"
                            required
                            className="inputStyling n-priceInput"
                            id="price"
                            label="Price"
                            placeholder=" Price in ADA"
                            name="price"
                            autoComplete="price"
                            value={price}
                            onChange={changeHandler}
                          />
                        </form>
                        <button
                          className="md:ml-3  btn-block btn btn-primary w-full md:w-auto  md:my-0"
                          style={{
                            backgroundColor: "black",
                            borderColor: "black",
                            marginTop: "2.4rem",
                          }}
                          type="submit"
                          variant="contained"
                          sx={{ bg: "#000000" }}
                          onClick={async () => {
                            if (price) {
                              const obj = {
                                nftid: id,
                                price: price,
                              };
                              listaNft({ obj, setdataa, refresh, setRefresh });
                              onHide();
                            } else {
                              Swal.fire({
                                toast: true,
                                icon: "error",
                                text: "Kindly Provide price",
                              });
                            }
                          }}
                        >
                          Sell This
                        </button>
                      </div>
                    </div>
                  </Card.Body>
                </Card>
              </Grid>
              <Grid item xs={12} className="ctmfontFamily "></Grid>
            </Grid>
          </>
        </Modal.Body>
      </Modal>
    </div>
  );
}

export default SellNft;
