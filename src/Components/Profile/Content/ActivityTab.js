import React from 'react'
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import noorImage from "../../../App/Assets/images/MaskGroup.png"
import SoldIcon from '../../../App/Assets/images/SVGIcons/SoldIcon';
import ActivityTabTableRow from './ActivityTabTableRow';

const ActivityTab = () => {
    return (
        <div className=" gridsm:grid-cols-12 gap-8 py-8 md:py-8">
            <section className="relative">
                <div className="relative max-w-5xl mx-auto  sm:px-6">
                    <TableContainer className="w-noor-table" component={Paper}>
                        <Table sx={{ minWidth: 650 }} aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell></TableCell>
                                    <TableCell>Item</TableCell>
                                    <TableCell>Price</TableCell>
                                    <TableCell>From</TableCell>
                                    <TableCell>To</TableCell>
                                    <TableCell>Time</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                <ActivityTabTableRow />
                                <ActivityTabTableRow />
                                <ActivityTabTableRow />
                                <ActivityTabTableRow />
                                <ActivityTabTableRow />
                                <ActivityTabTableRow />
                                <ActivityTabTableRow />
                                <ActivityTabTableRow />
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <div className="text-center wmt-50">
                        <button className="w-btn-load-more">Load More</button>
                    </div>
                </div>
            </section>
        </div>
    )
}

export default ActivityTab
