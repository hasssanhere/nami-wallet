import React, { useState, useEffect } from "react";
import { Button, Image } from "react-bootstrap";
import {
  editBio,
  editPassword,
  getUserDetailsbyID,
  UploadPimage,
} from "../../../Services/artistSevices";
import { flushSync } from "react-dom";
import CameraAltIcon from "@mui/icons-material/CameraAlt";
import "../../../App/Assets/css/css/profile.css";
import { Skeleton } from "@mui/material";

function EditProfileTab(id) {
  const [userDataa, setUserDataa] = useState({});
  const [refresh, setRefresh] = useState(false);
  const [bio, setBio] = useState("");
  const [oldPass, setOldPass] = useState("");
  const [newPass, setNewPass] = useState("");
  const [confirmPass, setConfirmPass] = useState("");

  useEffect(() => {
    getUserDetailsbyID(id.id, setUserDataa);
  }, [refresh]);
  const profilePic = (e) => {
    flushSync(() => {
      setProfile(e.target.files[0]);
    });
    var data = new FormData();
    data.append("imageUpload", e.target.files[0]);
    UploadPimage(data, setRefresh, refresh);
  };
  const [profile, setProfile] = useState({});

  const changeHandler = (e) => {
    setBio(e.target.value);
  };
  const oldPasshandler = (e) => {
    setOldPass(e.target.value);
  };
  const newPasshandler = (e) => {
    setNewPass(e.target.value);
  };
  const confirmNewPasshandler = (e) => {
    setConfirmPass(e.target.value);
  };

  const submitHandler = () => {
    editBio(bio);
    if (oldPass === "" || newPass === "" || confirmPass === "") {
    } else {
      editPassword(oldPass, newPass, confirmPass);
    }
  };

  return (
    <div>
      <div className=" gridsm:grid-cols-12 gap-8 py-8 md:py-8 ">
        <section className="relative">
          {/* Section background (needs .relative class on parent and next sibling elements) */}

          <div className="relative max-w-5xl mx-auto  sm:px-6">
            <div className="max-w-6xl    mx-auto px-4 sm:px-6">
              {/* Top area: Blocks */}
              <div className=" gridsm:grid-cols-12 gap-8 ">
                <div style={{ marginBottom: "50px" }} className="container ">
                  <div className="row ">
                    <div
                      className="col-sm-6 block-to-disappear-in-mobile "
                      style={{ padding: "0 30px" }}
                    >
                      <div
                        class="pro-img"
                        style={{
                          marginTop: "90px",
                        }}
                      >
                        {Object.keys(userDataa).length === 0 ? (
                          <Skeleton
                            variant="circular"
                            width={311}
                            height={311}
                          />
                        ) : (
                          <img
                            className="pimage3 rounded-full"
                            src={userDataa?.image}
                            // alt="user"
                            onError={(e) => {
                              /**
                               * Any code. For instance, changing the `src` prop with a fallback url.
                               * In our code, I've added `e.target.className = fallback_className` for instance.
                               */
                              e.target.src =
                                "https://media.istockphoto.com/vectors/male-profile-flat-blue-simple-icon-with-long-shadow-vector-id522855255?k=20&m=522855255&s=612x612&w=0&h=fLLvwEbgOmSzk1_jQ0MgDATEVcVOh_kqEe0rqi7aM5A=";
                            }}
                            // style={{
                            //   width: "311px",
                            //   Height: "311px",
                            // }}
                          />
                        )}

                        <input
                          type="file"
                          id="imageUpload"
                          className="d-none"
                          onChange={(e) => {
                            profilePic(e);
                          }}
                          required
                        ></input>
                        <div class="cameramiddle2">
                          <label for="imageUpload">
                            <CameraAltIcon sx={{ fontSize: 50 }} />
                          </label>
                        </div>
                      </div>
                    </div>

                    <div className="col-sm-6">
                      <for
                        action="/adddoctor"
                        method="POST"
                        className="form-group"
                      >
                        <input
                          type="hidden"
                          name="csrfmiddlewaretoken"
                          value="fIwiR9rbZTmvxfmW8gC8CiS93Zx36iAh0kdWjuhKGglTMld96xGITqBEbdBR4EkY"
                        />
                      </for>
                      <form className="w-nft-form">
                        <label className="nft-form-lable">UserName</label>
                        <input
                          type="text"
                          className="form-control"
                          // placeholder={userDataa?.username}
                          name="title"
                          disabled
                          value={userDataa?.username}
                          //   onChange={changeHandler}
                          required
                        />
                        <label className="nft-form-lable">Email</label>
                        <input
                          type="email"
                          className="form-control"
                          // placeholder="Email"
                          name="email"
                          disabled
                          value={userDataa?.email}
                          //   onChange={changeHandler}
                          required
                        />
                        <label className="nft-form-lable">
                          Current Password
                        </label>
                        <input
                          type="password"
                          className="form-control"
                          placeholder="Current password"
                          name="currentPassword"
                          // value={title}
                          onChange={oldPasshandler}
                          required
                        />
                        <label className="nft-form-lable"> New Password</label>
                        <input
                          type="password"
                          className="form-control"
                          placeholder="New Password"
                          name="newPassword"
                          //   value={title}
                          onChange={newPasshandler}
                          required
                        />
                        <label className="nft-form-lable">
                          Confirm Password
                        </label>
                        <input
                          type="password"
                          className="form-control"
                          placeholder="Confirm Password"
                          name="newPassword"
                          //   value={title}
                          onChange={confirmNewPasshandler}
                          required
                        />
                        <label className="nft-form-lable">Bio</label>
                        <textarea
                          type="text"
                          className="form-control"
                          placeholder="Enter Your Bio..."
                          name="bio"
                          maxLength="500"
                          placeholder="Write a compelling story about your NFT "
                          requiredx
                          multiline
                          rows="5"
                          name="bio"
                          // value={description}
                          onChange={changeHandler}
                          required
                        ></textarea>
                      </form>

                      <Button
                        // onClick={clickHandler}
                        className=" mt-5 w-btn-nft"
                        type="submit"
                        onClick={submitHandler}
                      >
                        Save{" "}
                      </Button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  );
}

export default EditProfileTab;
