import {
  Card,
  CardContent,
  CardMedia,
  CircularProgress,
  Grid,
  Typography,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import SaleImage from "../../../App/Assets/images/sale-image.png";
import { Button, Modal } from "react-bootstrap";
import {
  featurednft,
  getFeaturedPrices,
  getListedNft,
  listProjects,
  removenft,
} from "../../../Services/ProfileServices";
import Swal from "sweetalert2";
import { Link } from "react-router-dom";
import { Skeleton } from "@mui/material";
import UpdatePriceModal from "../Modals/UpdatePriceModal";
import RemoveOnSale from "../Modals/RemoveOnSale";

const getFormattedPrice = (price) => `$${price.toFixed(2)}`;

const OnSaleTab = ({ setCreation, refresh, setRefresh }) => {
  const [dataaLis, setdataaLis] = useState([]);
  const [Id, setId] = useState();
  const [priceData, setPriceData] = useState([]);
  const [count, setCount] = useState(1);
  const [DeleteItem, setDeleteItem] = useState();
  const [btnLoading, setBtnLoading] = useState(false);

  const [show, setShow] = useState(false);
  const [modalShow1, setModalShow1] = useState(false);

  const [show1, setShow1] = useState(false);
  // const [modalShow, setModalShow] = React.useState(false);
  const [loading, setLoading] = useState(true);
  const handleClose = () => setShow(false);

  const handleShow = () => setShow(true);
  const [detaildata, setDetaildata] = useState({
    modalShow: false,
    id: "",
    title: "",
    image: "",
    royalty: "",
  });
  const { id, title, image, price, modalShow, royalty } = detaildata;

  const [checkedState, setCheckedState] = useState(
    new Array(priceData?.length).fill(false)
  );

  const [formdata, setFormdata] = useState({
    mainBanner: false,
    mainPrice: "",
    secondaryBanner: false,
    secondaryPrice: "",
  });
  const { mainBanner, mainPrice, secondaryBanner, secondaryPrice } = formdata;

  const FeaturedHandler = (id) => {
    setId(id);
    handleShow();
  };

  const handleOnChange = (position, title, price) => {
    const updatedCheckedState = checkedState.map((item, index) =>
      index === position ? !item : item
    );

    setCheckedState(updatedCheckedState);

    if (title == "mainBanner") {
      setFormdata({ ...formdata, mainBanner: !mainBanner, mainPrice: price });
    } else {
      setFormdata({
        ...formdata,
        secondaryBanner: !secondaryBanner,
        secondaryPrice: price,
      });
    }
  };

  const handleSubmit = () => {
    let body = {};
    if (mainBanner && secondaryBanner) {
      body = {
        // mainBanner,
        mainPrice,
        // secondaryBanner,
        secondaryPrice,
      };
    } else if (mainBanner) {
      body = {
        // mainBanner,
        mainPrice,
      };
    } else if (secondaryBanner) {
      body = {
        // secondaryBanner,
        secondaryPrice,
      };
    } else {
      Swal.fire({
        icon: "error",
        text: "Kindly Select a Display Screen  ",
      });
    }
    featurednft({ Id, body });
    handleClose();
  };

  useEffect(() => {
    getListedNft({ setdataaLis, setLoading, count });
    getFeaturedPrices({ setPriceData });
    // listProjects();
  }, [refresh, count]);
  const onDeleteButtonClick = (item) => {
    setModalShow1(true);
    setDeleteItem(item);
  };
  const fetchData = () => {
    setBtnLoading(true);
    //Faking API call here
    setTimeout(() => {
      setBtnLoading(false);
    }, 2000);
  };
  console.log(loading, !(dataaLis.length == 0));
  return (
    <div className=" gridsm:grid-cols-12 gap-8 py-8 md:py-8 ">
      <UpdatePriceModal
        show={modalShow}
        onHide={() => setDetaildata({ ...detaildata, modalShow: false })}
        // setdataa={setdataa}
        refresh={refresh}
        setRefresh={setRefresh}
        item={id}
        oldPrice={price}
        royalty={royalty}
        image={image}
        title={title}
        // description={description}
      />
      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
        className="w-onSale-modal"
      >
        <Modal.Header closeButton>
          <Modal.Title>Select a Display Screen</Modal.Title>
        </Modal.Header>
        {priceData ? (
          <Modal.Body>
            {priceData?.map((item, index) => (
              <div key={index} className="container mx-auto">
                <div className="toppings-list-item font-Raleway">
                  <div className="left-section">
                    <input
                      type="checkbox"
                      class="custom-control-input pr-4"
                      id={`custom-checkbox-${index}`}
                      // value={item.title}
                      // checked={isChecked[index]}
                      // onChange={handleOnChange}
                      name={item.title}
                      value={item.title}
                      price={item.price}
                      checked={checkedState[index]}
                      onChange={() =>
                        handleOnChange(index, item.title, item.price)
                      }
                    />
                    {/* <label }>{name}</label> */}
                    <label
                      htmlFor={`custom-checkbox-${index}`}
                      className="font-bold text-lg"
                    >
                      {item.title == "mainBanner" ? (
                        <>Main Slider</>
                      ) : (
                        <>Secondary Slider</>
                      )}{" "}
                    </label>
                    {/* // Price <span>${item.price}</span> */}
                  </div>

                  <div className="right-section">
                    {getFormattedPrice(item.price)}
                  </div>
                </div>
              </div>
            ))}
            {/* <div className="container mx-auto my-4">
            <div className="toppings-list-item">
              <div className="left-section font-bold text-lg">Total:</div>
              <div className="right-section font-bold text-lg">{total}</div>
            </div>
          </div> */}

            <div className="text-right mt-4">
              <button className="btn btn-dark" onClick={handleSubmit}>
                Send Request
              </button>
            </div>
          </Modal.Body>
        ) : (
          <>
            <Modal.Body>
              <p>There are no NFT display screen available</p>
            </Modal.Body>
          </>
        )}
      </Modal>
      {dataaLis?.length !== 0 && !loading ? (
        <section className="relative">
          <div className="relative max-w-5xl mx-auto  sm:px-6">
            <>
              <div className="max-w-sm mx-auto grid gap-8 md:grid-cols-3 lg:grid-cols-3 md:max-w-2xl lg:max-w-none">
                {dataaLis?.map((item) => (
                  <>
                    <Card
                      style={{
                        backgroundColor: "#FfFfFf",
                        borderRadius: "10px",
                        boxShadow:
                          "0px 0.872367px 5.7489px rgba(0, 0, 0, 0.23)",
                        padding: "15px",
                      }}
                    >
                      <div className="w-sale-hover ">
                        <CardMedia
                          component="img"
                          image={item.gatewayLink}
                          onError={(e) => {
                            /**
                             * Any code. For instance, changing the `src` prop with a fallback url.
                             * In our code, I've added `e.target.className = fallback_className` for instance.
                             */
                            e.target.src =
                              "https://mpama.com/wp-content/uploads/2017/04/default-image-620x600.jpg";
                          }}
                          alt="green iguana"
                          style={{
                            height: 280,
                            objectFit: "contain",
                            width: "100%",
                            borderRadius: "10px",
                          }}
                          // {item.gatewayLink}
                        ></CardMedia>
                        {item.featured == true &&
                        item.reqStatus == "approved" ? (
                          <a
                            className="w-sale-hover-inner  cursor-pointer"
                            // onClick={() => featurednft(item._id)}
                            // onClick={handleShow}
                          >
                            Featured
                          </a>
                        ) : item.reqStatus == "pending" ? (
                          <a
                            className="w-sale-hover-inner  cursor-pointer"
                            // onClick={() => featurednft(item._id)}
                          >
                            Featured Request Pending
                          </a>
                        ) : (
                          <a
                            className="w-sale-hover-inner  cursor-pointer"
                            onClick={() => FeaturedHandler(item._id)}

                            // onClick={handleShow}
                          >
                            Get this Featured
                          </a>
                        )}
                      </div>
                      <Link
                        style={{
                          textDecoration: "none",
                          color: "inherit",
                        }}
                        to={{
                          pathname: `/artworkdetailed/${item._id}?nftId=${item._id}&userId=${item?.artistId?._id}`,
                          state: {
                            nftId: item?._id,
                            userId: item?.artistId?._id,
                          },
                        }}
                      >
                        <CardContent className="p-0">
                          <Typography gutterBottom variant="p" component="div">
                            <p
                              className="w-cd-title"
                              data-toggle="tooltip"
                              data-placement="top"
                              title={item?.title}
                            >
                              {item?.title?.length >= 10 ? (
                                <>
                                  {`${item?.title?.substring(0, 10)}...` || " "}
                                </>
                              ) : (
                                <>{`${item?.title}` || " "}</>
                              )}
                            </p>
                            <p
                              className="w-cd-text"
                              data-toggle="tooltip"
                              data-placement="top"
                              title={item?.collectionName}
                              style={{
                                fontSize: "12px",
                              }}
                            >
                              {/* {item.collectionName || ""} */}

                              {item?.collectionName?.length >= 15 ? (
                                <>
                                  {" "}
                                  {`${item?.collectionName?.substring(
                                    0,
                                    15
                                  )}...` || ""}
                                </>
                              ) : (
                                <>{item?.collectionName}</>
                              )}
                            </p>
                            <p
                              className="w-cd-price"
                              data-toggle="tooltip"
                              data-placement="top"
                              title={item?.price}
                            >
                              ADA {item.price || ""}{" "}
                            </p>
                          </Typography>
                        </CardContent>
                      </Link>
                      <button
                        type="button"
                        class="btn btn-sm w-btn-sale mb-1"
                        onClick={() =>
                          setDetaildata({
                            ...detaildata,
                            id: item._id,
                            image: item.gatewayLink,
                            title: item.title,
                            royalty: item.royalty,
                            price: item.price,
                            modalShow: true,
                          })
                        }
                      >
                        Update Price{" "}
                      </button>
                      <button
                        type="button"
                        class="btn btn-sm w-btn-sale"
                        onClick={() => onDeleteButtonClick(item._id)}
                      >
                        Remove from Listing
                      </button>
                    </Card>
                  </>
                ))}
              </div>
            </>
          </div>
        </section>
      ) : (
        <>
          {" "}
          {!loading && dataaLis.length == 0 ? (
            <div className=" gridsm:grid-cols-12 gap-8 py-8 md:py-8  ">
              <>
                <section className="relative">
                  <div className="relative max-w-5xl mx-auto  sm:px-6 text-center">
                    <div className="">
                      {/* Items */}

                      <p>Looks like there's nothing in your listing yet!</p>
                      <p>
                        See what's being created and collected in the{" "}
                        <a href="/market">activity feed</a>
                      </p>
                    </div>
                  </div>
                </section>
              </>
            </div>
          ) : (
            <div className="relative max-w-5xl mx-auto sm:px-6">
              <div
                className="max-w-sm mx-auto grid gap-8 md:grid-cols-3 lg:grid-cols-3 md:max-w-2xl lg:max-w-none"
                style={{ display: "flex", width: "100%", flexDirection: "row" }}
              >
                {[1, 2, 3].map(() => (
                  <div style={{ width: "33%", margin: "1rem" }}>
                    <Skeleton
                      animation="wave"
                      variant="rectangular"
                      width="100%"
                      height={300}
                    />
                    <Skeleton width="100%" />
                    <Skeleton width="100%" />
                    <Skeleton width="100%" />
                    <Skeleton width="100%" />
                  </div>
                ))}
              </div>
            </div>
          )}
        </>
      )}
      {dataaLis?.length > 11 && dataaLis?.length % 12 === 0 && (
        <div
          className="col-md-12 d-flex mt-5"
          style={{ justifyContent: "space-evenly" }}
        >
          {" "}
          <p
            className="btn-sm fontFamily ml-6 mr-6  text-white text-lg rounded-md cursor-pointer"
            style={{ fontWeight: "400", fontSize: "16px" }}
            onClick={() => {
              setCount(count + 1);
              fetchData();
            }}
          >
            {btnLoading && <CircularProgress size={20} className="mr-3" />}
            {btnLoading && <span>Loading </span>}
            {!btnLoading && <span>LoadMore</span>}{" "}
          </p>
        </div>
      )}

      <RemoveOnSale
        refresh={refresh}
        setRefresh={setRefresh}
        item={DeleteItem}
        show={modalShow1}
        onHide={() => setModalShow1(false)}
      />
    </div>
  );
};

export default OnSaleTab;
