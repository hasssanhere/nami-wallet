import React, { useEffect, useState } from "react";
import { Card, CardContent, CardMedia } from "@material-ui/core";
import { getFollowings, unFollowArtist } from "../../../Services/artistSevices";
import { Skeleton } from "@mui/material";

function FollowingList() {
  const [dataa, setDataa] = useState([]);
  const [count, setCount] = useState(1);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getFollowings({ setDataa, count, setLoading });
  }, []);
  const onUnfollowClick = (id) => {
    unFollowArtist(id);
  };

  return (
    <div className=" gridsm:grid-cols-12 gap-8 py-8 md:py-8  ">
      {dataa?.length !== 0 && !loading ? (
        <>
          <section className="relative">
            <div className="relative max-w-5xl mx-auto  sm:px-6">
              <div className="">
                {/* Items */}
                <div className="max-w-sm mx-auto grid md:grid-cols-3 lg:grid-cols-3 md:max-w-2xl lg:max-w-none">
                  {/*Loop Start*/}
                  {/* 1st item */}
                  {dataa?.map((item, key) => {
                    return (
                      <>
                        <Card
                          className="shadow-md"
                          style={{
                            backgroundColor: "#FfFfFf",
                            borderRadius: "10px",
                          }}
                        >
                          <CardContent style={{ padding: "15px" }}>
                            <CardMedia
                              component="img"
                              height="194"
                              image={item.image}
                              onError={(e) => {
                                e.target.src =
                                  "https://mpama.com/wp-content/uploads/2017/04/default-image-620x600.jpg";
                              }}
                              alt="green iguana"
                              style={{
                                height: "250px",
                                objectFit: "cover",
                                width: "100%",
                                borderRadius: "10px",
                              }}
                            />
                          </CardContent>
                          <CardContent>
                            <div className=" font-Raleway grid grid-cols-2 text-left">
                              <div className="sm:col-span-2 md:col-span-2 lg:col-span-2 text-left my-2">
                                <p className="font-bold  text-black-100  ">
                                  {item.username}
                                </p>
                              </div>
                              <div className=" d-grid sm:col-span-2 md:col-span-2 lg:col-span-2 text-center ">
                                <button
                                  className="btn btn-dark"
                                  onClick={() => onUnfollowClick(item._id)}
                                >
                                  UNFOLLOW
                                </button>
                              </div>
                            </div>
                          </CardContent>
                        </Card>
                      </>
                    );
                  })}
                </div>
              </div>
            </div>
          </section>
          {dataa.length > 12 && dataa?.length % 12 === 0 ? (
            <p
              className="btn-sm fontFamily ml-6 mr-6  text-white text-lg rounded-md cursor-pointer"
              style={{ fontWeight: "400", fontSize: "16px" }}
              onClick={() => {
                setCount(count + 1);
              }}
            >
              Load More
            </p>
          ) : null}
        </>
      ) : (
        <>
          <div className="relative max-w-5xl mx-auto sm:px-6">
            <div
              className="max-w-sm mx-auto grid  md:grid-cols-3 lg:grid-cols-3 md:max-w-2xl lg:max-w-none"
              style={{ display: "flex", width: "100%", flexDirection: "row" }}
            >
              {[1, 2, 3].map(() => (
                <div style={{ width: "33%", margin: "1rem" }}>
                  <Skeleton
                    animation="wave"
                    variant="rectangular"
                    width="100%"
                    height={300}
                  />
                  <Skeleton width="100%" />
                  <Skeleton width="100%" />
                  <Skeleton width="100%" />
                  <Skeleton width="100%" />
                </div>
              ))}
            </div>
          </div>
          )
        </>
      )}
      {!loading && !dataa && (
        <>
          <div className=" gridsm:grid-cols-12 gap-8 py-8 md:py-8  ">
            <section className="relative">
              {/* Section background (needs .relative class on parent and next sibling elements) */}

              <div className="relative max-w-5xl mx-auto  sm:px-6 text-center">
                <div className="">
                  // <p>No Followers!</p>
                </div>
              </div>
            </section>
          </div>
        </>
      )}
    </div>
  );
}

export default FollowingList;
