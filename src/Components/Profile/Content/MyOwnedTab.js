import {
  Card,
  CardContent,
  CardMedia,
  CircularProgress,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import Swal from "sweetalert2";
import { ownedNFT } from "../../../Services/ProfileServices";
import DeleteMyOwned from "../Modals/DeleteMyOwned";
import SellNft from "../Modals/SellNft";
import { Link } from "react-router-dom";
import { Skeleton } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import { deleteNFT } from "../../../Services/CreateNFT";
const MyOwnedTab = ({ setCollection, refresh, setRefresh }) => {
  const [data, setdata] = useState([]);
  const [count, setCount] = useState(1);
  const [dataa, setdataa] = useState({});
  const [modalShow1, setModalShow1] = useState(false);
  const [btnLoading, setBtnLoading] = useState(false);

  const [DeleteItem, setDeleteItem] = useState();

  const [loading, setLoading] = useState(true);
  // const [modalShow, setModalShow] = useState(false);
  const [detaildata, setDetaildata] = useState({
    modalShow: false,
    id: "",
    title: "",
    image: "",
    description: "",
  });
  const { id, title, image, modalShow, description } = detaildata;

  // const deletenft = (item) => {
  //   deleteNFT(item, refresh, setRefresh);
  // };
  const onDeleteButtonClick = (item) => {
    setModalShow1(true);
    setDeleteItem(item);
  };

  useEffect(() => {
    // getInventory(setdata, setCollection);
    // ownedNFT({ setdata, count, setLoading });
    ownedNFT({ setdata, count, setLoading });
  }, [refresh, count]);
  const fetchData = () => {
    setBtnLoading(true);
    //Faking API call here
    setTimeout(() => {
      setBtnLoading(false);
    }, 2000);
  };
  return (
    <div className=" gridsm:grid-cols-12 gap-8 py-8 md:py-8 ">
      {data?.length !== 0 && !loading ? (
        <>
          <section className="relative">
            {/* Section background (needs .relative class on parent and next sibling elements) */}

            <div className="relative max-w-5xl mx-auto  sm:px-6">
              <div className="">
                {/* Items */}
                <div className="max-w-sm mx-auto grid gap-8 md:grid-cols-3 lg:grid-cols-3 md:max-w-2xl lg:max-w-none">
                  {/*Loop Start*/}
                  {/* 1st item */}
                  {data?.map((item, key) => {
                    return (
                      <>
                        <Card
                          className="shadow-md"
                          style={{
                            backgroundColor: "#FfFfFf",
                            borderRadius: "10px",
                            // boxShadow: "none",
                            // padding: "10px 15px",
                          }}
                        >
                          <CardContent
                            style={{ padding: "20px 10px 10px 10px" }}
                          >
                            {/* <Link
                              style={{
                                textDecoration: "none",
                                color: "inherit",
                              }}
                              to={{
                                pathname: `/artworkdetailed/${item._id}?nftId=${item._id}&userId=${item?.artistId?._id}`,
                                state: {
                                  nftId: item?._id,
                                  userId: item?.artistId?._id,
                                },
                              }}
                            > */}
                            <CardMedia
                              component="img"
                              // className="cardmediastyling"
                              height="194"
                              image={item.gatewayLink}
                              alt="green iguana"
                              style={{
                                height: 280,
                                objectFit: "contain",
                                width: "100%",
                                borderRadius: "10px",
                              }}
                              onError={(e) => {
                                /**
                                 * Any code. For instance, changing the `src` prop with a fallback url.
                                 * In our code, I've added `e.target.className = fallback_className` for instance.
                                 */
                                e.target.src =
                                  "https://mpama.com/wp-content/uploads/2017/04/default-image-620x600.jpg";
                              }}
                            />

                            <div className=" font-Raleway grid grid-cols-1 text-left">
                              <div className="sm:col-span-2 md:col-span-2 lg:col-span-1 text-left my-2 w-full">
                                <div class="flex items-center justify-between w-full">
                                  <p
                                    className="font-bold text-base	text-black-100 text-break	"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title={item?.title}
                                  >
                                    {item?.title?.length >= 15 ? (
                                      <>
                                        {`${item?.title?.substring(
                                          0,
                                          15
                                        )}...` || " "}
                                      </>
                                    ) : (
                                      <>{`${item?.title}` || " "}</>
                                    )}
                                  </p>
                                  <div className="mt-1 col-span-1 text-right ">
                                    <DeleteIcon
                                      className="text-red-600 cursor-pointer"
                                      onClick={() => onDeleteButtonClick(item)}
                                    />
                                  </div>
                                </div>
                                <p
                                  className=" text-base	text-black-100 text-break	"
                                  data-toggle="tooltip"
                                  data-placement="top"
                                  title={item?.collectionId?.collectionName}
                                >
                                  {/* {item?.collectionId?.collectionName?.length >=
                                  20 ? (
                                    <>
                                      {`${item?.collectionId?.collectionName?.substring(
                                        0,
                                        20
                                      )}...` || " "}
                                    </>
                                  ) : ( */}
                                  <>
                                    {`${item?.collectionId?.collectionName}` ||
                                      " "}
                                  </>
                                  {/* )} */}
                                </p>
                              </div>
                            </div>
                            {/* </Link> */}
                            <div className=" font-Raleway grid grid-cols-2 text-left">
                              <div className=" d-grid sm:col-span-2 md:col-span-2 lg:col-span-2 text-center ">
                                <button
                                  className="btn btn-dark"
                                  onClick={() => {
                                    setDetaildata({
                                      ...detaildata,
                                      id: item._id,
                                      image: item.gatewayLink,
                                      title: item.title,
                                      description: item.description,
                                      modalShow: true,
                                    });
                                    // setModalShow(true);
                                  }}
                                >
                                  <p
                                    style={{
                                      fontWeight: "700",
                                    }}
                                  >
                                    Sell This
                                  </p>
                                </button>
                              </div>
                            </div>
                          </CardContent>
                        </Card>
                      </>
                    );
                  })}
                </div>
              </div>
            </div>
          </section>
          <SellNft
            show={modalShow}
            onHide={() => setDetaildata({ ...detaildata, modalShow: false })}
            setdataa={setdataa}
            refresh={refresh}
            setRefresh={setRefresh}
            id={id}
            image={image}
            title={title}
            description={description}
          />
        </>
      ) : (
        <>
          <div className="relative max-w-5xl mx-auto sm:px-6">
            <div
              className="max-w-sm mx-auto grid gap-8 md:grid-cols-3 lg:grid-cols-3 md:max-w-2xl lg:max-w-none"
              style={{ display: "flex", width: "100%", flexDirection: "row" }}
            >
              {[1, 2, 3].map(() => (
                <div style={{ width: "33%", margin: "1rem" }}>
                  <Skeleton
                    animation="wave"
                    variant="rectangular"
                    width="100%"
                    height={300}
                  />
                  <Skeleton width="100%" />
                  <Skeleton width="100%" />
                  <Skeleton width="100%" />
                  <Skeleton width="100%" />
                </div>
              ))}
            </div>
          </div>
        </>
      )}
      {
        data?.length > 11 && data?.length % 12 === 0 && (
          <div
            className="col-md-12 d-flex mt-5"
            style={{ justifyContent: "space-evenly" }}
          >
            {" "}
            <p
              className="btn-sm fontFamily ml-6 mr-6  text-white text-lg rounded-md cursor-pointer"
              style={{ fontWeight: "400", fontSize: "16px" }}
              onClick={() => {
                setCount(count + 1);
                fetchData();
              }}
            >
              {btnLoading && <CircularProgress size={20} className="mr-3" />}
              {btnLoading && <span>Loading </span>}
              {!btnLoading && <span>Load More</span>}{" "}
            </p>
          </div>
        )
        //  : null
      }
      {!loading && !data && (
        <div className=" gridsm:grid-cols-12 gap-8 py-8 md:py-8  ">
          <>
            <section className="relative">
              {/* Section background (needs .relative class on parent and next sibling elements) */}

              <div className="relative max-w-5xl mx-auto  sm:px-6 text-center">
                <div className="">
                  {/* Items */}

                  <p>Looks like there's nothing in your owned yet!</p>
                  <p>
                    See what's being created and collected in the{" "}
                    <a href="/market">activity feed</a>
                  </p>
                </div>
              </div>
            </section>
          </>
        </div>
      )}
      <DeleteMyOwned
        refresh={refresh}
        setRefresh={setRefresh}
        item={DeleteItem}
        show={modalShow1}
        onHide={() => setModalShow1(false)}
      />
    </div>
  );
};

export default MyOwnedTab;
