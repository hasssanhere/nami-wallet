import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import TabPanel from "../../CommonItems/TabPanel";
import "../../../App/Assets/css/css/Tabpanel.css";
import "../../../App/Assets/css/css/tabPanelCollection.css";
import Box from "@mui/material/Box";
// import config from "../../config";
import Loader from "../../../App/Layout/Loader";
import WalletTab from "./WalletTab";
import MyOwnedTab from "./MyOwnedTab";
import OnSaleTab from "./OnSaleTab";
import ActivityTab from "./ActivityTab";
import EditProfileTab from "./EditProfileTab";

function a11yProps(index) {
  return {
    id: `nav-tab-${index}`,
    "aria-controls": `nav-tabpanel-${index}`,
  };
}

function LinkTab(props) {
  return (
    <Tab
      component="a"
      style={{
        fontFamily: "Raleway",
        fontSize: "16px",
        color: "#000000",
        fontWeight: 500,
      }}
      onClick={(event) => {
        event.preventDefault();
      }}
      {...props}
    />
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function MainDashboard({
  id,
  userDataa,
  setCollection,
  setCreation,
  loading,
}) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const [NoDataCollection, setNoDataCollection] = useState(false);
  const [NoDataCreation, setNoDataCreation] = useState(false);
  const [refresh, setRefresh] = useState(true);
  const [nftid, setNftId] = useState("");

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <Box sx={{ bgcolor: "background.paper" }}>
        <AppBar position="static">
          <Tabs
            className="w-noor-tabs"
            // variant="fullWidth"
            value={value}
            onChange={handleChange}
            aria-label="nav tabs example"
            iconPosition="start"
            centered
            // variant="scrollable"
          >
            <LinkTab label="My Profile" href="/editProfile" {...a11yProps(0)} />
            <LinkTab
              iconPosition="start"
              label="MY NFTs"
              href="/owned"
              {...a11yProps(1)}
              style={{ textTransform: "capitalize" }}
            />
            <LinkTab label="On Sale" href="/sale" {...a11yProps(2)} />
            <LinkTab label="Wallets" href="/wallets" {...a11yProps(3)} />
            <LinkTab label="Activity" href="/activities" {...a11yProps(4)} />
            <LinkTab label="FAVOURITES" href="/fav" {...a11yProps(5)} />
          </Tabs>
        </AppBar>
      </Box>
      {loading ? (
        <>
          <Loader />
        </>
      ) : (
        <>
          <TabPanel value={value} index={0}>
            <EditProfileTab id={id} />
          </TabPanel>
          <TabPanel value={value} index={1}>
            <MyOwnedTab
              setCollection={setCollection}
              refresh={refresh}
              setRefresh={setRefresh}
            />
          </TabPanel>
          <TabPanel value={value} index={2}>
            <OnSaleTab
              setCreation={setCreation}
              refresh={refresh}
              setRefresh={setRefresh}
            />
          </TabPanel>

          {/* WAllet SECTION */}
          <TabPanel value={value} index={3}>
            <WalletTab userDataa={userDataa} />
          </TabPanel>

          {/* Activity SECTION */}
          <TabPanel value={value} index={4}>
            <ActivityTab />
          </TabPanel>
        </>
      )}
    </div>
  );
}
