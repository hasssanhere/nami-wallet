import React from 'react'
import TableRow from '@mui/material/TableRow';
import TableCell from '@mui/material/TableCell';
import noorImage from "../../../App/Assets/images/MaskGroup.png"
import SoldIcon from '../../../App/Assets/images/SVGIcons/SoldIcon';

const ActivityTabTableRow = () => {
    return (
        <TableRow>
            <TableCell>
                <p className="d-flex align-items-center ">
                    <SoldIcon /> <span className="ml-3 w-text-black">Sold</span>
                </p>
            </TableCell>
            <TableCell>
                <div className="d-flex align-items-center">
                    <div>
                        <img width="50" height="50" src={noorImage} />
                    </div>
                    <div className="ml-2">
                        <p>99 Name of Allah</p>
                        <p className="w-text-black">NFT #14586</p>
                    </div>
                </div>
            </TableCell>
            <TableCell>2ADA</TableCell>
            <TableCell>Ahmed</TableCell>
            <TableCell>Muhammad</TableCell>
            <TableCell>14 days ago</TableCell>
        </TableRow>
    )
}

export default ActivityTabTableRow;
