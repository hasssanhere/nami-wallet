import React, { useState, useEffect } from "react";
import MainDashboard from "./TabPanelCollection";
import "../../../App/Assets/css/css/profile.css";
import {
  getUserDetailsbyID,
  UploadPimage,
} from "../../../Services/artistSevices";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import CameraAltIcon from "@mui/icons-material/CameraAlt";
import { SERVER_URL } from "../../../Routes/serverRoute";
import Swal from "sweetalert2";
import { flushSync } from "react-dom";
import { Link } from "react-router-dom";
import FollowersList from "./FollowersList";
import FollowingList from "./FollowingList";
import bannerimg from "../../../App/Assets/images/profile-banner.png";
import { Skeleton } from "@mui/material";

function MainUserDetail({ id }) {
  const [collection, setCollection] = useState("0");
  const [creation, setCreation] = useState("0");
  const [loading2, setLoading2] = useState(false);
  const [userDataa, setUserDataa] = useState({});
  const [profile, setProfile] = useState({});
  const [refresh, setRefresh] = useState(false);
  const [followersList, ShowFollowersList] = useState(false);
  const [tabpannel, showTabPanel] = useState(true);
  const [followingList, showFollowingList] = useState(false);

  const checkMimeType = (event) => {
    let files = event.target.files[0];
    let err = "";
    const types = ["image/png", "image/jpeg", "image/jpg"];
    if (types.every((type) => files.type !== type)) {
      err += files.type + " is not a supported format\n";

      Swal.fire({
        toast: true,
        icon: "error",
        text: err,
      });
    }

    if (err !== "") {
      event.target.value = null;
      return false;
    }
    return true;
  };
  const profilePic = (e) => {
    flushSync(() => {
      setProfile(e.target.files[0]);
    });
    var data = new FormData();
    data.append("imageUpload", e.target.files[0]);
    UploadPimage(data, setRefresh, refresh);
  };

  useEffect(() => {
    getUserDetailsbyID(id, setUserDataa);
  }, [refresh]);

  const OnClickFollowers = () => {
    showTabPanel(false);
    ShowFollowersList(true);
    showFollowingList(false);
  };
  const OnClickFollowing = () => {
    ShowFollowersList(false);
    showTabPanel(false);
    showFollowingList(true);
  };
  return (
    <>
      <div className="max-w-max   ">
        <>
          <img class="card-img-top" src={bannerimg} alt="Card image cap" />
          <div class="card-body little-profile text-center ">
            <div class="pro-img relative">
              <img
                className="pimage"
                src={userDataa?.image}
                // alt="user"
                onError={(e) => {
                  /**
                   * Any code. For instance, changing the `src` prop with a fallback url.
                   * In our code, I've added `e.target.className = fallback_className` for instance.
                   */
                  e.target.src =
                    "https://media.istockphoto.com/vectors/male-profile-flat-blue-simple-icon-with-long-shadow-vector-id522855255?k=20&m=522855255&s=612x612&w=0&h=fLLvwEbgOmSzk1_jQ0MgDATEVcVOh_kqEe0rqi7aM5A=";
                }}
              />
            </div>
            <h3 class="mb-2 f-36 w-username">
              {Object.keys(userDataa).length === 0 ? (
                <Skeleton width="100%" />
              ) : (
                <> {userDataa?.flname}</>
              )}
            </h3>
            <p className="f-20 text-black">
              {Object.keys(userDataa).length === 0 ? (
                <Skeleton width="100%" />
              ) : (
                <>
                  {" "}
                  <p>
                    @{userDataa?.username}{" "}
                    {userDataa?.reqStatus ? (
                      <CheckCircleIcon
                        style={{ fontSize: "25", color: "#367BF5" }}
                      />
                    ) : null}
                  </p>
                </>
              )}
            </p>{" "}
            <div class="d-flex justify-content-center px-50 mt-4">
              <div class="d-flex align-items-center w-folow">
                <p>{userDataa?.followers?.length || 0}</p>{" "}
                <Link onClick={OnClickFollowers}>Followers</Link>
              </div>
              <div class="d-flex align-items-center w-folow">
                {" "}
                <p>{userDataa?.following?.length}</p>{" "}
                <Link onClick={OnClickFollowing}>Following</Link>
              </div>
            </div>
          </div>
        </>
      </div>
      {tabpannel && (
        <div className="mt-5">
          <MainDashboard
            id={id}
            setCollection={setCollection}
            setCreation={setCreation}
            setLoading={setLoading2}
            loading={loading2}
            userDataa={userDataa}
          />
        </div>
      )}
      {followersList && (
        <div className="mt-5">
          <FollowersList />
        </div>
      )}
      {followingList && (
        <div className="mt-5">
          <FollowingList />
        </div>
      )}
    </>
  );
}
export default MainUserDetail;
