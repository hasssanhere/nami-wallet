import React, { useEffect, useState } from "react";
import Swal from "sweetalert2";
import { getUserDetailsbyID } from "../../../Services/artistSevices";
import {
  addWallet,
  makewalletPrimary,
} from "../../../Services/ProfileServices";
import HighlightOffIcon from "@mui/icons-material/HighlightOff";
import DeleteModal from "../Modals/DeleteModal";

const Toast = Swal.mixin({
  toast: true,
  position: "top-end",
  showConfirmButton: false,
  timer: 1000,
  timerProgressBar: true,
  didOpen: (toast) => {
    toast.addEventListener("mouseenter", Swal.stopTimer);
    toast.addEventListener("mouseleave", Swal.resumeTimer);
  },
});

const WalletTab = ({ userDataa }) => {
  const [data, setUserDataa] = useState({});

  const [walletaddress, setwalletaddress] = useState("");
  const [modalShow, setModalShow] = useState(false);
  const [refresh, setrefresh] = useState(false);

  const connectWallet = async () => {
    const cardano = await window.cardano;
    if (cardano) {
      await cardano?.enable();
      const enable = await cardano?.isEnabled();
      if (enable) {
        const walletaddress = await cardano?.getUsedAddresses();
        const body = {
          address: walletaddress[0],
        };

        addWallet(body, setrefresh, refresh);
      } else {
        Toast.fire({
          icon: "error",
          text: "Some Problem in Enabling Nami",
        });
      }
    } else {
      Toast.fire({
        toast: true,
        icon: "error",
        text: "Kindly Install a Nami wallet in your Browser",
      });
    }
  };

  const disconnectWallet = (address) => {
    setwalletaddress(address);
    setModalShow(true);
  };

  useEffect(() => {
    const id = sessionStorage.getItem("id");
    getUserDetailsbyID(id, setUserDataa);
  }, [refresh]);
  return (
    <>
      {" "}
      <div className="max-w-6xl mt-5 mx-auto p-6 sm:px-6 bg-white-100 ">
        {/* {MAPping of wallet adress will start from here} */}
        {data?.address?.length !== 0 ? (
          <>
            {data?.address?.map((item) => (
              <div className="max-w-4xl mt-1 mx-auto px-20 sm:px-6">
                <div className="rounded-md bg-gray-200 p-3 ">
                  <div class="grid grid-cols-3 gap-4">
                    <div
                      class="col-span-2 text-break"
                      onClick={() => {
                        sessionStorage.setItem("walletaddress", item);
                        Toast.fire({
                          icon: "success",
                          text: "Primary Wallet Set",
                        });
                      }}
                    >
                      {item}
                    </div>
                    <div class="col-span-1 gap-4 text-right">
                      {data.primaryAddress !== item ? (
                        <button
                          className="btn-sm fontFamily mr-4 text-white text-lg rounded-md cursor-pointer"
                          style={{ fontWeight: "400", fontSize: "16px" }}
                          onClick={() =>
                            makewalletPrimary(
                              {
                                primaryAddress: item,
                              },
                              refresh,
                              setrefresh
                            )
                          }
                        >
                          Make Default
                        </button>
                      ) : (
                        <button
                          className="btn-sm fontFamily mr-4 text-white text-lg rounded-md cursor-pointer"
                          style={{ fontWeight: "400", fontSize: "16px" }}
                        >
                          Default Selected
                        </button>
                      )}
                      <HighlightOffIcon
                        onClick={() => disconnectWallet(item)}
                      />
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </>
        ) : (
          <div className="max-w-4xl mt-1 mx-auto px-20 sm:px-6">
            <div className="rounded-md bg-gray-200 p-3 ">
              <div class="grid grid-cols-3 gap-4">
                <div class="col-span-3 text-break text-center">
                  You don't not have Any wallet Connected
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
      <h2 className=" fontFamily text-4xl font-bold font-heading text-center">
        <button
          // data-aos="zoom-y-out"
          className="mt-8 mb-8 btn btn-big text-center rounded-md "
          onClick={connectWallet}
        >
          <strong>Add Wallet</strong>
        </button>
      </h2>
      <DeleteModal
        show={modalShow}
        onHide={() => setModalShow(false)}
        walletaddress={walletaddress}
        refresh={refresh}
        setdata={setUserDataa}
        setrefresh={setrefresh}
      />
    </>
  );
};

export default WalletTab;
