import React, { useState, useEffect } from "react";
// import Header from "../partials/Header";
// import Footer from "../partials/Footer";
import "react-dropdown/style.css";
import { useHistory, useParams, withRouter } from "react-router-dom";

import MainUserDetail from "./Content/MainUserDetail";
function UserDetail() {
  const id = sessionStorage.getItem("id");
  console.log(id);
  return (
    <>
      <div className="wmt-67">{/* Top area: Blocks */}</div>
      <MainUserDetail id={id} />
    </>
  );
}

export default withRouter(UserDetail);
