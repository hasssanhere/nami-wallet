import React, { useState } from "react";
import { useTheme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import Chip from "@mui/material/Chip";
import { Modal } from "react-bootstrap";

const CollectionDropDown = ({
  collections,
  setformdata,
  formdata,
  setShow,
}) => {
  const { collectionName } = formdata;

  const SelectHandler = (e) => {
    setformdata({ ...formdata, collectionName: e.target.value });
  };

  return (
    <div>
      <FormControl className="w-nft-dropdown" sx={{ width: "100%" }}>
        <Select
          value={collectionName}
          onChange={SelectHandler}
          displayEmpty
          inputProps={{ "aria-label": "Without label" }}
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          label=""
        >
          {collections?.map((item) => (
            <MenuItem key={item._id} value={item.collectionName}>
              {item.collectionName}
            </MenuItem>
          ))}
          <MenuItem
            className="w-new-collec"
            onClick={() => {
              setShow(true);
            }}
          >
            New Collection
          </MenuItem>
        </Select>
      </FormControl>
    </div>
  );
};

export default CollectionDropDown;
