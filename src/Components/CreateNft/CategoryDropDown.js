import React from "react";
import { useTheme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import Chip from "@mui/material/Chip";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const names = ["Art", "Graphics", "Others"];

function getStyles(name, personName, theme) {
  return {
    fontWeight:
      personName.indexOf(name) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
  };
}

const CategoryDropDown = ({ setformdata, formdata }) => {
  const { category } = formdata;
  const theme = useTheme();
  const [personName, setPersonName] = React.useState([]);

  // const handleChange = (event) => {
  //   const {
  //     target: { value },
  //   } = event;
  //   setPersonName(
  //     // On autofill we get a stringified value.
  //     typeof value === "string" ? value.split(",") : value
  //   );
  // };
  const SelectHandler = (e) => {
    setformdata({ ...formdata, category: e.target.value });
  };
  return (
    <div>
      <FormControl className="w-nft-dropdown" sx={{ width: "100%" }}>
        <Select
          // labelId="demo-simple-select-label"
          inputProps={{ "aria-label": "Without label" }}
          displayEmpty
          id="demo-simple-select"
          value={category}
          // label=""
          label={false}
          onChange={SelectHandler}
        >
          {names?.map((names) => (
            <MenuItem key={names} value={names}>
              {names}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
};

export default CategoryDropDown;
