import React, { useState, useEffect, useRef } from "react";
// import Header from "../partials/Header";
// import Footer from "../partials/Footer";
import "react-dropdown/style.css";
import { Button, Image } from "react-bootstrap";
import { createNewCollection, getCollections } from "../../Services/CreateNFT";
import Swal from "sweetalert2";
import { TextField } from "@material-ui/core";
import NFTUpload from "../../App/Assets/images/NFT-Upload.png";
import UploadNFT from "../../App/Assets/images/UploadNFT.png";
import CategoryDropDown from "./CategoryDropDown";
import CollectionDropDown from "./CollectionDropDown";
import { Modal } from "react-bootstrap";
import { CreateNFT } from "../../Services/blockchainServices";
import { CircularProgress } from "@mui/material";

function SignUpArtist() {
  // const [formdata, setFormData] = useState({
  //   assetName: "",
  //   description: "",
  //   projectId: sessionStorage.getItem("projectId"),
  // });
  // const [assetImage, setAssetImage] = useState({});
  // // const [category, setCategory] = useState("");
  // const { assetName, description } = formdata;

  // const checkMimeType = (event) => {
  //   let files = event.target.files[0];
  //   let err = "";
  //   const types = ["image/png", "image/jpeg", "image/jpg"];
  //   if (types.every((type) => files.type !== type)) {
  //     err += files.type + " is not a supported format\n";
  //     // toast.error(err, { draggable: true });
  //     Swal.fire({
  //       title: "Error!",
  //       text: err,
  //       icon: "error",
  //       confirmButtonText: "Okay",
  //     });
  //   }

  //   if (err !== "") {
  //     event.target.value = null;
  //     return false;
  //   }
  //   return true;
  // };

  // const changeHandlerText = (e) => {
  //   setFormData({ ...formdata, [e.target.name]: e.target.value });
  // };

  // const onAssetFileChange = (e) => {
  //   console.log(e.target.value);
  //   if (checkMimeType(e)) {
  //     setAssetImage();
  //   }
  // };

  // const clickHandler = () => {
  //   var data = new FormData();
  //   data.append("assetImage", assetImage);
  //   console.log("form data is==>", formdata);

  //   CreateNFT({ data, formdata });

  //   // console.log(formdata);
  //   setFormData({
  //     assetName: "",
  //     description: "",
  //     projectId: sessionStorage.getItem("projectId"),
  //   });
  //   setAssetImage({});
  //   // setCategory("");
  // };
  // // const handleChange = (e) => {
  // //   setCategory(e.target.value);
  // // };

  const [collections, setCollections] = useState(null);
  const [btnLoading, setBtnLoading] = useState(false);

  const [formdata, setformdata] = useState({
    title: "",
    description: "",
    externalLink: "",
    royality: "",
    collectionName: "",
    category: "",
    newCollection: "",
    mimetype: "",
    imageUpload: {},
    image: UploadNFT || "",
  });

  const {
    title,
    description,
    externalLink,
    royality,
    collectionName,
    imageUpload,
    image,
    category,
    mimetype,
    newCollection,
  } = formdata;
  const [refresh, setRefresh] = useState(false);
  const [show, setShow] = useState(false);
  const [showText, setShowText] = useState(true);

  const handleClose = () => {
    setShow(false);
    setformdata({ newCollection: "" });
  };
  const handleShow = () => setShow(true);

  const handleImageClick = () => {
    supported.current.style.visibility = "hidden";
  };

  const supported = useRef();

  const changeHandler = (e) => {
    setformdata({ ...formdata, [e.target.name]: e.target.value });
  };

  const collectionHandler = () => {
    const body = {
      collectionName: newCollection,
    };
    createNewCollection({ body, refresh, setRefresh });
    handleClose();
    setformdata({ newCollection: "" });
  };

  const checkMimeType = (event) => {
    let files = event.target.files[0];
    let err = "";
    const types = ["image/png", "image/jpeg", "image/jpg"];
    if (types.every((type) => files.type !== type)) {
      err += files.type + " is not a supported format\n";
      Swal.fire({
        title: "Error!",
        text: err,
        icon: "error",
        confirmButtonText: "Okay",
      });
    }

    if (err !== "") {
      event.target.value = null;
      return false;
    }
    return true;
  };

  const NftchangeImage = (e) => {
    if (checkMimeType(e)) {
      setformdata({
        ...formdata,
        image: URL.createObjectURL(e.target.files[0]),
        imageUpload: e.target.files[0],
        mimetype: e.target.files[0].type,
      });
      setShowText(false);
    }
  };

  const submitHandler = (e) => {
    if (
      sessionStorage.getItem("projectId") !== "null" ||
      sessionStorage.getItem("walletAddress") !== "null"
    ) {
      const obj = new FormData();
      obj.append("imageUpload", imageUpload);

      if (!title || !description || !imageUpload || !image || !mimetype) {
        Swal.fire({
          icon: "error",
          text: "Kindly Fill all fields",
        });
      } else {
        CreateNFT({ obj, formdata });
      }
    } else {
      Swal.fire({
        icon: "error",
        text: "Your Wallet Address is not yet approved by the Admin",
      });
    }
  };

  useEffect(() => {
    getCollections(setCollections);
  }, [refresh]);
  const fetchData = () => {
    setBtnLoading(true);
    //Faking API call here
    setTimeout(() => {
      setBtnLoading(false);
    }, 3000);
  };
  return (
    <>
      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
        className="w-onSale-modal"
      >
        <Modal.Header closeButton>
          <Modal.Title>Create a new Collection</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <label className="nft-form-lable">Collection Name</label>
          <input
            type="text"
            className="form-control"
            placeholder="New Collection"
            name="newCollection"
            value={newCollection}
            onChange={changeHandler}
            // maxlength="15"
          />

          <div className="text-right mt-4">
            <button
              className="btn btn-dark"
              onClick={() => {
                collectionHandler();
              }}
            >
              Submit
            </button>
          </div>
        </Modal.Body>
      </Modal>

      <div className="max-w-7xl  mt-5 pt-4 md:pt-5 mx-auto px-0 sm:px-6">
        {/* Top area: Blocks */}
        <div className=" grid gap-8 py-6 md:py-20 md:py-12">
          <div style={{ marginBottom: "50px" }} className="container ">
            <div className="row n-uploadNFTResponsive">
              <div className="col-sm-6 block-to-disappear-in-mobile n-uploadNFTImage">
                <label
                  for="NftImage"
                  class="n-uploadNFT w-nft-image flex flex-col justify-center items-center"
                >
                  <div className="flex flex-col justify-center items-center grow">
                    <input
                      type="file"
                      id="NftImage"
                      className="d-none"
                      onChange={(e) => {
                        NftchangeImage(e);
                      }}
                    />
                    <div class="flex flex-col justify-center items-center grow">
                      <div class="n-toUploadImage">
                        <img
                          src={image}
                          layout="cover"
                          alt="image htmlFor add doctor"
                          style={{
                            width: "392px",
                            height: "402px",
                            objectFit: "contain",
                          }}
                        />
                      </div>
                      {/* <div>
                      </div> */}
                    </div>
                  </div>
                </label>
                <p className="w-nft-file-text " ref={supported}>
                  Supported file formates: <span>.jpg, .jpeg, .png</span>
                </p>
              </div>
              <div className="col-sm-6">
                <div>
                  <h2 className="w-nft-main-title">Create an NFT</h2>
                  <p className="w-nft-main-text">
                    If you’re ready to sell this NFT, you can set the price and{" "}
                    <br />
                    sale type on the next page.
                  </p>
                </div>
                <for method="POST" className="form-group">
                  <input
                    type="hidden"
                    name="csrfmiddlewaretoken"
                    value="fIwiR9rbZTmvxfmW8gC8CiS93Zx36iAh0kdWjuhKGglTMld96xGITqBEbdBR4EkY"
                  />
                </for>
                <form className="w-nft-form">
                  <label className="nft-form-lable">Title</label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Name your NFT"
                    name="title"
                    value={title}
                    onChange={changeHandler}
                    required
                  />
                  <label className="nft-form-lable">Description</label>
                  <textarea
                    type="text"
                    className="form-control"
                    placeholder="Description (Max 64 Character)"
                    name="description"
                    // placeholder="Write a compelling story about your NFT "
                    requiredx
                    multiline
                    rows="5"
                    name="description"
                    value={description}
                    onChange={changeHandler}
                    required
                  ></textarea>
                  <label className="nft-form-lable">Royality </label>
                  <input
                    type="number"
                    className="form-control"
                    placeholder="Royality in Percentage"
                    name="royality"
                    value={royality}
                    // oninput={(e) => if(e.target.value <=-1){Swal.fire({
                    //   icon:"error"
                    // })}
                    // }
                    onChange={(e) => {
                      if (e.target.value <= -1) {
                        Swal.fire({
                          icon: "error",
                          text: "Enter Positive Value",
                        });
                      } else {
                        changeHandler(e);
                      }
                    }}
                    required
                    min="0"
                  />
                  <label className="nft-form-lable">External Link</label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Optional URL"
                    name="externalLink"
                    value={externalLink}
                    onChange={changeHandler}
                  />
                  <label htmlFor="category" className="nft-form-lable">
                    Collection
                  </label>{" "}
                  <CollectionDropDown
                    collections={collections}
                    setShow={setShow}
                    setformdata={setformdata}
                    formdata={formdata}
                  />
                  <label className="nft-form-lable">Category</label>
                  <CategoryDropDown
                    setformdata={setformdata}
                    formdata={formdata}
                  />
                </form>

                <div className="col-sm-6 block-to-disappear-in-mobile mt-4 n-uploadNFTImageResponsive">
                  <div className="n-uploadNFT w-nft-image flex flex-col justify-center items-center grow">
                    <label
                      for="NftImage"
                      class="n-uploadNFT w-nft-image flex flex-col justify-center items-center"
                    >
                      <div className="flex flex-col justify-center items-center grow">
                        <input
                          type="file"
                          id="NftImage"
                          className="d-none"
                          onChange={(e) => {
                            NftchangeImage(e);
                          }}
                        />
                        <div class="flex flex-col justify-center items-center grow">
                          <div class="n-toUploadImage">
                            <img
                              src={image}
                              layout="cover"
                              alt="image htmlFor add doctor"
                              style={{
                                width: "392px",
                                height: "402px",
                                objectFit: "cover",
                              }}
                            />
                          </div>
                          {/* <div>
                      </div> */}
                        </div>
                      </div>
                    </label>
                  </div>
                  <p className="w-nft-file-text">
                    Supported file formates: <span>.jpg, .jpeg, .png</span>
                  </p>
                </div>

                <Button
                  // onClick={clickHandler}
                  className=" mt-5 w-btn-nft"
                  type="submit"
                  onClick={(e) => {
                    submitHandler(e);
                    fetchData();
                  }}
                >
                  {btnLoading && (
                    <CircularProgress size={20} className="mr-3" />
                  )}
                  {btnLoading && <span>Loading </span>}
                  {!btnLoading && <span> Create NFT</span>}
                </Button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
export default SignUpArtist;
