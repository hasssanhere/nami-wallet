import React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardActions from "@mui/material/CardActions";
import Collapse from "@mui/material/Collapse";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { styled } from "@mui/material/styles";
import IconButton from "@mui/material/IconButton";

const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? "rotate(0deg)" : "rotate(180deg)",
  marginLeft: "auto",
  transition: theme.transitions.create("transform", {
    duration: theme.transitions.duration.shortest,
  }),
}));

const NftSec = ({ nftDetail, nftDetailAdv, expanding, setExpanding }) => {
  const { expend2, expend3 } = expanding;

  const handleExpandClick = () => {
    setExpanding({ ...expanding, expend2: !expend2 });
  };

  const handleExpandClick2 = () => {
    setExpanding({ ...expanding, expend3: !expend3 });
  };
  console.log(nftDetailAdv);
  return (
    <>
      <div>
        <Card
          className="mt-3 border"
          style={{ boxShadow: "none", borderRadius: "5px" }}
        >
          <CardActions disableSpacing>
            <h2>Asset Details</h2>
            <ExpandMore
              expand={expend2}
              onClick={handleExpandClick}
              aria-expend2={expend2}
              aria-label="show more"
            >
              <ExpandMoreIcon />
            </ExpandMore>
          </CardActions>
          <Collapse in={expend2} timeout="auto" unmountOnExit>
            <CardContent class="border-t" style={{ padding: "10px" }}>
              <div class="row">
                <div class="col w-1/2">
                  {" "}
                  <div className="rounded-lg cardsincard ">
                    <h3 className="n-lable">Asset ID</h3>
                    <p className="n-value break-all">{nftDetailAdv?.assetid}</p>
                  </div>
                </div>
                <div class="col">
                  {" "}
                  <div className="rounded-lg cardsincard ">
                    <h3 className="n-lable">Asset Name</h3>
                    <p className="n-value uppercase">{nftDetailAdv?.name}</p>
                  </div>
                </div>
              </div>
              <div class=" mt-3 row">
                <div class="col w-1/2">
                  {" "}
                  <div className="rounded-lg cardsincard ">
                    <h3 className="n-lable">Policy ID</h3>
                    <p className="n-value break-all">
                      {nftDetailAdv?.policyid}
                    </p>
                  </div>
                </div>
                <div class="col">
                  {" "}
                  <div className="rounded-lg cardsincard ">
                    <h3 className="n-lable">Listing ID</h3>
                    <p className="n-value">{nftDetailAdv?.id}</p>
                  </div>
                </div>
              </div>
              <div class=" mt-3 row">
                <div class="col">
                  {" "}
                  <div className="rounded-lg cardsincard ">
                    <h3 className="n-lable">Quantity</h3>
                    <p className="n-value">1</p>
                  </div>
                </div>
                <div class="col">
                  {" "}
                  <div className="rounded-lg cardsincard ">
                    <h3 className="n-lable">Minted</h3>
                    <p className="n-value capitalize">
                      {nftDetailAdv?.minted?.toString()}
                    </p>
                  </div>
                </div>
              </div>
            </CardContent>
          </Collapse>
        </Card>
      </div>
      <div>
        {/* <Card
          className="mt-3 border"
          style={{ boxShadow: "none", borderRadius: "5px" }}
        >
          <CardActions disableSpacing>
         
            <h2>Asset Tags</h2>
            <ExpandMore
              expand={expend3}
              onClick={handleExpandClick2}
              aria-expanded={expend3}
              aria-label="show more"
            >
              <ExpandMoreIcon />
            </ExpandMore>
          </CardActions>
          <Collapse in={expend3} timeout="auto" unmountOnExit>
            <CardContent class="border-t" style={{ padding: "10px" }}>
              <div class="row">
                <div class="col">
                  {" "}
                  <div className="rounded-lg cardsincard ">
                    <h3 className="n-lable">Asset ID</h3>
                    <p className="n-value text-break ">{nftDetail?._id}</p>
                  </div>
                </div>
                <div class="col">
                  {" "}
                  <div className="rounded-lg cardsincard ">
                    <h3 className="n-lable">Asset ID</h3>
                    <p className="n-value">{nftDetail?._id}</p>
                  </div>
                </div>
              </div>
              <div class=" mt-3 row">
                <div class="col">
                  {" "}
                  <div className="rounded-lg cardsincard ">
                    <h3 className="n-lable">Asset ID</h3>
                    <p className="n-value">{nftDetail?._id}</p>
                  </div>
                </div>
              </div>
            </CardContent>
          </Collapse>
        </Card> */}
      </div>
    </>
  );
};

export default NftSec;
