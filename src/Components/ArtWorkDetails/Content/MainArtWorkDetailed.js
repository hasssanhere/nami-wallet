import React, { useEffect, useState } from "react";
// import { SERVER_URL } from "../../../Routes/serverRoute";
import "../../../App/Assets/css/css/check.css";
import {
  FacebookShareButton,
  TwitterShareButton,
  RedditShareButton,
  EmailShareButton,
} from "react-share";
import RedditIcon from "@mui/icons-material/Reddit";
import FacebookIcon from "@mui/icons-material/Facebook";
import TwitterIcon from "@mui/icons-material/Twitter";
import EmailIcon from "@mui/icons-material/Email";
import VisibilityIcon from "@mui/icons-material/Visibility";
import { makeStyles } from "@material-ui/core/styles";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import "../../../App/Assets/css/css/artworkDetail.css";

import { Button } from "react-bootstrap";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";

import {
  getArtistDetailbyId,
  getParticularNftDetail,
} from "../../../Services/artistSevices";
import { addViewCount, getUsdPrice } from "../../../Services/MarketServices";
import ImageSec from "./ImageSec";
import NftSec from "./NftSec";
import { Link } from "react-router-dom";
import cardino from "../../../App/Assets/images/cardano.png";
import { mintNFt } from "../../../Services/blockchainServices";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(0),
    },
  },
}));

function MainArtWorkDetailed({ nftId, userId, setModalSignIn }) {
  const [nftDetail, setNftDetail] = useState({});
  const [nftDetailAdv, setNftDetailAdv] = useState({});
  const [userData, setUserData] = useState({});
  const [usd, setUsd] = useState("");

  const [expanding, setExpanding] = useState({
    expend1: false,
    expend2: true,
    expend3: false,
  });

  //--------------------GETTING NFT DETAILS-------------------------------------------------------------
  useEffect(() => {
    //GETTING NFT DETAILS
    getParticularNftDetail(nftId, setNftDetail, setNftDetailAdv);

    //UserDetail getting
    getArtistDetailbyId(userId, setUserData);
    //ADDING VIEW COUNTS
    addViewCount({ nftId, userId });
    getUsdPrice(setUsd);
  }, []);

  const URL = "http://165.227.195.188:3000/";
  const urlSearchParams = new URLSearchParams(window.location.search);
  const params = Object.fromEntries(urlSearchParams.entries());
  const newUSD = nftDetail?.price * usd;
  var rounded = Math.round(newUSD * 10) / 10;

  console.log(nftDetailAdv);
  return (
    <>
      <>
        <div className="mt-20 md:mt-40 n-artWorkDetails">
          <ImageSec
            nftDetail={nftDetail}
            expanding={expanding}
            setExpanding={setExpanding}
            userData={userData}
          />

          <div className=" sm:col-span-12 lg:col-span-6  n-nftDetailsDescription">
            <div className="artworkDetailCol2Header">
              <Link
                // to={`/artist-detail/${nft.artistId}`}
                to={{
                  pathname: `/artist-detail/${userData.username}`,
                  state: {
                    userId: userData._id,
                  },
                }}
                className="font-normal text-lg text-blue-700 text-break mb-2"
                data-toggle="tooltip"
                data-placement="top"
                title={userData?.flname}
              >
                {userData?.flname?.toUpperCase()}{" "}
                <CheckCircleIcon
                  style={{
                    fontSize: "20",
                    color: "#367BF5",
                  }}
                />
              </Link>
              <h1
                title={nftDetail?.title}
                className=" uppercase detailpageheading "
              >
                {nftDetail?.title?.length > 12 ? (
                  <>{`${nftDetail?.title?.substring?.(0, 11)}...` || " "}</>
                ) : (
                  <>{`${nftDetail?.title}` || " "}</>
                )}
              </h1>

              <div className="detailpanelicons">
                <div>
                  <FavoriteBorderIcon className=" float-left" />{" "}
                  <p className=" float-left ml-1"> 3 favourites</p>
                  <VisibilityIcon className=" float-left ml-8" />
                  <p className=" float-left ml-1">
                    {" "}
                    {nftDetail?.viewCount || "0"} views
                  </p>
                </div>
                <div>
                  <FacebookShareButton
                    url={`${URL}artworkdetailed/${nftId}`}
                    // src={nftDetail?.gatewayLink}
                    quote={nftDetail?.title || "0"}
                    hashtag={"#hashtag"}
                    description={"aiueo"}
                    className="Demo__some-network__share-button"
                  >
                    <FacebookIcon className=" ml-8 float-right " />
                  </FacebookShareButton>
                  <TwitterShareButton
                    title={"test"}
                    url={`${URL}artworkdetailed/${nftId}?nftId=${params.nftId}&userId=${params.userId}`}
                    hashtags={["hashtag1", "hashtag2"]}
                  >
                    <TwitterIcon className=" ml-8 float-right " />
                  </TwitterShareButton>
                  <RedditShareButton
                    title={"test"}
                    url={`${URL}artworkdetailed/${nftId}`}
                    hashtags={["hashtag1", "hashtag2"]}
                  >
                    <RedditIcon className=" ml-8 float-right " />
                  </RedditShareButton>
                  <EmailShareButton
                    title={"test"}
                    subject="NoorNFT"
                    url={`${URL}artworkdetailed/${nftId}`}
                    hashtags={["hashtag1", "hashtag2"]}
                  >
                    <EmailIcon className=" ml-8 float-right " />
                  </EmailShareButton>
                </div>
              </div>
            </div>

            {/*Loop Start */}
            <div className="artworkDetailCol2tokenDetail ">
              <div>
                <div
                  className="border flex justify-between items-center"
                  style={{
                    padding: "10px",
                    borderRadius: "5px",
                    width: "100%",
                  }}
                >
                  <div>
                    <h3
                      style={{
                        fontStyle: "normal",
                        fontSize: "16px",
                        lineHeight: " 19px",
                        color: "black",
                        fontWeight: "normal",
                      }}
                    >
                      Buy now price
                    </h3>
                    <div class="flex items-end mt-3">
                      <img
                        src={cardino}
                        style={{
                          height: "35px",
                        }}
                      />
                      <p className="adaPrice">{nftDetail?.price} </p>
                      {/* <p className="adaPrice1"></p> */}
                      <p className="adaPrice1 text-sm ml-2">
                        {" "}
                        (${rounded} USD)
                      </p>
                    </div>
                  </div>
                  <Button
                    variant="dark"
                    style={{ borderRadius: "5px" }}
                    onClick={() => {
                      if (sessionStorage.getItem("id")) {
                        mintNFt({
                          nftprojectid: userData.projectId,
                          nftid: nftDetailAdv.id,
                          receiveraddress: userData.primaryAddress,
                        });
                      } else {
                        setModalSignIn(true);
                      }
                    }}
                  >
                    Buy Now
                  </Button>
                </div>
                <NftSec
                  expanding={expanding}
                  setExpanding={setExpanding}
                  // nftDetail={nftDetail}
                  nftDetailAdv={nftDetailAdv}
                />
              </div>
            </div>
          </div>
        </div>
      </>
    </>
  );
}
export default MainArtWorkDetailed;
