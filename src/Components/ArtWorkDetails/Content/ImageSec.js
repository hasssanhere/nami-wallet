import React, { useState } from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { styled } from "@mui/material/styles";
import IconButton from "@mui/material/IconButton";
import Collapse from "@mui/material/Collapse";
import CardContent from "@mui/material/CardContent";
import ModalImage from "react-modal-image";
import "../../../App/Assets/css/css/artworkDetail.css";

const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? "rotate(0deg)" : "rotate(180deg)",
  marginLeft: "auto",
  transition: theme.transitions.create("transform", {
    duration: theme.transitions.duration.shortest,
  }),
}));

const ImageSec = ({ nftDetail, userData, expanding, setExpanding }) => {
  const { expend1 } = expanding;

  const handleExpandClick3 = () => {
    setExpanding({ ...expanding, expend1: !expend1 });
  };
  const [isReadMore, setIsReadMore] = useState(true);
  const toggleReadMore = () => {
    setIsReadMore(!isReadMore);
  };
  return (
    <div
      className="  sm:col-span-12  lg:col-span-6 n-imageNFTDetails"
      // style={{ marginLeft: " 11rem" }}
    >
      <div class="n-mainDiv">
        <div className="ff">
          <ModalImage
            small={nftDetail?.gatewayLink}
            large={nftDetail?.gatewayLink}
            hideDownload={true}
            hideZoom={true}
            className="modal-image"
            // alt="Hello World!"
          />
        </div>
        {/* <img
          style={{
            height: "535px",
            objectFit: "fill",
            width: "535px",
            borderRadius: "5px",
          }}
          src={nftDetail?.gatewayLink}
        /> */}
      </div>
      <div class="n-nftDetails">
        <h2 className="font-bold text-2xl"> NFT DESCRIPTION</h2>
        <p className="text-break mt-2">
          {nftDetail?.description}

          {/* {nftDetail?.description} */}
          {/* <a> Read More</a> */}
        </p>
      </div>
      <div>
        <Card className="mt-2 border n-descriptionDropDown">
          <CardActions disableSpacing>
            <h2>Artist Details</h2>
            <ExpandMore
              expand={expend1}
              onClick={handleExpandClick3}
              aria-expanded={expend1}
              aria-label="show more"
            >
              <ExpandMoreIcon />
            </ExpandMore>
          </CardActions>
          <Collapse in={expend1} timeout="auto" unmountOnExit>
            <CardContent class="border-t" style={{ padding: "10px" }}>
              <div class="mt-3 row">
                <div class="col">
                  {" "}
                  <div className="rounded-lg cardsincard ">
                    <h3 className="n-lable">Name</h3>
                    <p className="n-value capitalize">{userData?.flname}</p>
                  </div>
                </div>
                {/* <div class="col">
                  {" "}
                  <div className="rounded-lg cardsincard ">
                    <h3 className="n-lable">Username</h3>
                    <p className="n-value">{userData?.username}</p>
                  </div>
                </div> */}
              </div>
              <div class=" mt-3 row">
                <div class="col">
                  {" "}
                  <div className="rounded-lg cardsincard ">
                    <h3 className="n-lable font-semibold">Bio</h3>
                    <p className="n-Bio">
                      {isReadMore
                        ? userData?.bio?.slice(0, 150)
                        : userData?.bio}
                      {userData?.bio?.length > 150 && (
                        <span
                          style={{ color: "blue", cursor: "pointer" }}
                          onClick={toggleReadMore}
                        >
                          {isReadMore ? "...read more" : " ...show less"}
                        </span>
                      )}
                      {/* {userData?.bio} */}
                    </p>
                  </div>
                </div>
              </div>
            </CardContent>
          </Collapse>
        </Card>
      </div>
    </div>
  );
};

export default ImageSec;
