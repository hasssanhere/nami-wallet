import React, { useState, useEffect } from "react";
// import Header from "../../partials/Header";
import {
  useHistory,
  useLocation,
  useParams,
  withRouter,
} from "react-router-dom";
// import Footer from "../../partials/Footer";
import "react-dropdown/style.css";
import MainArtWorkDetailed from "./Content/MainArtWorkDetailed";
import CreateArtistProfile from "../Authentication/SignUp/Index";
import ArtistLogin from "../Authentication/SignIn/Index";

const ArtWorkDetailed = () => {
  const [modalSignUp, setModalSignUp] = useState(false);
  const [modalSignIn, setModalSignIn] = useState(false);
  let location = useLocation();
  const urlSearchParams = new URLSearchParams(window.location.search);
  const params = Object.fromEntries(urlSearchParams.entries());
  const { nftId, userId } = params;

  return (
    <>
      {/* Top area: Blocks */}
      <MainArtWorkDetailed
        nftId={nftId}
        userId={userId}
        setModalSignIn={setModalSignIn}
      />

      <CreateArtistProfile
        show={modalSignUp}
        // onHide={() => setModalSignUp(false)}
        setModalSignIn={setModalSignIn}
        setModalSignUp={setModalSignUp}
        modalSignIn={modalSignIn}
        modalSignUp={modalSignUp}
      />
      <ArtistLogin
        show={modalSignIn}
        onHide={() => setModalSignIn(false)}
        setModalSignIn={setModalSignIn}
        setModalSignUp={setModalSignUp}
      />
    </>
  );
};

export default withRouter(ArtWorkDetailed);
