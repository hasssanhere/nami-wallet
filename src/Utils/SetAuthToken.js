import axios from "axios";

const setAuthToken = (token) => {
  axios.interceptors.request.use(
    function (config) {
      // Do something before request is sent
      if (token && !config.url.includes("/api.php")) {
        config.headers = {
          ...config.headers,
          Authorization: `Bearer ${token}`,
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Methods":
            "GET, HEAD, POST, PUT, DELETE, CONNECT, OPTIONS, TRACE, PATCH",
        };
      } else {
      }
      return config;
    },
    function (error) {
      // Do something with request error
      return Promise.reject(error);
    }
  );
};

export default setAuthToken;
