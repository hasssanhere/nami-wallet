import axios from "axios";
import Swal from "sweetalert2";
import { blockchain } from "../Routes/BlockChainRoute";
import { nftCreating } from "../Routes/serverRoute";
import { creatingNFT } from "./CreateNFT";

const sharedKey = process.env.REACT_APP_SHAREDKEY;

const Toast = Swal.mixin({
  toast: true,
  position: "top-right",
  showConfirmButton: false,
  timer: 2000,
  timerProgressBar: true,
  didOpen: (toast) => {
    toast.addEventListener("mouseenter", Swal.stopTimer);
    toast.addEventListener("mouseleave", Swal.resumeTimer);
  },
});

// getting My NFT's
export const ownedNFT = ({ setdata, count, setLoading }) => {
  setLoading(true);
  var body = new FormData();
  body.append("sharedKey", sharedKey);
  body.append("service", "getNfts");
  body.append("nftprojectid", sessionStorage.getItem("projectId"));
  body.append("state", `free`);
  body.append("count", `12`);
  body.append("page", count);

  axios
    .post(`${blockchain.BLOCKCHAIN_URL}`, body, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    })
    .then(async (res) => {
      console.log(res);
      setdata(res?.data);

      setLoading(false);
    })
    .catch((err) => {
      setLoading(false);
    });
};

// Uploading My NFT

export const CreateNFT = ({ obj, formdata }) => {
  // sending Imgae to IPFS
  console.log(obj, formdata);
  Swal.fire({
    toast: false,
    html: `<div id="preloader">
      <svg width="205" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
       viewBox="-192 356.8 182.9 80.4" style="enable-background:new -192 356.8 182.9 80.4;" xml:space="preserve">
    <path class="path-preloader" d="M-102,398c4-8.3,12.9-19.8,19-24.3c6.1-4.5,11.2-7.5,15.4-9c4.2-1.5,8.7-2.2,13.6-2.3c10.3,0,18.7,3.5,25,10.4
      c6.4,6.8,9.6,15.1,9.6,24.9c0,6.7-1.4,12.8-4.3,18.5c-2.8,5.7-6.9,10-12.1,12.9c-5.2,2.9-11.1,4.4-17.9,4.4c-8.8,0-16.4-1.9-23-5.6
      C-83.1,424.1-96.2,409.8-102,398c-5.1,11.6-19.5,26.5-25.9,30.1c-6.4,3.6-13.9,5.4-22.4,5.4c-10.8,0-19.3-3.4-25.4-10.1
      c-6-6.7-9-15.3-9-25.6c0-9.7,3.2-18,9.5-24.9c6.4-6.9,14.8-10.4,25.1-10.4c5,0,9.6,0.8,13.8,2.3c4.2,1.5,9.2,4.5,15.3,9
      C-114.9,378.2-105.9,390.9-102,398"/>
    </svg>
        </div>
        <p style="color: #ffffff ; margin-top: 10px ; font-size:26px;">YOUR NFT IS BEING CREATED</p>`,
    showConfirmButton: false,
    background: `#00000000`,
    color: `#ffffff`,
    allowOutsideClick: false,
    allowEscapeKey: false,
    allowEnterKey: false,
  });
  axios
    .post(nftCreating.UPLOADING_NFT_IMAGE, obj)
    .then(async (res) => {
      const body = new FormData();
      body.append("sharedKey", sharedKey);
      body.append("service", "UploadNft");
      body.append("nftprojectid", sessionStorage.getItem("projectId"));
      body.append("id", "1");
      body.append("assetname", formdata.title);
      body.append("mimetype", formdata.mimetype);
      body.append("description", formdata.description);
      body.append("creator", sessionStorage.getItem("username"));
      body.append("collection", formdata.collectionName);
      body.append("thumbnailimagelink", res.data.imageUrl);
      body.append("highresimagelink", res.data.imageUrl);

      await axios
        .post(blockchain.BLOCKCHAIN_URL, body, {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        })
        .then(async (res) => {
          const data = new FormData();
          data.append("Category", formdata.category);
          data.append("CollectionName", formdata.collectionName);

          data.append("Description", formdata.description);

          data.append("externalLink", formdata.externalLink);

          data.append("image", formdata.image);

          data.append("imageUpload", formdata.imageUpload);

          data.append("mimetype", formdata.mimetype);

          data.append("newCollection", formdata.newCollection);

          data.append("Royality", formdata.royality);

          data.append("Title", formdata.title);
          data.append("nftId", res.data.nftId);

          res.data.errorMessage
            ? Toast.fire({
                icon: "error",
                title: res.data.errorMessage,
              })
            : creatingNFT(data);
        })
        .catch((err) => {
          Swal.fire({
            text: err?.response?.data?.message,
            icon: "error",
          });
        });
    })
    .catch((err) => {
      Swal.fire({
        text: err?.response?.data?.message,
        icon: "error",
      });
    });
};

export const getnftdetailsbyid = (nftId, projectId, setNftDetailAdv) => {
  const body = new FormData();
  body.append("sharedKey", sharedKey);
  body.append("service", "getNftDetailsById");
  body.append("nftprojectid", projectId);
  body.append("nftid", nftId);

  axios
    .post(`${blockchain.BLOCKCHAIN_URL}`, body, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    })
    .then(async (res) => {
      console.log(res);
      setNftDetailAdv(res.data);
    })
    .catch((err) => {});
};

export const deleteFromNftmaker = (item) => {
  const body = new FormData();
  body.append("sharedKey", sharedKey);
  body.append("service", "DeleteNft");
  body.append("nftprojectid", sessionStorage.getItem("projectId"));
  body.append("nftid", item.nftId);

  axios
    .post(`${blockchain.DELETE_NFT}`, body, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    })
    .then(async (res) => {
      console.log(res);
    })
    .catch((err) => {});
};

// //Creating Project
export const mintNFt = ({ nftprojectid, nftid, receiveraddress }) => {
  var body = new FormData();
  body.append("sharedKey", sharedKey);
  body.append("service", "MintAndSendSpecific");
  body.append("nftprojectid", nftprojectid);
  body.append("nftid", nftid);
  body.append("countnft", "1");
  body.append(
    "receiveraddress",
    "addr_test1qpmpz0avxe5xhryw8d27z7tcykefklj5eavdgpuwhujp0f3w28yduqanmmu6tydf5uph0h6qn7jzekw9hu2mv2ye7n5qq89phk"
  );

  axios
    .post(blockchain.BLOCKCHAIN_URL, body, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    })
    .then(function (res) {
      console.log(res);
      Toast.fire({
        icon: "success",
        text: "Mint Successful",
      });
    })
    .catch((err) => {
      Toast.fire({
        icon: "error",
        text: err?.responce?.data?.errorMessage,
      });
    });
};
