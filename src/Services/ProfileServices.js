import React from "react";
import axios from "axios";
import FormData from "form-data";
import { profile } from "../Routes/serverRoute";
import Swal from "sweetalert2";

const Toast = Swal.mixin({
  toast: true,
  position: "top-end",
  showConfirmButton: false,
  timer: 1000,
  timerProgressBar: true,
  didOpen: (toast) => {
    toast.addEventListener("mouseenter", Swal.stopTimer);
    toast.addEventListener("mouseleave", Swal.resumeTimer);
  },
});

//GET my Owned NFTS

export const ownedNFT = ({ setdata, count, setLoading }) => {
  setLoading(true);
  axios
    .get(`${profile.GET_OWNED_NFT}${count}`)
    .then(async (res) => {
      setdata(res?.data?.paginated);
      setLoading(false);
    })
    .catch((err) => {
      setLoading(false);
    });
};

//listing An NFT
export const listaNft = ({ obj, refresh, setRefresh }) => {
  const { nftid, price } = obj;
  const data = {
    price,
  };
  axios
    .post(`${profile.LIST_A_NFT}/${nftid}`, data)
    .then(async (res) => {
      Toast.fire({
        icon: "success",
        text: "Listed in the marketplace Successfully",
      });
      setRefresh(!refresh);
    })
    .catch((err) => {
      Toast.fire({
        icon: "error",
        text: "Listed in the marketplace Successfully",
      });
    });
};

//GEt Listed NFTS
export const getListedNft = ({ setdataaLis, setLoading, count }) => {
  setLoading(true);
  axios
    .get(`${profile.GET_LISTED_NFT}${count}`)
    .then(async (res) => {
      setdataaLis(res?.data?.paginated);
      setLoading(false);
    })
    .catch((err) => {
      setLoading(false);
    });
};

//Making An NFt Featured
export const featurednft = ({ Id, body }) => {
  axios
    .post(`${profile.MAKE_FEATURED_NFT}${Id}`, body)
    .then(async (res) => {
      Toast.fire({
        icon: "success",
        text: res?.data.message,
      });
    })
    .catch((err) => {
      Toast.fire({
        icon: "error",
        text: err?.error.message,
      });
    });
};

//REMOVING AN NFT FROM LISTING
export const removenft = (nftid, refresh, setRefresh) => {
  axios
    .put(`${profile.REMOVE_FROM_LISTING}${nftid}`)
    .then(async (res) => {
      Toast.fire({
        icon: "success",
        text: res?.data.message,
      });
      setRefresh(!refresh);
    })
    .catch((err) => {
      Toast.fire({
        icon: "error",
        // text: err?.error.message,
      });
    });
};

// ..Adding a wallet

export const addWallet = (body, setrefresh, refresh) => {
  axios
    .post(`${profile.ADD_A_WALLET}`, body)
    .then(async (res) => {
      setrefresh(!refresh);
      await Toast.fire({
        icon: "info",
        text: res?.data.message,
      });
      // sessionStorage.setItem("walletaddress", res?.data.result);
      if (res?.data.address.length == 1) {
        makewalletPrimary(
          {
            primaryAddress: res?.data.result,
          },
          refresh,
          setrefresh
        );
      }
    })
    .catch((err) => {
      Toast.fire({
        icon: "error",
      });
    });
};

// ..Removing a wallet

export const removeWallet = (body, refresh, setrefresh) => {
  axios
    .put(`${profile.REMOVE_A_WALLET}`, body)
    .then(async (res) => {
      // await Toast.fire({
      //   icon: "success",
      //   text: res?.data.message,
      // });
      setrefresh(!refresh);
      if (res?.data?.result == sessionStorage.getItem("walletaddress")) {
        sessionStorage.removeItem("walletaddress", res?.data.result);
      }
      setrefresh(!refresh);
    })
    .catch((err) => {
      Toast.fire({
        icon: "error",
        // text: err?.error.message,
      });
    });
};

//GET featured Prices
export const getFeaturedPrices = ({ setPriceData }) => {
  axios
    .get(profile.GET_FEATURED_PRICE)
    .then(async (res) => {
      setPriceData(res.data.getItem);
    })
    .catch((err) => {
      Swal.fire({
        text: err?.response?.data?.message,
        icon: "error",
      });
    });
};
export const editPriceOfListedNfts = ({ obj, refresh, setRefresh }) => {
  const { nftid, price } = obj;

  const body = { editprice: price };
  axios
    .put(`${profile.EDIT_PRICE_OF_LISTED_NFTS}/${nftid}`, body)
    .then(async (res) => {
      Toast.fire({
        icon: "success",
        text: res?.data?.message,
      });
      setRefresh(!refresh);
    })
    .catch((err) => {
      Toast.fire({
        icon: "error",
        text: err?.data?.message,
      });
    });
};

export const listProjects = () => {
  var data = new FormData();
  data.append("sharedKey", "GAEZJUKFJ9MB5ZI2M4KW");
  data.append("service", "listProjects");
  data.append("count", "10");
  data.append("page", "1");

  axios
    .post(profile.GET_LIST_PROJECTS, data, {
      headers: {
        "Postman-Token": "<calculated when request is sent>",
        "Content-Type":
          "multipart/form-data; boundary=<calculated when request is sent>",
        "Content-Length": "<calculated when request is sent>",
        Host: "<calculated when request is sent>",
        "User-Agent": "PostmanRuntime/7.28.4",
        Accept: "*/*",
        "Accept-Encoding": "gzip, deflate, br",
        Connection: "keep-alive",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
    })
    .then((res) => {
      console.log("3rd party api", res.data);
    });
};
// export const listProjects = () => {
//   var FormData = require("form-data");
//   var data = new FormData();
//   data.append("sharedKey", "GAEZJUKFJ9MB5ZI2M4KW");
//   data.append("service", "listProjects");
//   data.append("count", "10");
//   data.append("page", "1");

//   var config = {
//     method: "post",
//     url: "http://165.227.195.188/api.php",
//     headers: {
//       "Postman-Token": "<calculated when request is sent>",
//     },
//     data: data,
//   };

//   axios(config)
//     .then(function (response) {
//       console.log(JSON.stringify(response.data));
//     })
//     .catch(function (error) {
//       console.log(error);
//     });
// };

// making Wallet Primary

export const makewalletPrimary = (obj, refresh, setRefresh) => {
  const body = { primaryAddress: obj.primaryAddress };

  axios
    .post(`${profile.MAKE_WALLETPRIMARY}`, body)
    .then(async (res) => {
      Toast.fire({
        icon: "success",
        text: res?.data?.message,
      });
      sessionStorage.setItem("walletAddress", res.data.result);
      setRefresh(!refresh);
    })
    .catch((err) => {
      Toast.fire({
        icon: "error",
        text: err?.data?.message,
      });
    });
};
