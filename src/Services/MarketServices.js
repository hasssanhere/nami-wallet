import axios from "axios";
import Swal from "sweetalert2";
import { market } from "../Routes/serverRoute";

const Toast = Swal.mixin({
  toast: true,
  position: "top-end",
  showConfirmButton: false,
  timer: 5000,
  timerProgressBar: true,
  didOpen: (toast) => {
    toast.addEventListener("mouseenter", Swal.stopTimer);
    toast.addEventListener("mouseleave", Swal.resumeTimer);
  },
});

//All NFT IN THE MARKET PLACE
export const getAllListedNFTs = ({
  setNFTData,
  count,

  setCountButton,
  setLoading,
}) => {
  // setLoading(true);

  const { countMarket } = count;
  axios
    .get(`${market.ALL_NFT_FOR_MARKET}${countMarket}`)
    .then(async (res) => {
      setNFTData(res?.data?.paginated);
      setCountButton({
        buttonMarket: true,
        buttonGeneral: false,
        buttonFilters: false,
      });
      setLoading(false);
    })
    .catch((err) => {
      setLoading(false);
    });
};

//getting all collections
export const getallCollections = (setAllCollection) => {
  axios
    .get(`${market.GET_ALL_COLLECTIONS}`)
    .then(async (res) => {
      setAllCollection(res?.data?.collections);
    })
    .catch((err) => {});
};

// ADDING VIEW COUNTS
export const addViewCount = ({ nftId, userId }) => {
  if (sessionStorage.getItem("id") == userId) {
  } else {
    axios
      .put(`${market.ADD_VIEW_COUNTS}${nftId}`)
      .then(async (res) => {})
      .catch((err) => {});
  }
};

//GENERAL SEARCHING API INTEGRATION
export const generalSearchFilter = ({
  generalSearch,
  setNFTData,
  count,
  setCountButton,
}) => {
  const { countGeneral } = count;
  axios
    .get(`${market.GENERAL_SEARCHING}${countGeneral}&value=${generalSearch}`)
    .then(async (res) => {
      setNFTData(res?.data?.paginated);
      setCountButton({
        buttonMarket: false,
        buttonGeneral: true,
        buttonFilters: false,
      });
    })
    .catch((err) => {});
};

// FILTER_SEARCHING API INTEGRATOION
export const multiSearchFilter = ({
  filterData,
  setFilterData,
  setNFTData,
  count,

  setCountButton,
  setSuccess,
}) => {
  const { countFilters } = count;
  const { minPrice } = filterData;

  console.log("price data", filterData.minPrice);
  axios
    .post(`${market.FILTER_SEARCHING}${countFilters}`, filterData)
    .then(async (res) => {
      setNFTData(res?.data?.paginated);
      setCountButton({
        buttonMarket: false,
        buttonGeneral: false,
        buttonFilters: true,
      });
      setSuccess(res?.data?.success);
    })
    .catch((err) => {});
};
export const getUsdPrice = (setUsd) => {
  fetch(
    `https://min-api.cryptocompare.com/data/pricemulti?fsyms=ADA&tsyms=USD&api_key=a79fe5a3ddddb156648aee973f931fcfba74998da5446c6c64922dca0326bbc7`
  )
    .then((response) => response.json())
    .then((res) => {
      console.log(res);
      setUsd(res.ADA.USD);
    });
};
