import React from "react";
import axios from "axios";
import { NFT, Artist, nftCreating } from "../Routes/serverRoute";
import Swal from "sweetalert2";
import { deleteFromNftmaker } from "./blockchainServices";

const Toast = Swal.mixin({
  toast: true,
  position: "top-end",
  showConfirmButton: false,
  timer: 2000,
  timerProgressBar: true,
  didOpen: (toast) => {
    toast.addEventListener("mouseenter", Swal.stopTimer);
    toast.addEventListener("mouseleave", Swal.resumeTimer);
  },
});

// export const CreateNFT = ({ data, formdata }) => {
//   // sending Imgae to IPFS
//   axios
//     .post(NFT.UPLOAD_IMAGE, data)
//     .then(async (res) => {
//       var allData = {
//         assetName: formdata.assetName,
//         previewImageNft: {
//           fileFromIPFS: res?.data,
//           description: formdata.description,
//           displayname: "string",
//         },
//         projectId: formdata.projectId,
//       };

//       await axios
//         .post(NFT.UPLOAD_NFT, allData)
//         .then(async (res) => {
//           Toast.fire({
//             icon: "success",
//             title: "NFT Created SuccessFully",
//           });
//         })
//         .catch((err) => {
//           Swal.fire({
//             text: err?.response?.data?.message,
//             icon: "error",
//           });
//         });
//     })
//     .catch((err) => {
//       Swal.fire({
//         text: err?.response?.data?.message,
//         icon: "error",
//       });
//     });
// };
// // GET NFT ALL FOR MARKETPLACE
// export const getAllNftsByState = (setNFTData) => {
//   axios
//     .get(NFT.ALL_NFT_FOR_MARKET)
//     .then(async (res) => {
//       setNFTData(res.data);
//     })
//     .catch((err) => {});
// };

// //GET NFT DETAILS
// export const getNftById = ({ obj, setTokenURL, setTokenId }) => {
//   const { nftId, projectId } = obj.obj;
//   // const { setTokenURL, setTokenId } = obj;
//   axios.get(NFT.GET_NFT_BY_ID + `${projectId}/${nftId}`).then((res) => {
//     const data1 = "https://gateway.pinata.cloud/ipfs/";
//     setTokenURL(data1 + res.data.ipfshash);
//     setTokenId(res.data);
//   });
// };

// // Get Checker Address
// const getAddressChecker = async ({
//   payAddresses,
//   obj,
//   setShoww,
//   setOwnerData,
// }) => {
//   console.log(obj);

//   const { nftId, projectId } = obj.obj;

//   const data = await axios.get(
//     NFT.GET_ADDRESS_CHECKER + `${projectId}/${payAddresses}`
//   );
//   // setBuyButtonClick(data.data);
//   console.log("address checker will return==>", data.data);
//   if (data?.data?.state == "paid") {
//     setShoww(true);
//   }
// };

// //get sender Address
// export const getSenderAddress = async ({
//   payAddresses,
//   obj,
//   setShoww,
//   setOwnerData,
// }) => {
//   // console.log("aaa", afterbuy.data);

//   console.log("buy", payAddresses);
//   getAddressChecker({ payAddresses, obj });
//   setInterval(
//     () => getAddressChecker({ payAddresses, obj, setShoww, setOwnerData }),
//     10000
//   );
//   // if (buyButtonClick?.state == "paid") {
//   //   clearInterval(interval);
//   // }
// };

// //get Fetured NFT

// export const getFeaturedNFT = (setFeatured) => {
//   axios
//     .get(NFT.GET_FEATURED_NFT)
//     .then(async (res) => {
//       setFeatured(res.data);
//     })
//     .catch((err) => {});
// };

// // GET LISTING NFT ALL
// // export const getAlllisting = (setdataaLis, setCreation) => {
// //   // console.log("===================>", NFT.ALL_NFT_FOR_MARKET);
// //   axios
// //     .get(NFT.ALL_NFT_FOR_MARKET)
// //     .then(async (res) => {
// //       setdataaLis(res.data);
// //       const inventory = res.data;
// //       setCreation(inventory.length);
// //       console.log("Inventory", res.data);
// //       // console.log("................this is my response", res.data);
// //     })
// //     .catch((err) => {});
// // };

// // storing price by id
// export const storeNftIdPrice = (nftid, price) => {
//   axios
//     .post(`${NFT.STORE_PRICE_BY_ID}${nftid}/${price}`)
//     .then(async (res) => {
//       console.log(res.data);
//     })
//     .catch((err) => {
//       Swal.fire({
//         icon: "error",
//         text: "Server Error",
//       });
//     });
// };

// //sending nft
// // export const sendNFT = (obj, setdataa, refresh, setRefresh) => {
// //   const PROJECT_ID = sessionStorage.getItem("projectId");
// //   axios
// //     .get(`${NFT.GET_NFT_ID_DASH}${PROJECT_ID}/${obj.nftid}/1/${obj.text}`)
// //     .then(async (res) => {
// //       console.log(res.data.adaToSend);
// //       storeNftIdPrice(obj.nftid, res.data.adaToSend);
// //       setdataa(res.data);
// //       await sessionStorage.setItem("dataa", res.data.paymentAddress);
// //       await Swal.fire({
// //         icon: "success",
// //         text: "Successful",
// //       });
// //       setRefresh(!refresh);
// //     })
// //     .catch((err) => {
// //       Swal.fire({
// //         icon: "error",
// //         text: "Server Error",
// //       });
// //     });
// // };

// //GET_PRICE_ALL_IDS
// // export const getallprices = (setPricesId) => {
// //   axios
// //     .get(`${NFT.GET_PRICE_ALL_IDS}`)
// //     .then(async (res) => {
// //       console.log(res.data);
// //       setPricesId(res?.data);
// //     })
// //     .catch((err) => {
// //       Swal.fire({
// //         icon: "error",
// //         text: "Server Error",
// //       });
// //     });
// // };

// ===========================NEW WORK================================

// geting NFTCOLLECTION LIST
export const getCollections = (setCollections) => {
  axios
    .get(`${nftCreating.GET_COLLECTION}`)
    .then(async (res) => {
      setCollections(res?.data?.result);
    })
    .catch((err) => {
      Swal.fire({
        icon: "error",
        text: "Server Error",
      });
    });
};

// CREATE A NEW Collection

export const createNewCollection = ({ body, refresh, setRefresh }) => {
  axios
    .post(`${nftCreating.CREATE_A_COLLECTION}`, body)
    .then(async (res) => {
      if (res?.data?.success) {
        Toast.fire({
          icon: "success",
          text: "Collection Created Successfully",
        });
        setRefresh(!refresh);
      }
    })
    .catch((err) => {
      Swal.fire({
        icon: "error",
        text: "Server Error",
      });
    });
};

//Creating NFT

export const creatingNFT = (data) => {
  axios
    .post(`${nftCreating.CREATEING_NFT}`, data)
    .then(async (res) => {
      // setCollections(res?.data?.result);
      if (res.data.message) {
        Toast.fire({
          icon: "success",
          text: res.data.message,
        });
      } else {
        Toast.fire({
          icon: "error",
          text: res.data[0],
        });
      }
    })
    .catch((err) => {
      Swal.fire({
        icon: "error",
        text: "Server Error",
      });
    });
};

export const deleteNFT = (item, refresh, setRefresh) => {
  axios
    .delete(`${nftCreating.DELETE_NFT}${item._id}`)
    .then(async (res) => {
      deleteFromNftmaker(item);
      setRefresh(!refresh);
    })
    .catch((err) => {
      Swal.fire({
        icon: "error",
        text: "Server Error",
      });
    });
};
