import axios from "axios";
import { Artist, NFT } from "../Routes/serverRoute";
import Swal from "sweetalert2";
import { getnftdetailsbyid } from "./blockchainServices";

const Toast = Swal.mixin({
  toast: true,
  position: "top-right",
  showConfirmButton: false,
  timer: 2000,
  timerProgressBar: true,
  didOpen: (toast) => {
    toast.addEventListener("mouseenter", Swal.stopTimer);
    toast.addEventListener("mouseleave", Swal.resumeTimer);
  },
});
export const Signup = (formdata, setModalSignUp) => {
  axios
    .post(Artist.SIGN_UP, formdata)
    .then(async (res) => {
      if (res?.data[0]) {
        await Toast.fire({
          text: res?.data[0],
        });
      } else {
        await Toast.fire({
          text: res?.data?.message,
        });
        setModalSignUp(false);
      }
    })
    .catch((err) => {
      if (err?.response?.data?.error) {
        Swal.fire({
          text: err?.response?.data?.error?.message,
          icon: "error",
        });
      } else {
        Swal.fire({
          text: err?.response?.data?.message,
          icon: "error",
        });
      }
    });
};
export const Signin = (formdata, setModalSignIn) => {
  axios
    .post(Artist.SIGN_IN, formdata)
    .then(async (res) => {
      if (res?.data?._id == undefined || res?.data?.role == undefined) {
        Toast.fire({
          icon: "info",
          text: res?.data?.message,
        });
      } else {
        if (res?.data?.role == "admin") {
          Toast.fire({
            icon: "info",
            text: "User Does not exist",
          });
        } else {
          // Toast.fire({
          //   icon: "info",
          //   text: res?.data?.message,
          // });
          sessionStorage.setItem("id", res?.data?._id);
          sessionStorage.setItem("role", res?.data?.role);
          sessionStorage.setItem("username", res?.data?.username);
          sessionStorage.setItem("projectId", res?.data?.projectId);
          sessionStorage.setItem("token", res?.data?.token);
          sessionStorage.setItem("image", res?.data?.image);
          sessionStorage.setItem("flname", res?.data?.flname);
          sessionStorage.setItem("email", res?.data?.email);
          sessionStorage.setItem("walletAddress", res?.data?.primaryAddress);

          setModalSignIn(false);
        }
      }
    })
    .catch((err) => {
      Swal.fire({
        text: err?.response?.data?.message,
        icon: "error",
      });
    });
};
//GETTING THE ORIGINAL WALLET ADDRESS
export const OWalletAddress = ({ body, setRefresh, refresh }) => {
  // const body = { address: HEXwalletaddress };
  axios
    .post(Artist.GET_WALLET_ADDRESS, body)
    .then(async (res) => {
      // sessionStorage.setItem("walletaddress", walletaddress[0]);
      sessionStorage.setItem("walletAddress", res?.data?.address);
      // await Toast.fire({
      //   icon: "success",
      //   text: res?.data?.error || "Wallet Connected",
      // });
      setRefresh(!refresh);

      // history.push("/signinn");
    })
    .catch((err) => {
      Swal.fire({
        text: err?.response?.data?.message,
        icon: "error",
      });
    });
};

// get User Details
export const getUserDetailsbyID = (id, setUserDataa) => {
  // const id = sessionStorage.getItem("id");
  axios
    .get(`${Artist.GET_USER_DETAILS}/${id}`)
    .then(async (res) => {
      setUserDataa(res?.data?.user);
      sessionStorage.setItem("projectId", res?.data?.user?.projectId);
      sessionStorage.setItem("walletAddress", res?.data?.primaryAddress);
    })
    .catch((err) => {});
};

// Upload Profile Image
// /users/profile/picture

export const UploadPimage = (data, setRefresh, refresh) => {
  axios
    .post(`${Artist.UPLOAD_IMAGE}`, data)
    .then(async (res) => {
      await setRefresh(!refresh);
      Toast.fire({
        text: res?.data.message,
      });
    })
    .catch((err) => {
      Toast.fire({
        text: err?.responce?.data?.message,
        icon: "error",
      });
    });
};
export const followArtist = (id) => {
  // const id = "61c5b598410362a2538ff1fe";

  axios
    .put(`${Artist.FOLLOW_ARTIST}${id}`)
    .then(async (res) => {})
    .catch((err) => {});
};
export const getArtistDetailbyId = (id, setArtistData) => {
  // const id = sessionStorage.getItem("id");
  axios
    .get(`${Artist.GET_USER_DETAILS}/${id}`)
    .then(async (res) => {
      setArtistData(res?.data?.user);
    })
    .catch((err) => {});
};
export const unFollowArtist = (id) => {
  axios
    .put(`${Artist.UNFOLLOW_ARTIST}${id}`)
    .then(async (res) => {})
    .catch((err) => {});
};
export const getFollowers = ({ setDataa, count, setLoading }) => {
  setLoading(true);
  axios.get(`${Artist.GET_FOLLOWERS}${count}`).then(async (res) => {
    setDataa(res?.data?.paginated);
    setLoading(false);
  });
};
export const getFollowings = ({ setDataa, count, setLoading }) => {
  setLoading(true);
  axios.get(`${Artist.GET_FOLLOWINGS}${count}`).then(async (res) => {
    setDataa(res.data.paginated);
    setLoading(false);
  });
};
export const getTopArtists = (setData) => {
  axios.get(`${Artist.GET_TOP_ARTISTS}`).then(async (res) => {
    setData(res?.data?.topArtists);
  });
};
export const getArtistNftById = ({
  id,
  setNftData,
  setNftMsg,
  setLoading,
  count,
}) => {
  // setLoading(false);
  axios
    .get(`${Artist.GET_ARTISTID_BY_ID}/${id}?page=${count}`)
    .then(async (res) => {
      if (res?.data?.paginated) {
        setNftData(res?.data?.paginated);
        setLoading(false);
      } else {
        setNftMsg(res?.data?.message);
        setLoading(false);
      }
    })
    .catch((err) => {
      setLoading(false);
    });
};
export const getFollowUnfollowStatus = (id, setButtonState) => {
  // const id = "61c5b598410362a2538ff1fe";

  axios
    .get(`${Artist.GET_FOLLOWUNFOLLOW_STATUS}/${id}`)
    .then(async (res) => {
      setButtonState(res.data.success);
    })
    .catch((err) => {});
};
export const getAllArtists = (setAllArtists) => {
  axios.get(`${Artist.GET_ALL_ARTISTS}`).then(async (res) => {
    setAllArtists(res.data.getArtists);
  });
};
export const getSelectedArtistNfts = (id, setFilterArtist) => {
  const body = { ids: [id] };
  axios.post(`${Artist.GET_SELECTED_ARTIST_NFTS}`, body).then((res) => {
    setFilterArtist(res.data.Nfts);
  });
};
export const getParticularNftDetail = (id, setNftDetail, setNftDetailAdv) => {
  axios.get(`${NFT.GET_PARTICULAR_NFT_DETAIL}/${id}`).then((res) => {
    setNftDetail(res?.data?.is_available);
    getnftdetailsbyid(
      res?.data?.is_available?.nftId,
      res?.data?.is_available?.artistId.projectId,
      setNftDetailAdv
    );
  });
};

export const editBio = (bio) => {
  const body = { bio: bio };
  axios
    .put(Artist.EDIT_BIO, body)
    .then((res) => {
      Toast.fire({
        icon: "info",
        text: res?.data?.message,
      });
    })
    .catch((err) => {
      Toast.fire({
        icon: "error",
        text: err?.data.message,
      });
    });
};
export const editPassword = (oldPass, newPass, confirmPass) => {
  const body = {
    oldPassword: oldPass,
    newPassword: newPass,
    confirmPassword: confirmPass,
  };
  axios
    .put(Artist.EDIT_PASSWORD, body)
    .then((res) => {
      Toast.fire({
        icon: "info",
        text: res?.data?.message,
      });
    })
    .catch((err) => {
      Toast.fire({
        icon: "error",
        text: err?.data.message,
      });
    });
};
