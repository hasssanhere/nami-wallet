import React from "react";
import axios from "axios";

import Swal from "sweetalert2";
import { Home, profile } from "../Routes/serverRoute";

const Toast = Swal.mixin({
  toast: true,
  position: "top-end",
  showConfirmButton: false,
  timer: 5000,
  timerProgressBar: true,
  didOpen: (toast) => {
    toast.addEventListener("mouseenter", Swal.stopTimer);
    toast.addEventListener("mouseleave", Swal.resumeTimer);
  },
});

//GET my Owned NFTS

export const getfeaturedNft = (setdata) => {
  axios
    .get(Home.GET_FEATURED_NFT)
    .then(async (res) => {
      if (res?.data?.paginated) {
        setdata(res?.data?.paginated);
      } else {
        setdata([]);
      }
    })
    .catch((err) => {
      console.log(err);
    });
};

//Connecting a Wallet

export const connectHWallet = (body, refresh, setRefresh) => {
  axios
    .post(`${profile.ADD_A_WALLET}`, body)
    .then(async (res) => {
      if (res?.data?.success) {
        Toast.fire({
          icon: "success",
          text: res?.data.message,
        });
        sessionStorage.setItem("walletaddress", res?.data.result);
      } else {
        Toast.fire({
          icon: "success",
          text: "Wallet Connected",
        });
        sessionStorage.setItem("walletAddress", res?.data.result);
      }

      setRefresh(!refresh);
    })
    .catch((err) => {});
};
