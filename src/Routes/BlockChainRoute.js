const BLOCKCHAIN_URL = process.env.REACT_APP_BLOCKCHAIN_URL;
const SHARED_KEY_URL = process.env.REACT_APP_SHAREDKEY;
const SERVER_URL = process.env.REACT_APP_SERVER_URL;

//Block-chain Routes
export const blockchain = {
  BLOCKCHAIN_URL: `${BLOCKCHAIN_URL}`,
};
