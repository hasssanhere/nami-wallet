const BlOCKCHAIN_SERVER_URL = "https://api-testnet.nft-maker.io/";
const API_KEY = "16c7cc3e391542428c99cbd1ab8819f1";
const PROJECT_ID = 5176;
export const SERVER_URL = process.env.REACT_APP_SERVER_URL;
export const BLOCKCHAIN_URL = "http://165.227.195.188/api.php";
// const SERVER_URL = "http://6683-101-53-242-135.ngrok.io/";

export const NFT = {
  ALL_SINGLE_PROJECT_NFT_FOR_MARKET: `${BlOCKCHAIN_SERVER_URL}GetNfts/${API_KEY}/${PROJECT_ID}/reserved`,
  SINGLE_PROJECT_NFT_FOR_MARKET: `${BlOCKCHAIN_SERVER_URL}GetNfts/${API_KEY}`,
  //---------------------------------------------------------------------------------

  //------------------------------------------------------------------------------
  GET_PRICE_BY_ID: `${SERVER_URL}users/getnftidandprice/`,
  GET_NFT_BY_ID: `${BlOCKCHAIN_SERVER_URL}GetNftDetailsById/${API_KEY}/`,
  UPLOAD_NFT: `${SERVER_URL}users/upload/nft/info`,
  POST_PRICE_API: `${SERVER_URL}users/postnftidandprice/`,
  LIST_PROJECT: `${BlOCKCHAIN_SERVER_URL}ListProjects/${API_KEY}`,
  //wploading images
  UPLOAD_IMAGE: `${SERVER_URL}users/upload/nft`,
  BUY_BUTTON_API: `${BlOCKCHAIN_SERVER_URL}users/mintAndSend/`,
  GET_SENDER_ADDRESS: `${SERVER_URL}users/wallet/get/address/`,
  GET_ADDRESS_CHECKER: `${SERVER_URL}users/wallet/checkaddress/state/`,
  GET_FEATURED_NFT: `${SERVER_URL}users/featured`,
  GET_NFT_ID_DASH: `${BlOCKCHAIN_SERVER_URL}GetAddressForSpecificNftSale/${API_KEY}/`,
  STORE_PRICE_BY_ID: `${SERVER_URL}users/postnftidandprice/`,
  GET_PRICE_ALL_IDS: `${SERVER_URL}users/getnftidandprice`,
  GET_PARTICULAR_NFT_DETAIL: `${SERVER_URL}nft/detail`,
  // GET_USD_PRICE: `{}`
};

// Artist SignIn
export const Artist = {
  SIGN_UP: `${SERVER_URL}signup`,
  SIGN_IN: `${SERVER_URL}signin`,
  GET_WALLET_ADDRESS: `${SERVER_URL}wallet/getaddress`,
  GET_USER_DETAILS: `${SERVER_URL}users/profile`,
  UPLOAD_IMAGE: `${SERVER_URL}users/upload/profile/picture`,
  FOLLOW_ARTIST: `${SERVER_URL}users/follow/`,
  UNFOLLOW_ARTIST: `${SERVER_URL}users/unfollow/`,
  GET_FOLLOWERS: `${SERVER_URL}users/followers?page=`,
  GET_FOLLOWINGS: `${SERVER_URL}users/following?page=`,
  GET_TOP_ARTISTS: `${SERVER_URL}users/top/artists`,
  GET_ARTISTID_BY_ID: `${SERVER_URL}users/listednfts`,
  GET_FOLLOWUNFOLLOW_STATUS: `${SERVER_URL}users/followstatus`,
  GET_ALL_ARTISTS: `${SERVER_URL}users/getartists`,
  GET_SELECTED_ARTIST_NFTS: `${SERVER_URL}users/getnfts/multiple`,
  EDIT_BIO: `${SERVER_URL}users/edit/profile`,
  EDIT_PASSWORD: `${SERVER_URL}changepassword`,
};

// Artist SignIn
export const buyer = {
  SIGN_UP: `${SERVER_URL}signup`,
  SIGN_IN: `${SERVER_URL}signin`,
};

// Profile Routes
export const profile = {
  GET_OWNED_NFT: `${SERVER_URL}nft/myowned?page=`,
  LIST_A_NFT: `${SERVER_URL}listing/`, //:Nftid
  GET_LISTED_NFT: `${SERVER_URL}listing/getlisted?page=`,
  GET_FEATURED_PRICE: `${SERVER_URL}featuredprices/view`,
  MAKE_FEATURED_NFT: `${SERVER_URL}featured/nfts/`, //:Nftid
  REMOVE_FROM_LISTING: `${SERVER_URL}listing/remove/`, //Nftid
  ADD_A_WALLET: `${SERVER_URL}wallet/add`,
  REMOVE_A_WALLET: `${SERVER_URL}wallet/remove`,
  EDIT_PRICE_OF_LISTED_NFTS: `${SERVER_URL}listing/editprice`,
  GET_LIST_PROJECTS: `${BLOCKCHAIN_URL}`,
  MAKE_WALLETPRIMARY: `${SERVER_URL}wallet/primary`,
};

//Creating NFT

export const nftCreating = {
  GET_COLLECTION: `${SERVER_URL}collections/view`,
  CREATE_A_COLLECTION: `${SERVER_URL}collections/create`,
  CREATEING_NFT: `${SERVER_URL}nft/create`, //nft/create
  DELETE_NFT: `${SERVER_URL}nft/delete/`, //nftid
  UPLOADING_NFT_IMAGE: `${SERVER_URL}nft/getlink`,
};

export const market = {
  ALL_NFT_FOR_MARKET: `${SERVER_URL}listing/marketplace?page=`,
  NFT_VIEW_COUNTS: `${SERVER_URL}listing/viewcount/`, //id
  ADD_VIEW_COUNTS: `${SERVER_URL}listing/addcount/`, //id
  GET_ALL_COLLECTIONS: `${SERVER_URL}collections/allcollections`,
  ARTIST_FILTER: `${SERVER_URL}users/getnfts/multiple`,
  GENERAL_SEARCHING: `${SERVER_URL}nft/searchnft?page=`, //page no  and Value
  FILTER_SEARCHING: `${SERVER_URL}listing/filter/marketplace?page=`, //pageno and Body
};

//Home
export const Home = {
  GET_FEATURED_NFT: `${SERVER_URL}featured/getfeaturednft/homepage`,
};
