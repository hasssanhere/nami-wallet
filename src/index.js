import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
import AppIndex from "./App/Index";
import * as serviceWorker from "./serviceWorker";
import "bootstrap/dist/css/bootstrap.css";
import "./App/Assets/css/index.scss";
import dotenv from "dotenv";
import setAuthToken from "./Utils/SetAuthToken";

if (sessionStorage.getItem("token"))
  setAuthToken(sessionStorage.getItem("token"));

const App = () => {
  dotenv.config();

  return (
    <>
      <Router>
        <AppIndex />
      </Router>
    </>
  );
};

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("app")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
