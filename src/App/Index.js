import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import "./Assets/css/css/style.scss";
import "./Assets/css/css/App.css";
import Header from "../App/Layout/Header";
import Footer from "../App/Layout/Footer";
import Home from "../Components/Home/Index";
// import Discover from "./pages/Discover";
import Market from "../Components/Market/Index";
// import SignIn from "./pages/SignIn";
// import Collection from "./pages/Collection";
import Dashboard from "../Components/Activity/Index";
import ArtWorkDetailed from "../Components/ArtWorkDetails/Index";

import UserDetail from "../Components/Profile/Index";
// import Profile from "./pages/Profile";
import SignUpArtist from "../Components/CreateNft/Index";
import NFTGuide from "../Components/NFTGuide/Index";
// import ArtistLogin from "./pages/ArtistLogin";
import ArtistLogin from "../Components/Authentication/SignIn/Index";

import BuyerLogin from "../Components/Authentication/SignIn/Index";
import CreatebuyerProfile from "../Components/Authentication/SignUp/Index";
// import CreateArtistProfile from "./pages/CreateArtistProfile";
import CreateArtistProfile from "../Components/Authentication/SignUp/Index";
import ArtistProfile from "../Components/Artistprofile/ArtistProfile";

import FAQ from "../Components/FooterComponents/FAQ";
import Features from "../Components/FooterComponents/Features";

import OurTeam from "../Components/FooterComponents/OurTeam";

import PartnerWithUs from "../Components/FooterComponents/PartnerWithUs";

import PrivacyPolicy from "../Components/FooterComponents/PrivacyPolicy";

import Product from "../Components/FooterComponents/Product";

import Resource from "../Components/FooterComponents/Resource";

import TermsConditions from "../Components/FooterComponents/TermsConditions";
import { BrowserRouter } from "react-router-dom";

function App() {
  return (
    <>
      <div className="flex flex-col min-h-screen overflow-hidden">
        <Header />

        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={Home} />

            <Route exact path="/market" component={Market} />

            <Route exact path="/nftguide">
              <NFTGuide />
            </Route>
            <Route exact path="/createNFT" component={SignUpArtist} />

            <Route exact path="/user-detail/:id" component={UserDetail} />

            <Route exact path="/artist-detail/:id" component={ArtistProfile} />

            <Route
              exact
              path="/artworkdetailed/:id"
              component={ArtWorkDetailed}
            />

            {/* FOOTER ROUTES */}
            <Route exact path="/faq" component={FAQ} />
            <Route exact path="/features" component={Features} />
            <Route exact path="/ourteam" component={OurTeam} />
            <Route exact path="/resource" component={Resource} />
            <Route exact path="/terms-conditions" component={TermsConditions} />
            <Route exact path="/products" component={Product} />
            <Route exact path="/privacypolicy" component={PrivacyPolicy} />
            <Route exact path="/partner-with-us" component={PartnerWithUs} />

            <Redirect from="*" to="/" />
          </Switch>
        </BrowserRouter>
        <Footer />
      </div>
    </>
  );
}
export default App;
