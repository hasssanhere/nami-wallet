let DummyNotificationData = [
  {
    time: "Fri Nov 10 2021 9:43 pm",
    title: "Welcome to NoorNFT!",
    description: "Welcome to NoorNFT!",
  },
  {
    time: "SAT Nov 11 2021 10:43 Am",
    title: "Production Environment",
    description:
      "n a production Environment it might make sense to also report information about errors back to the",
  },
  {
    time: "Mon Nov 13 2021 9:15 pm",
    title: "Push Subscription Details",
    description:
      "he push subscription details needed by the application server are now available, and can be sent to it using,",
  },
  {
    time: "Tues Nov 14 2021 01:43 pm",
    title: "Service Worker",
    description:
      "The Service Worker specification defines a ServiceWorkerRegistration interface [SERVICE-WORKERS], which this specification extends.",
  },
  {
    time: "Fri Nov 17 2021 1:23 pm",
    title: "supportedContentEncodings",
    description:
      "The supportedContentEncodings attribute exposes the sequence of supported content codings that can be used to encrypt the payload of a push message. A content coding is indicated using the Content-Encoding header field when requesting the sending of a push message from the push service.",
  },
  {
    time: "Mon Nov 20 2021 5:14 pm",
    title: "content coding",
    description:
      " A content coding is indicated using the Content-Encoding header field when requesting the sending of a push message from the push service.",
  },
];

export default DummyNotificationData;
