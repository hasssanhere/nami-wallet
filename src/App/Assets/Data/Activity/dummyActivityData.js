let DummyactivityData = [
  {
    Avatar:
      "https://ipfs.pixura.io/ipfs/Qmcs6Ruf6d5yuU5XKxMvKAQozpL886jXn9gWxh5qmCxLcj/SR-PROFILE._2K21_ASTROpsd.gif",
    Name: "@jd842842",
    filterType: "Sale",
    discription: "made an offer for about $909 on",
    Art: "back2back",
    ArtImage:
      "https://images.pexels.com/photos/7249183/pexels-photo-7249183.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
    AgoTime: "Tue Nov 30 2021 11:48:57 GMT+0500",
    URL: "www.google.com",
  },
  {
    Avatar:
      "https://ipfs.pixura.io/ipfs/QmRgCjywZCUqaxWVMtYS88J2y4SD6W4j5V3iqyoCuhj84B/3D5441F2-F327-415C-8DBF-62FBDDA11F4B.jpeg",
    Name: "@angie_mathot",
    filterType: "Offer",
    discription: "created",
    Art: "The Million Dollar Line",
    ArtImage:
      "https://images.pexels.com/photos/36704/pexels-photo.jpg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
    Video: false,
    AgoTime: "Tue Oct 30 2021 11:48:57 GMT+0500",
    URL: "www.google.com",
  },
  {
    Avatar:
      "https://ipfs.pixura.io/ipfs/Qmcs6Ruf6d5yuU5XKxMvKAQozpL886jXn9gWxh5qmCxLcj/SR-PROFILE._2K21_ASTROpsd.gif",
    Name: "@jd842842",
    filterType: "Sale",
    discription: "made an offer for about $909 on",
    Art: "back2back",
    ArtImage:
      "https://images.pexels.com/photos/7261977/pexels-photo-7261977.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
    AgoTime: "Tue Nov 30 2021 11:48:57 GMT+0500",
    URL: "www.google.com",
  },
  {
    Avatar:
      "https://ipfs.pixura.io/ipfs/Qmcs6Ruf6d5yuU5XKxMvKAQozpL886jXn9gWxh5qmCxLcj/SR-PROFILE._2K21_ASTROpsd.gif",
    Name: "@jd842842",
    filterType: "Listing",
    discription: "made an offer for about $909 on",
    Art: "back2back",
    ArtImage:
      "https://images.pexels.com/photos/2427797/pexels-photo-2427797.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
    AgoTime: "Tue Nov 30 2021 11:48:57 GMT+0500",
    URL: "www.google.com",
  },
  {
    Avatar:
      "https://ipfs.pixura.io/ipfs/QmRgCjywZCUqaxWVMtYS88J2y4SD6W4j5V3iqyoCuhj84B/3D5441F2-F327-415C-8DBF-62FBDDA11F4B.jpeg",
    Name: "@angie_mathot",
    filterType: "Sale",
    discription: "created",
    Art: "The Million Dollar Line",
    ArtImage:
      "https://images.pexels.com/photos/3163677/pexels-photo-3163677.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
    Video: false,
    AgoTime: "Tue Oct 30 2021 11:48:57 GMT+0500",
    URL: "www.google.com",
  },
];

export default DummyactivityData;
