import React from 'react'

const ArrowDown = () => {
    return (
        <svg width="10" height="7" viewBox="0 0 10 7" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M1.175 0.951904L5 4.65606L8.825 0.951904L10 2.09227L5 6.94487L0 2.09227L1.175 0.951904Z" fill="black"/>
        </svg>
    )
}

export default ArrowDown
