import React from "react";
import { Link } from "react-router-dom";
import youtube from "../Assets/images/youtube.svg";
import instagram from "../Assets/images/instagram.svg";
import discord from "../Assets/images/discord.svg";
import FooterModal from "./FooterModal";
import PrivacyPolicyModal from "./PrivacyPolicyModal";
import TermsConditionModal from "./TermsConditionModal";

function Footer() {
  const [modalShow, setModalShow] = React.useState(false);
  const [modalShow1, setModalShow1] = React.useState(false);

  const [modalShow2, setModalShow2] = React.useState(false);

  const modalFunction = () => {
    setModalShow(true);
    // textForFeesCommission = {};
  };
  return (
    <footer className="bg-black-100 mt-5">
      <div class="container">
        <div className="max-w-10xl mx-auto px-4 sm:px-6 ">
          <FooterModal show={modalShow} onHide={() => setModalShow(false)} />
          <PrivacyPolicyModal
            show={modalShow1}
            onHide={() => setModalShow1(false)}
          />
          <TermsConditionModal
            show={modalShow2}
            onHide={() => setModalShow2(false)}
          />

          <div className=" grid sm:grid-cols-12 gap-8 py-8 md:py-16 border-t border-gray-200">
            {/* 1st block */}
            <div className="pl-0 sm:col-span-12 lg:col-span-3 ">
              <div className="mb-2 ">
                {/* Logo */}
                <Link to="/" className="inline-block" aria-label="Cruip">
                  <h1 className=" font-black text-white w-footer-title mb-2">
                    NoorNFT
                  </h1>
                </Link>
                <p className="w-footer-text">
                  The best NFT marketplace website in the world and feel your
                  experience in selling or buy our work
                </p>
              </div>
            </div>

            {/* 2nd block */}
            <div className="sm:col-span-6 md:col-span-3 lg:col-span-3">
              <h6 className="text-white font-medium mb-3 w-nft-link-title">
                About
              </h6>
              <ul className="text-sm w-nft-footlink">
                <li className="mb-2">
                  <Link
                    to="/market"
                    className=" hover:text-white transition duration-150 ease-in-out"
                  >
                    Markretplace
                  </Link>
                </li>
                <li className="mb-2">
                  <Link
                    to="/nftguide"
                    className=" hover:text-white transition duration-150 ease-in-out"
                  >
                    NFT Guide
                  </Link>
                </li>
                <li className="mb-2">
                  <Link
                    to="/terms-conditions"
                    className=" hover:text-white transition duration-150 ease-in-out"
                  >
                    FAQ's{" "}
                  </Link>
                </li>
              </ul>
            </div>
            {/* 3rd block */}
            <div className="sm:col-span-6 md:col-span-3 lg:col-span-3">
              <h6 className="text-white font-medium mb-3">Company</h6>
              <ul className="text-sm w-nft-footlink">
                <li className="mb-2">
                  <Link
                    onClick={() => setModalShow2(true)}
                    className=" hover:text-white transition duration-150 ease-in-out"
                  >
                    Terms & Conditions
                  </Link>
                </li>
                <li className="mb-2">
                  <Link
                    onClick={() => setModalShow1(true)}
                    className=" hover:text-white transition duration-150 ease-in-out"
                  >
                    Privacy & Policy{" "}
                  </Link>
                </li>

                <li className="mb-2">
                  <Link
                    onClick={modalFunction}
                    className=" hover:text-white transition duration-150 ease-in-out"
                  >
                    Fees and Commission
                  </Link>
                </li>
              </ul>
            </div>

            {/* 4th block */}
            <div className="sm:col-span-6 md:col-span-3 lg:col-span-3">
              <h6 className="text-white font-medium mb-3">Contact</h6>
              <ul className="text-sm w-nft-footlink">
                <li className="mb-2">
                  <Link
                    to="#"
                    className=" hover:text-white transition duration-150 ease-in-out"
                  >
                    +012 3456789
                  </Link>
                </li>
                <li className="mb-2">
                  <Link
                    to="#"
                    className=" hover:text-white transition duration-150 ease-in-out"
                  >
                    hello@email.com
                  </Link>
                </li>
                <li className="mb-2 d-flex">
                  <Link
                    to="#"
                    className=" hover:text-white transition duration-150 ease-in-out"
                  >
                    <img src={youtube} />
                  </Link>
                  <Link
                    to="#"
                    className="ml-3 hover:text-white transition duration-150 ease-in-out"
                  >
                    <img src={discord} />
                  </Link>
                  <Link
                    to="#"
                    className="ml-3 hover:text-white transition duration-150 ease-in-out"
                  >
                    <img src={instagram} />
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
