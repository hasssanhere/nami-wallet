import React from "react";
import { Button, Modal } from "react-bootstrap";

function TermsConditionModal(props) {
  return (
    <div>
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Term & Condition
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>
            <p>
              Welcome to NoorNFT a non-custodial NFT marketplace, your
              one-stop-shop for buying and selling Cardano NFTs! We like to keep
              things simple around here but there are some important things you
              should be aware of:
            </p>
            <p className="mt-3">
              1. We are a non-custodial NFT resale solution, as such NoorNFT
              does not market its own NFTs or sell on behalf of others.
            </p>
            <p className="mt-2">
              2. Sellers can link their Cardano wallets to their account and
              then select an NFT or selection of NFTs to list for sale. These
              NFTs remain in the sellers wallet at all times until a buyer is
              found and the purchase confirmed. At no point will anyone from
              NoorNFT contact you and ask you to send your NFT or your ADA
              outside of a live sale. If in doubt please contact us. Please
              report any suspicious activity or messages to us without delay.
            </p>{" "}
            <p className="mt-2">
              3. All NFTs are listed by community members, as with all second
              hand sales BUYER BEWARE. You are advised to do your own research
              BEFORE buying any NFT. Please confirm that the policy ID and other
              data match the project you are intending to buy. If there is any
              doubt please direct any such queries to the project who first
              minted the NFT, they are best placed to help you make an informed
              decision.
            </p>
            <p className="mt-2">
              4. NoorNFT is acting as a non-custodial technology service
              provider only. As we are only an intermediary linking sellers with
              buyers only there is a strict no refund policy for any reason. We
              simply have no control over the seller once a purchase is
              complete.
            </p>
            <p className="mt-2">
              5. The buying and selling of digital artwork and NFTs remains
              largely unregulated but you are advised to check the laws in your
              country before buying or selling any NFTs. You may wish to consult
              with a lawyer.
            </p>
            <p className="mt-2">
              6. NoorNFT is comprised of a global team of developers but our
              business is entity is registered in the Seychelles. We have
              adopted a privacy first approach to business, as such any data
              stored with us is extremely secure.
            </p>
            <p className="mt-2">
              7. We hope everyone will continue to use this platform for the
              right purposes, however any spamming, offensive material or
              unlawful activity will result in an immediate, permanent ban
              without warning.
            </p>
            <p className="mt-2">
              8. To remain compliant with current AML & KYC laws in the USA, UK
              and Europe we have implemented KYC checks for all buyers are
              sellers with transactions totalling 10,000 Euros. This is a
              cumulative figure. There is no KYC requirement for accounts
              spending less than 10,000 Euros. Any collected data is stored
              securely and will be used for AML & KYC purposes only. Be assured
              only the minimum data required will be requested. We will never
              sell or pass such data on.
            </p>
            <p className="mt-2">
              9. By using our service you are agreeing to the above terms of
              service as well as our fees which are detailed on the fees &
              commission page.
            </p>
            <p className="mt-2">
              10. We reserve the right to amend these terms at any time without
              prior warning.
            </p>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button
            style={{
              backgroundColor: "black",
              borderColor: "black",
            }}
            onClick={props.onHide}
          >
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}

export default TermsConditionModal;
