import React, { Fragment } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const UpperHeader = () => {
  return (
    <>
      <div className="bg-gray-900 px-4 py-2 flex flex-wrap flex-row-reverse flex-auto gap-5">
        <button className="font-semibold text-white inline-flex items-center space-x-3 transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110">
          <i className="fab fa-facebook fa-lg"></i>
        </button>
        <button className="font-semibold text-white inline-flex items-center space-x-3 transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110">
          <i className="fab fa-instagram fa-lg"></i>
        </button>
        <button className="font-semibold text-white inline-flex items-center space-x-3 transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110">
          <i className="fab fa-twitter fa-lg"></i>
        </button>
        <button className="font-semibold text-white inline-flex items-center space-x-3 transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110">
          <i className="fab fa-discord fa-lg"></i>
        </button>
      </div>
    </>
  );
};

export default UpperHeader;
