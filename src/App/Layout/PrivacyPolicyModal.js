import React from "react";
import { Button, Modal } from "react-bootstrap";

function PrivacyPolicyModal(props) {
  return (
    <div>
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Privacy & Policy
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>
            <p>
              Welcome to V1 of NoorNFT By listing any NFT or buying any NFT you
              are agreeing fully to our Fees & Commission structure as well as
              our main Terms of Service.
            </p>
            <p className="mt-3">
              1. There is a minimal network ‘dust’ fee to link each wallet to
              your account, none of which is retained by NoorNFT.{" "}
            </p>
            <p className="mt-2">
              2. There is no fee to list or de-list an NFT.{" "}
            </p>{" "}
            <p className="mt-2">
              3. There is no cost to create an account or ongoing membership
              fee.{" "}
            </p>
            <p className="mt-2">
              4. We charge 2.5% commission (min 1 ADA) on each successful NFT
              sale. This will be deducted from the sellers ADA amount.{" "}
            </p>
            <p className="mt-2">
              5. In addition there are Cardano network transaction fees, which
              will vary over time, none of which are retained by NoorNFT. These
              fees will also be deduced from the sellers ADA amount.{" "}
            </p>
            <p className="mt-3">
              By way of a simple example: A seller lists a Cardano NFT for 100
              ADA and a buyer is found. The buyer sends 100 ADA. The retained
              NoorNFT commission is 2.5 ADA and the network fee is 1.7 ADA. Thus
              the seller will receive 95.8 ADA.
            </p>
            <p className="mt-3">
              Don't forget when any NFT is sent there is the Cardano network fee
              of circa 1.5 ADA. But you will have recieved that amount when you
              were sent the NFT, so to us that is a nil cost for most users.{" "}
            </p>
            <p className="mt-3">
              All sales are full & final so please ensure you understand the
              commission structure BEFORE selling any NFTs.
            </p>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button
            style={{
              backgroundColor: "black",
              borderColor: "black",
            }}
            onClick={props.onHide}
          >
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}

export default PrivacyPolicyModal;
