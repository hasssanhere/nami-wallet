import React, { useState, useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import { BrowserRouter as Router } from "react-router-dom";
import { Navbar, Nav, NavDropdown } from "react-bootstrap";
import "../Assets/css/css/header.css";
import Avatar from "@material-ui/core/Avatar";
import { makeStyles } from "@material-ui/core/styles";
import Swal from "sweetalert2";
import CreateArtistProfile from "../../Components/Authentication/SignUp/Index";
import ArtistLogin from "../../Components/Authentication/SignIn/Index";
import { connectHWallet } from "../../Services/HomeServices";
import mainLogo from "../Assets/images/LOGO.jpg";
import { OWalletAddress } from "../../Services/artistSevices";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
    },
  },
}));

const Toast = Swal.mixin({
  toast: true,
  position: "top-end",
  showConfirmButton: false,
  timer: 1000,
  timerProgressBar: true,
  didOpen: (toast) => {
    toast.addEventListener("mouseenter", Swal.stopTimer);
    toast.addEventListener("mouseleave", Swal.resumeTimer);
  },
});

function Header() {
  const classes = useStyles();
  const [top, setTop] = useState(true);
  const [refresh, setRefresh] = useState(true);
  const history = useHistory();
  const [modalSignUp, setModalSignUp] = useState(false);
  const [modalSignIn, setModalSignIn] = useState(false);
  const [data, setUserDataa] = useState({});

  //-------------------------------------------------------------------------

  const SignOut = () => {
    // history.push("/");
    sessionStorage.clear();
    setRefresh(!refresh);
  };

  const disConnectWallet = () => {
    sessionStorage.removeItem("walletAddress");
    // Toast.fire({
    //   icon: "success",
    //   text: "Wallet DisConnected",
    // });
    setRefresh(!refresh);
  };

  const connectWallet = async () => {
    const cardano = await window.cardano;
    if (cardano) {
      await cardano?.enable();
      const enable = await cardano?.isEnabled();

      console.log(enable);
      if (enable) {
        const walletaddress = await cardano?.getUsedAddresses();
        console.log(walletaddress);

        // OWalletAddress(walletaddress[0], setRefresh, refresh);
        const body = {
          address: walletaddress[0],
        };
        // connectHWallet(body, refresh, setRefresh);
        OWalletAddress({ body, refresh, setRefresh });
        // const addr = await cardanoSerializationLibNodejs.Address.from_bytes(
        //   Buffer.from(sessionStorage.getItem("walletaddress"), "hex")
        // );
        // console.log(addr.to_bech32());
      } else {
        Toast.fire({
          icon: "error",
          text: "Something Went Wrong Kindly Try Again",
        });
      }
    } else {
      Toast.fire({
        toast: true,
        icon: "error",
        text: "Kindly Install a Nami wallet in your Browser",
      });
    }
    // if (sessionStorage.getItem("id")) {

    // } else {
    //   Toast.fire({
    //     toast: true,
    //     icon: "error",
    //     text: "Please login to connect wallet",
    //   });
    // }

    // console.log(cardanoSerializationLibNodejs);
  };

  // detect whether user has scrolled the page down by 10px
  useEffect(() => {
    const scrollHandler = () => {
      window.pageYOffset > 10 ? setTop(false) : setTop(true);
    };
    window.addEventListener("scroll", scrollHandler);
    return () => window.removeEventListener("scroll", scrollHandler);
  }, [top, refresh]);

  // useEffect(() => {
  //   const id = sessionStorage.getItem("id");
  //   getUserDetailsbyID(id, setUserDataa);
  // }, [refresh]);
  return (
    <Router>
      <div>
        <header
          className={`fixed w-bd-bottom w-headbg-white mx px-0 w-full bg-white-500 z-30 md:bg-opacity-90 transition duration-300 ease-in-out ${
            !top && "  shadow-sm"
          }`}
        >
          {/* <UpperHeader /> */}
          {/* <TickerNews /> */}

          <Navbar expand="lg">
            <Navbar.Brand href="/">
              <img
                src={mainLogo}
                style={{ width: 130, height: 80, marginTop: "2px" }}
              />
              {/* <h1 className="ml-6 font-black text-2xl">NoorNFT</h1> */}
            </Navbar.Brand>
            <div className=" lg:hidden n-userMobileDropDown">
              {sessionStorage.getItem("id") ? (
                <>
                  <NavDropdown
                    className="flex flex-row align-center"
                    id="check"
                    title={
                      <div className=" md:px-0 lg:px-0 flex flex-row align-center -mt-2 ">
                        <div className="flex align-center mr-3 mt-1">
                          {" "}
                          <Avatar
                            id="nav-avatar"
                            style={{
                              width: "32px",
                              height: "32px",
                            }}
                            alt="Remy Sharp"
                            src={
                              sessionStorage.getItem("image").match("undefined")
                                ? "https://media.istockphoto.com/vectors/male-profile-flat-blue-simple-icon-with-long-shadow-vector-id522855255?k=20&m=522855255&s=612x612&w=0&h=fLLvwEbgOmSzk1_jQ0MgDATEVcVOh_kqEe0rqi7aM5A="
                                : `${sessionStorage.getItem("image")}`
                            }
                            className={classes.large}
                          />
                          {/* <div className="w-nft-user flex flex-row items-center justify-center">
                              {data?.flname?.split(" ")[0]}
                            </div> */}
                        </div>
                        {/* {data?.flname.splice(" ")} */}
                      </div>
                    }
                  >
                    <NavDropdown.Item
                      href={`/user-detail/${sessionStorage.getItem(
                        "username"
                      )}`}
                    >
                      <p>{sessionStorage.getItem("flname")}</p>

                      {sessionStorage.getItem("email")}
                    </NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item
                      href={`/user-detail/${sessionStorage.getItem(
                        "username"
                      )}`}
                    >
                      Profile
                    </NavDropdown.Item>

                    {/* <NavDropdown.Item href="/activity">
                      Activity
                    </NavDropdown.Item> */}

                    <NavDropdown.Divider />
                    <NavDropdown.Item href="/" onClick={() => SignOut()}>
                      {/* <Link to="/">Sign out</Link> */}
                      Sign Out
                    </NavDropdown.Item>
                  </NavDropdown>
                </>
              ) : null}
            </div>
            <Navbar.Toggle aria-controls="basic-navbar-nav" className="mr-6" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="ml-auto mt-2">
                <Nav.Link href="/market"> Marketplace</Nav.Link>

                {sessionStorage.getItem("role") == "artist" ? (
                  <Nav.Link href="/createNft">Create NFT </Nav.Link>
                ) : null}
                <Nav.Link href="/nftguide">NFT Guide</Nav.Link>
                {sessionStorage.getItem("id") ? null : (
                  <>
                    <CreateArtistProfile
                      show={modalSignUp}
                      // onHide={() => setModalSignUp(false)}
                      setModalSignIn={setModalSignIn}
                      setModalSignUp={setModalSignUp}
                      modalSignIn={modalSignIn}
                      modalSignUp={modalSignUp}
                    />
                    <ArtistLogin
                      show={modalSignIn}
                      onHide={() => setModalSignIn(false)}
                      setModalSignIn={setModalSignIn}
                      setModalSignUp={setModalSignUp}
                    />
                    <Nav.Link
                      onClick={() => {
                        setModalSignIn(true);
                      }}
                      // onClick={() =>
                      //   history.push({
                      //     pathname: "/market",
                      //     search: "?signIn=true",
                      //   })
                      // }
                    >
                      Login
                    </Nav.Link>
                  </>
                )}

                {sessionStorage.getItem("id") ? (
                  <>
                    {" "}
                    {sessionStorage.getItem("walletAddress") ? (
                      <p
                        className="btn-sm fontFamily ml-6 mr-6 text-white text-lg rounded-md cursor-pointer"
                        onClick={() => {
                          disConnectWallet();
                          // history.push("./wallets");
                        }}
                      >
                        Disconnect Wallet
                      </p>
                    ) : (
                      <p
                        className="btn-sm fontFamily ml-6 mr-6  text-white text-lg rounded-md cursor-pointer"
                        style={{ fontWeight: "400", fontSize: "16px" }}
                        onClick={() => {
                          connectWallet();
                          // history.push("./wallets");
                        }}
                      >
                        Connect Wallet
                      </p>
                    )}
                  </>
                ) : (
                  <></>
                )}
                {sessionStorage.getItem("id") ? (
                  <>
                    <NavDropdown
                      className="flex flex-row align-center hidden lg:block n-userDesktopDropDown"
                      id="check"
                      title={
                        <div className=" md:px-0 lg:px-0 flex flex-row align-center -mt-2 ">
                          <div className="flex align-center mr-3 mt-1">
                            {" "}
                            <img
                              className="pimage2 rounded-full"
                              src={
                                sessionStorage
                                  .getItem("image")
                                  .match("undefined")
                                  ? "https://media.istockphoto.com/vectors/male-profile-flat-blue-simple-icon-with-long-shadow-vector-id522855255?k=20&m=522855255&s=612x612&w=0&h=fLLvwEbgOmSzk1_jQ0MgDATEVcVOh_kqEe0rqi7aM5A="
                                  : `${sessionStorage.getItem("image")}`
                              }
                              alt="Noor"
                              onError={(e) => {
                                /**
                                 * Any code. For instance, changing the `src` prop with a fallback url.
                                 * In our code, I've added `e.target.className = fallback_className` for instance.
                                 */
                                e.target.src =
                                  "https://media.istockphoto.com/vectors/male-profile-flat-blue-simple-icon-with-long-shadow-vector-id522855255?k=20&m=522855255&s=612x612&w=0&h=fLLvwEbgOmSzk1_jQ0MgDATEVcVOh_kqEe0rqi7aM5A=";
                              }}
                              style={{
                                width: "32px",
                                height: "32px",
                              }}
                            />
                            {/* <div className="w-nft-user flex flex-row items-center justify-center">
                              {data?.flname?.split(" ")[0]}
                            </div> */}
                          </div>
                        </div>
                      }
                    >
                      <NavDropdown.Item
                        href={`/user-detail/${sessionStorage.getItem(
                          "username"
                        )}`}
                      >
                        <p>{sessionStorage.getItem("flname")}</p>

                        {sessionStorage.getItem("email")}
                      </NavDropdown.Item>
                      <NavDropdown.Divider />
                      <NavDropdown.Item
                        href={`/user-detail/${sessionStorage.getItem(
                          "username"
                        )}`}
                      >
                        Profile
                      </NavDropdown.Item>

                      {/* <NavDropdown.Item href="/activity">
                      Activity
                    </NavDropdown.Item> */}

                      <NavDropdown.Divider />
                      <NavDropdown.Item href="/" onClick={() => SignOut()}>
                        {/* <Link to="/">Sign out</Link> */}
                        Sign out
                      </NavDropdown.Item>
                    </NavDropdown>
                  </>
                ) : null}
              </Nav>
            </Navbar.Collapse>
          </Navbar>
        </header>
      </div>
    </Router>
  );
}
export default Header;
